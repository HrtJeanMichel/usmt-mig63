# **USMT MIG63** #

# Description

Cet outil permet de migrer des paramètres ainsi que des profils utilisateurs locaux ou domaines.
Il repose sur les outils en ligne de commmande Scanstate et Loadstate . Ces derniers font partie de Microsoft USMT (User State Migration Tool de l'ADK).

# Screenshot

![usmt-mig63.gif](https://i.imgur.com/t0D2gPZ.gif)


# Features

* Capturer/restaurer l'état d'un ou plusieurs profils et paramètres utilisateurs
* Chiffrement/déchiffrement par mot de passe de l'archive .MIG
* Enregistrement de la capture dans un fichier de migration (.MIG) et un fichier de configuration (.XML)
* Détection automatique du répertoire contenant les outils LoadState et Scanstate, l'arborescence USMT (x86 ou amd64) doit être copiée à côté du fichier USMT-Mig63.exe.
* Possibilité d'importer nos propres fichiers .xml de migration (miguser.xml, migapp.xml, config.xml, *.xml, etc...)
* Visualisation d'un fichier .MIG (à l'aide de son fichier .XML associé)


# Prerequisites

* Tout OS Windows
* DotNet Framework 4.5
* Pas d'installation
* Le répertoire USMT de Microsoft doit être copié à côté du fichier USMT-Mig63.exe
* Exécution de l'outil depuis un compte membre du groupe administrateurs
* Autres recommandations d'usage liées à l'outil USMT !

# Credits

* Sven Walter, Dennis Magno : Pour la version de la librairie [MetroFramework](https://github.com/dennismagno/metroframework-modern-ui)
* Microsoft : [User State Migration Tool](https://docs.microsoft.com/en-us/windows/deployment/usmt/getting-started-with-the-user-state-migration-tool)


# Copyright

Copyright © HrtJm 2008-2020


# Licence

