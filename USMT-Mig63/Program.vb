﻿Imports System.Threading
Imports USMT_Mig63.Helper

Friend Class Program
    <STAThread()>
    Public Shared Sub Main(Args As String())
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Dim instanceCountOne As Boolean = False
        Using mtex As Mutex = New Mutex(True, Application.ProductName, instanceCountOne)
            If instanceCountOne Then
                If Utils.CurrentIsAdministrator Then
                    If Utils.GetUsernameBySessionId.ToUpper = Utils.CurrentLoggedinUserNameElevated.ToUpper Then
                        If Utils.isCorrectOS Then
                            Dim Tups = Utils.Usmt5PathInfos
                            If Tups.Item1 = True Then
                                Application.Run(New FrmMain)
                                mtex.ReleaseMutex()
                            Else
                                Using infos As New FrmInfo(Tups.Item2, Tups.Item3)
                                    infos.ShowDialog()
                                End Using
                            End If
                        Else
                            Using infos As New FrmInfo("Mauvais environnement", "Cette application doit être exécutée sur un des système d'exploitation suivant : " & vbNewLine & Utils.GetOsInline)
                                infos.ShowDialog()
                            End Using
                        End If
                    Else
                        Using infos As New FrmInfo("Mauvais environnement", "Cette application ne sera pas exécutée depuis une élévation de privilèges !")
                            infos.ShowDialog()
                        End Using
                    End If
                Else
                    Using infos As New FrmInfo("Mauvais environnement", "Cette application doit être exécutée depuis un compte membre du groupe Administrateurs !")
                        infos.ShowDialog()
                    End Using
                End If
            End If
        End Using
    End Sub
End Class

