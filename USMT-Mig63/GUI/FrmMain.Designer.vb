﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.TbcMain = New MetroFramework.Controls.MetroTabControl()
        Me.TpCapture = New MetroFramework.Controls.MetroTabPage()
        Me.PnlCaptureUsmt = New MetroFramework.Controls.MetroPanel()
        Me.BtnCaptureUsmtBrowse = New System.Windows.Forms.Button()
        Me.LblCaptureUsmtPath = New MetroFramework.Controls.MetroLabel()
        Me.LblCaptureUsmt = New MetroFramework.Controls.MetroLabel()
        Me.BtnCapture = New System.Windows.Forms.Button()
        Me.PnlCaptureEncryption = New MetroFramework.Controls.MetroPanel()
        Me.LblCaptureEncrypt = New MetroFramework.Controls.MetroLabel()
        Me.BtnCaptureEncrypt = New System.Windows.Forms.Button()
        Me.LblCaptureEncryption = New MetroFramework.Controls.MetroLabel()
        Me.PnlCaptureOS = New MetroFramework.Controls.MetroPanel()
        Me.PcbCaptureOsLogo = New System.Windows.Forms.PictureBox()
        Me.LblCaptureOsName = New MetroFramework.Controls.MetroLabel()
        Me.LblCaptureOS = New MetroFramework.Controls.MetroLabel()
        Me.PnlCaptureUsers = New MetroFramework.Controls.MetroPanel()
        Me.LblCaptureUser = New MetroFramework.Controls.MetroLabel()
        Me.BtnCaptureUsers = New System.Windows.Forms.Button()
        Me.LblCaptureUsers = New MetroFramework.Controls.MetroLabel()
        Me.PnlCaptureSave = New MetroFramework.Controls.MetroPanel()
        Me.LblCaptureSavePath = New MetroFramework.Controls.MetroLabel()
        Me.BtnCaptureSaveBrowse = New System.Windows.Forms.Button()
        Me.LblCaptureSave = New MetroFramework.Controls.MetroLabel()
        Me.TpRestore = New MetroFramework.Controls.MetroTabPage()
        Me.PnlRestoreUsmt = New MetroFramework.Controls.MetroPanel()
        Me.BtnRestoreUsmtBrowse = New System.Windows.Forms.Button()
        Me.LblRestoreUsmtPath = New MetroFramework.Controls.MetroLabel()
        Me.LblRestoreUsmt = New MetroFramework.Controls.MetroLabel()
        Me.PnlRestoreUsers = New MetroFramework.Controls.MetroPanel()
        Me.LblRestoreUser = New MetroFramework.Controls.MetroLabel()
        Me.BtnRestoreUsers = New System.Windows.Forms.Button()
        Me.LblRestoreUsers = New MetroFramework.Controls.MetroLabel()
        Me.BtnRestore = New System.Windows.Forms.Button()
        Me.PnlRestoreFile = New MetroFramework.Controls.MetroPanel()
        Me.BtnRestoreFileInfos = New System.Windows.Forms.Button()
        Me.LblRestoreFilePath = New MetroFramework.Controls.MetroLabel()
        Me.BtnRestoreFile = New System.Windows.Forms.Button()
        Me.LblRestoreFile = New MetroFramework.Controls.MetroLabel()
        Me.PnlRestoreDecryption = New MetroFramework.Controls.MetroPanel()
        Me.LblRestoreDecrypt = New MetroFramework.Controls.MetroLabel()
        Me.BtnRestoreDecrypt = New System.Windows.Forms.Button()
        Me.LblRestoreDecryption = New MetroFramework.Controls.MetroLabel()
        Me.PnlRestoreOS = New MetroFramework.Controls.MetroPanel()
        Me.PcbRestoreOsLogo = New System.Windows.Forms.PictureBox()
        Me.LblRestoreOsName = New MetroFramework.Controls.MetroLabel()
        Me.LblRestoreOS = New MetroFramework.Controls.MetroLabel()
        Me.TpAbout = New MetroFramework.Controls.MetroTabPage()
        Me.PnlAbout = New MetroFramework.Controls.MetroPanel()
        Me.PcbAboutLogo = New System.Windows.Forms.PictureBox()
        Me.LblAboutUsmtVersion = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutProgramVersion = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutDeveloper = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutUsmtVersionTitle = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutProgramVersionTitle = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutDeveloperTitle = New MetroFramework.Controls.MetroLabel()
        Me.LblAboutTitle = New MetroFramework.Controls.MetroLabel()
        Me.TtInfos = New System.Windows.Forms.ToolTip(Me.components)
        Me.LblLoading = New MetroFramework.Controls.MetroLabel()
        Me.TbcMain.SuspendLayout()
        Me.TpCapture.SuspendLayout()
        Me.PnlCaptureUsmt.SuspendLayout()
        Me.PnlCaptureEncryption.SuspendLayout()
        Me.PnlCaptureOS.SuspendLayout()
        CType(Me.PcbCaptureOsLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlCaptureUsers.SuspendLayout()
        Me.PnlCaptureSave.SuspendLayout()
        Me.TpRestore.SuspendLayout()
        Me.PnlRestoreUsmt.SuspendLayout()
        Me.PnlRestoreUsers.SuspendLayout()
        Me.PnlRestoreFile.SuspendLayout()
        Me.PnlRestoreDecryption.SuspendLayout()
        Me.PnlRestoreOS.SuspendLayout()
        CType(Me.PcbRestoreOsLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TpAbout.SuspendLayout()
        Me.PnlAbout.SuspendLayout()
        CType(Me.PcbAboutLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TbcMain
        '
        Me.TbcMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TbcMain.Controls.Add(Me.TpCapture)
        Me.TbcMain.Controls.Add(Me.TpRestore)
        Me.TbcMain.Controls.Add(Me.TpAbout)
        Me.TbcMain.Location = New System.Drawing.Point(23, 63)
        Me.TbcMain.Name = "TbcMain"
        Me.TbcMain.SelectedIndex = 0
        Me.TbcMain.Size = New System.Drawing.Size(685, 643)
        Me.TbcMain.Style = MetroFramework.MetroColorStyle.Silver
        Me.TbcMain.TabIndex = 0
        Me.TbcMain.UseSelectable = True
        '
        'TpCapture
        '
        Me.TpCapture.Controls.Add(Me.PnlCaptureUsmt)
        Me.TpCapture.Controls.Add(Me.BtnCapture)
        Me.TpCapture.Controls.Add(Me.PnlCaptureEncryption)
        Me.TpCapture.Controls.Add(Me.PnlCaptureOS)
        Me.TpCapture.Controls.Add(Me.PnlCaptureUsers)
        Me.TpCapture.Controls.Add(Me.PnlCaptureSave)
        Me.TpCapture.HorizontalScrollbarBarColor = True
        Me.TpCapture.HorizontalScrollbarHighlightOnWheel = False
        Me.TpCapture.HorizontalScrollbarSize = 10
        Me.TpCapture.Location = New System.Drawing.Point(4, 41)
        Me.TpCapture.Name = "TpCapture"
        Me.TpCapture.Size = New System.Drawing.Size(677, 598)
        Me.TpCapture.TabIndex = 0
        Me.TpCapture.Text = "Capturer"
        Me.TpCapture.VerticalScrollbarBarColor = True
        Me.TpCapture.VerticalScrollbarHighlightOnWheel = False
        Me.TpCapture.VerticalScrollbarSize = 10
        '
        'PnlCaptureUsmt
        '
        Me.PnlCaptureUsmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCaptureUsmt.Controls.Add(Me.BtnCaptureUsmtBrowse)
        Me.PnlCaptureUsmt.Controls.Add(Me.LblCaptureUsmtPath)
        Me.PnlCaptureUsmt.Controls.Add(Me.LblCaptureUsmt)
        Me.PnlCaptureUsmt.HorizontalScrollbarBarColor = True
        Me.PnlCaptureUsmt.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlCaptureUsmt.HorizontalScrollbarSize = 10
        Me.PnlCaptureUsmt.Location = New System.Drawing.Point(5, 100)
        Me.PnlCaptureUsmt.Name = "PnlCaptureUsmt"
        Me.PnlCaptureUsmt.Size = New System.Drawing.Size(330, 215)
        Me.PnlCaptureUsmt.TabIndex = 8
        Me.PnlCaptureUsmt.VerticalScrollbarBarColor = True
        Me.PnlCaptureUsmt.VerticalScrollbarHighlightOnWheel = False
        Me.PnlCaptureUsmt.VerticalScrollbarSize = 10
        '
        'BtnCaptureUsmtBrowse
        '
        Me.BtnCaptureUsmtBrowse.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureUsmtBrowse.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Settings
        Me.BtnCaptureUsmtBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureUsmtBrowse.FlatAppearance.BorderSize = 0
        Me.BtnCaptureUsmtBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureUsmtBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureUsmtBrowse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureUsmtBrowse.Location = New System.Drawing.Point(115, 56)
        Me.BtnCaptureUsmtBrowse.Name = "BtnCaptureUsmtBrowse"
        Me.BtnCaptureUsmtBrowse.Size = New System.Drawing.Size(100, 100)
        Me.BtnCaptureUsmtBrowse.TabIndex = 7
        Me.BtnCaptureUsmtBrowse.Tag = ""
        Me.BtnCaptureUsmtBrowse.UseVisualStyleBackColor = False
        '
        'LblCaptureUsmtPath
        '
        Me.LblCaptureUsmtPath.Location = New System.Drawing.Point(3, 169)
        Me.LblCaptureUsmtPath.Name = "LblCaptureUsmtPath"
        Me.LblCaptureUsmtPath.Size = New System.Drawing.Size(322, 19)
        Me.LblCaptureUsmtPath.TabIndex = 5
        Me.LblCaptureUsmtPath.Text = "Pas de répertoire sélectionné !"
        Me.LblCaptureUsmtPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureUsmtPath.UseCustomForeColor = True
        '
        'LblCaptureUsmt
        '
        Me.LblCaptureUsmt.BackColor = System.Drawing.Color.SteelBlue
        Me.LblCaptureUsmt.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblCaptureUsmt.ForeColor = System.Drawing.Color.White
        Me.LblCaptureUsmt.Location = New System.Drawing.Point(0, 0)
        Me.LblCaptureUsmt.Name = "LblCaptureUsmt"
        Me.LblCaptureUsmt.Size = New System.Drawing.Size(329, 29)
        Me.LblCaptureUsmt.TabIndex = 2
        Me.LblCaptureUsmt.Text = "Répertoire des outils de migration USMT"
        Me.LblCaptureUsmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureUsmt.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblCaptureUsmt.UseCustomBackColor = True
        Me.LblCaptureUsmt.UseCustomForeColor = True
        '
        'BtnCapture
        '
        Me.BtnCapture.BackColor = System.Drawing.Color.SteelBlue
        Me.BtnCapture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCapture.Enabled = False
        Me.BtnCapture.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCapture.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCapture.ForeColor = System.Drawing.Color.White
        Me.BtnCapture.Location = New System.Drawing.Point(3, 541)
        Me.BtnCapture.Name = "BtnCapture"
        Me.BtnCapture.Size = New System.Drawing.Size(671, 54)
        Me.BtnCapture.TabIndex = 7
        Me.BtnCapture.Text = "LANCER LA CAPTURE"
        Me.BtnCapture.UseVisualStyleBackColor = False
        '
        'PnlCaptureEncryption
        '
        Me.PnlCaptureEncryption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCaptureEncryption.Controls.Add(Me.LblCaptureEncrypt)
        Me.PnlCaptureEncryption.Controls.Add(Me.BtnCaptureEncrypt)
        Me.PnlCaptureEncryption.Controls.Add(Me.LblCaptureEncryption)
        Me.PnlCaptureEncryption.HorizontalScrollbarBarColor = True
        Me.PnlCaptureEncryption.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlCaptureEncryption.HorizontalScrollbarSize = 10
        Me.PnlCaptureEncryption.Location = New System.Drawing.Point(5, 321)
        Me.PnlCaptureEncryption.Name = "PnlCaptureEncryption"
        Me.PnlCaptureEncryption.Size = New System.Drawing.Size(330, 214)
        Me.PnlCaptureEncryption.TabIndex = 4
        Me.PnlCaptureEncryption.VerticalScrollbarBarColor = True
        Me.PnlCaptureEncryption.VerticalScrollbarHighlightOnWheel = False
        Me.PnlCaptureEncryption.VerticalScrollbarSize = 10
        '
        'LblCaptureEncrypt
        '
        Me.LblCaptureEncrypt.Location = New System.Drawing.Point(3, 168)
        Me.LblCaptureEncrypt.Name = "LblCaptureEncrypt"
        Me.LblCaptureEncrypt.Size = New System.Drawing.Size(322, 19)
        Me.LblCaptureEncrypt.TabIndex = 9
        Me.LblCaptureEncrypt.Text = "Pas de mot de passe !"
        Me.LblCaptureEncrypt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnCaptureEncrypt
        '
        Me.BtnCaptureEncrypt.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureEncrypt.BackgroundImage = CType(resources.GetObject("BtnCaptureEncrypt.BackgroundImage"), System.Drawing.Image)
        Me.BtnCaptureEncrypt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureEncrypt.FlatAppearance.BorderSize = 0
        Me.BtnCaptureEncrypt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureEncrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureEncrypt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureEncrypt.Location = New System.Drawing.Point(115, 56)
        Me.BtnCaptureEncrypt.Name = "BtnCaptureEncrypt"
        Me.BtnCaptureEncrypt.Size = New System.Drawing.Size(100, 100)
        Me.BtnCaptureEncrypt.TabIndex = 8
        Me.BtnCaptureEncrypt.UseVisualStyleBackColor = False
        '
        'LblCaptureEncryption
        '
        Me.LblCaptureEncryption.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCaptureEncryption.BackColor = System.Drawing.Color.SteelBlue
        Me.LblCaptureEncryption.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblCaptureEncryption.ForeColor = System.Drawing.Color.White
        Me.LblCaptureEncryption.Location = New System.Drawing.Point(0, 0)
        Me.LblCaptureEncryption.Name = "LblCaptureEncryption"
        Me.LblCaptureEncryption.Size = New System.Drawing.Size(328, 29)
        Me.LblCaptureEncryption.TabIndex = 2
        Me.LblCaptureEncryption.Text = "Chiffrement de l'archive"
        Me.LblCaptureEncryption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureEncryption.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblCaptureEncryption.UseCustomBackColor = True
        Me.LblCaptureEncryption.UseCustomForeColor = True
        '
        'PnlCaptureOS
        '
        Me.PnlCaptureOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCaptureOS.Controls.Add(Me.PcbCaptureOsLogo)
        Me.PnlCaptureOS.Controls.Add(Me.LblCaptureOsName)
        Me.PnlCaptureOS.Controls.Add(Me.LblCaptureOS)
        Me.PnlCaptureOS.HorizontalScrollbarBarColor = True
        Me.PnlCaptureOS.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlCaptureOS.HorizontalScrollbarSize = 10
        Me.PnlCaptureOS.Location = New System.Drawing.Point(5, 3)
        Me.PnlCaptureOS.Name = "PnlCaptureOS"
        Me.PnlCaptureOS.Size = New System.Drawing.Size(667, 91)
        Me.PnlCaptureOS.TabIndex = 2
        Me.PnlCaptureOS.VerticalScrollbarBarColor = True
        Me.PnlCaptureOS.VerticalScrollbarHighlightOnWheel = False
        Me.PnlCaptureOS.VerticalScrollbarSize = 10
        '
        'PcbCaptureOsLogo
        '
        Me.PcbCaptureOsLogo.BackColor = System.Drawing.Color.Transparent
        Me.PcbCaptureOsLogo.Location = New System.Drawing.Point(599, 32)
        Me.PcbCaptureOsLogo.Name = "PcbCaptureOsLogo"
        Me.PcbCaptureOsLogo.Size = New System.Drawing.Size(54, 54)
        Me.PcbCaptureOsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbCaptureOsLogo.TabIndex = 5
        Me.PcbCaptureOsLogo.TabStop = False
        '
        'LblCaptureOsName
        '
        Me.LblCaptureOsName.Location = New System.Drawing.Point(95, 50)
        Me.LblCaptureOsName.Name = "LblCaptureOsName"
        Me.LblCaptureOsName.Size = New System.Drawing.Size(479, 19)
        Me.LblCaptureOsName.TabIndex = 4
        Me.LblCaptureOsName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblCaptureOS
        '
        Me.LblCaptureOS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCaptureOS.BackColor = System.Drawing.Color.SteelBlue
        Me.LblCaptureOS.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblCaptureOS.ForeColor = System.Drawing.Color.White
        Me.LblCaptureOS.Location = New System.Drawing.Point(0, 0)
        Me.LblCaptureOS.Name = "LblCaptureOS"
        Me.LblCaptureOS.Size = New System.Drawing.Size(665, 29)
        Me.LblCaptureOS.TabIndex = 2
        Me.LblCaptureOS.Text = "Système d'exploitation source"
        Me.LblCaptureOS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureOS.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblCaptureOS.UseCustomBackColor = True
        Me.LblCaptureOS.UseCustomForeColor = True
        '
        'PnlCaptureUsers
        '
        Me.PnlCaptureUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCaptureUsers.Controls.Add(Me.LblCaptureUser)
        Me.PnlCaptureUsers.Controls.Add(Me.BtnCaptureUsers)
        Me.PnlCaptureUsers.Controls.Add(Me.LblCaptureUsers)
        Me.PnlCaptureUsers.HorizontalScrollbarBarColor = True
        Me.PnlCaptureUsers.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlCaptureUsers.HorizontalScrollbarSize = 10
        Me.PnlCaptureUsers.Location = New System.Drawing.Point(342, 321)
        Me.PnlCaptureUsers.Name = "PnlCaptureUsers"
        Me.PnlCaptureUsers.Size = New System.Drawing.Size(330, 214)
        Me.PnlCaptureUsers.TabIndex = 5
        Me.PnlCaptureUsers.VerticalScrollbarBarColor = True
        Me.PnlCaptureUsers.VerticalScrollbarHighlightOnWheel = False
        Me.PnlCaptureUsers.VerticalScrollbarSize = 10
        '
        'LblCaptureUser
        '
        Me.LblCaptureUser.Location = New System.Drawing.Point(3, 168)
        Me.LblCaptureUser.Name = "LblCaptureUser"
        Me.LblCaptureUser.Size = New System.Drawing.Size(322, 19)
        Me.LblCaptureUser.TabIndex = 10
        Me.LblCaptureUser.Text = "Pas d'utilisateur sélectionné !"
        Me.LblCaptureUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnCaptureUsers
        '
        Me.BtnCaptureUsers.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureUsers.BackgroundImage = CType(resources.GetObject("BtnCaptureUsers.BackgroundImage"), System.Drawing.Image)
        Me.BtnCaptureUsers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureUsers.FlatAppearance.BorderSize = 0
        Me.BtnCaptureUsers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureUsers.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureUsers.Location = New System.Drawing.Point(114, 56)
        Me.BtnCaptureUsers.Name = "BtnCaptureUsers"
        Me.BtnCaptureUsers.Size = New System.Drawing.Size(100, 100)
        Me.BtnCaptureUsers.TabIndex = 9
        Me.BtnCaptureUsers.UseVisualStyleBackColor = False
        '
        'LblCaptureUsers
        '
        Me.LblCaptureUsers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCaptureUsers.BackColor = System.Drawing.Color.SteelBlue
        Me.LblCaptureUsers.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblCaptureUsers.ForeColor = System.Drawing.Color.White
        Me.LblCaptureUsers.Location = New System.Drawing.Point(0, 0)
        Me.LblCaptureUsers.Name = "LblCaptureUsers"
        Me.LblCaptureUsers.Size = New System.Drawing.Size(328, 29)
        Me.LblCaptureUsers.TabIndex = 3
        Me.LblCaptureUsers.Text = "Sélection des utilisateurs"
        Me.LblCaptureUsers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureUsers.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblCaptureUsers.UseCustomBackColor = True
        Me.LblCaptureUsers.UseCustomForeColor = True
        '
        'PnlCaptureSave
        '
        Me.PnlCaptureSave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlCaptureSave.Controls.Add(Me.LblCaptureSavePath)
        Me.PnlCaptureSave.Controls.Add(Me.BtnCaptureSaveBrowse)
        Me.PnlCaptureSave.Controls.Add(Me.LblCaptureSave)
        Me.PnlCaptureSave.HorizontalScrollbarBarColor = True
        Me.PnlCaptureSave.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlCaptureSave.HorizontalScrollbarSize = 10
        Me.PnlCaptureSave.Location = New System.Drawing.Point(342, 100)
        Me.PnlCaptureSave.Name = "PnlCaptureSave"
        Me.PnlCaptureSave.Size = New System.Drawing.Size(330, 214)
        Me.PnlCaptureSave.TabIndex = 3
        Me.PnlCaptureSave.VerticalScrollbarBarColor = True
        Me.PnlCaptureSave.VerticalScrollbarHighlightOnWheel = False
        Me.PnlCaptureSave.VerticalScrollbarSize = 10
        '
        'LblCaptureSavePath
        '
        Me.LblCaptureSavePath.Location = New System.Drawing.Point(3, 169)
        Me.LblCaptureSavePath.Name = "LblCaptureSavePath"
        Me.LblCaptureSavePath.Size = New System.Drawing.Size(322, 19)
        Me.LblCaptureSavePath.TabIndex = 7
        Me.LblCaptureSavePath.Text = "Pas de répertoire sélectionné !"
        Me.LblCaptureSavePath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnCaptureSaveBrowse
        '
        Me.BtnCaptureSaveBrowse.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureSaveBrowse.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Save
        Me.BtnCaptureSaveBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureSaveBrowse.FlatAppearance.BorderSize = 0
        Me.BtnCaptureSaveBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureSaveBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureSaveBrowse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureSaveBrowse.Location = New System.Drawing.Point(114, 56)
        Me.BtnCaptureSaveBrowse.Name = "BtnCaptureSaveBrowse"
        Me.BtnCaptureSaveBrowse.Size = New System.Drawing.Size(100, 100)
        Me.BtnCaptureSaveBrowse.TabIndex = 6
        Me.BtnCaptureSaveBrowse.UseVisualStyleBackColor = False
        '
        'LblCaptureSave
        '
        Me.LblCaptureSave.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblCaptureSave.BackColor = System.Drawing.Color.SteelBlue
        Me.LblCaptureSave.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblCaptureSave.ForeColor = System.Drawing.Color.White
        Me.LblCaptureSave.Location = New System.Drawing.Point(0, 0)
        Me.LblCaptureSave.Name = "LblCaptureSave"
        Me.LblCaptureSave.Size = New System.Drawing.Size(328, 29)
        Me.LblCaptureSave.TabIndex = 3
        Me.LblCaptureSave.Text = "Répertoire de sauvegarde"
        Me.LblCaptureSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblCaptureSave.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblCaptureSave.UseCustomBackColor = True
        Me.LblCaptureSave.UseCustomForeColor = True
        '
        'TpRestore
        '
        Me.TpRestore.Controls.Add(Me.PnlRestoreUsmt)
        Me.TpRestore.Controls.Add(Me.PnlRestoreUsers)
        Me.TpRestore.Controls.Add(Me.BtnRestore)
        Me.TpRestore.Controls.Add(Me.PnlRestoreFile)
        Me.TpRestore.Controls.Add(Me.PnlRestoreDecryption)
        Me.TpRestore.Controls.Add(Me.PnlRestoreOS)
        Me.TpRestore.HorizontalScrollbarBarColor = True
        Me.TpRestore.HorizontalScrollbarHighlightOnWheel = False
        Me.TpRestore.HorizontalScrollbarSize = 10
        Me.TpRestore.Location = New System.Drawing.Point(4, 41)
        Me.TpRestore.Name = "TpRestore"
        Me.TpRestore.Size = New System.Drawing.Size(677, 598)
        Me.TpRestore.TabIndex = 1
        Me.TpRestore.Text = "Restaurer"
        Me.TpRestore.VerticalScrollbarBarColor = True
        Me.TpRestore.VerticalScrollbarHighlightOnWheel = False
        Me.TpRestore.VerticalScrollbarSize = 10
        '
        'PnlRestoreUsmt
        '
        Me.PnlRestoreUsmt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlRestoreUsmt.Controls.Add(Me.BtnRestoreUsmtBrowse)
        Me.PnlRestoreUsmt.Controls.Add(Me.LblRestoreUsmtPath)
        Me.PnlRestoreUsmt.Controls.Add(Me.LblRestoreUsmt)
        Me.PnlRestoreUsmt.HorizontalScrollbarBarColor = True
        Me.PnlRestoreUsmt.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlRestoreUsmt.HorizontalScrollbarSize = 10
        Me.PnlRestoreUsmt.Location = New System.Drawing.Point(5, 100)
        Me.PnlRestoreUsmt.Name = "PnlRestoreUsmt"
        Me.PnlRestoreUsmt.Size = New System.Drawing.Size(330, 215)
        Me.PnlRestoreUsmt.TabIndex = 10
        Me.PnlRestoreUsmt.VerticalScrollbarBarColor = True
        Me.PnlRestoreUsmt.VerticalScrollbarHighlightOnWheel = False
        Me.PnlRestoreUsmt.VerticalScrollbarSize = 10
        '
        'BtnRestoreUsmtBrowse
        '
        Me.BtnRestoreUsmtBrowse.BackColor = System.Drawing.Color.Transparent
        Me.BtnRestoreUsmtBrowse.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Settings
        Me.BtnRestoreUsmtBrowse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestoreUsmtBrowse.FlatAppearance.BorderSize = 0
        Me.BtnRestoreUsmtBrowse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnRestoreUsmtBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestoreUsmtBrowse.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnRestoreUsmtBrowse.Location = New System.Drawing.Point(115, 56)
        Me.BtnRestoreUsmtBrowse.Name = "BtnRestoreUsmtBrowse"
        Me.BtnRestoreUsmtBrowse.Size = New System.Drawing.Size(100, 100)
        Me.BtnRestoreUsmtBrowse.TabIndex = 8
        Me.BtnRestoreUsmtBrowse.UseVisualStyleBackColor = False
        '
        'LblRestoreUsmtPath
        '
        Me.LblRestoreUsmtPath.Location = New System.Drawing.Point(3, 169)
        Me.LblRestoreUsmtPath.Name = "LblRestoreUsmtPath"
        Me.LblRestoreUsmtPath.Size = New System.Drawing.Size(322, 19)
        Me.LblRestoreUsmtPath.TabIndex = 5
        Me.LblRestoreUsmtPath.Text = "Pas de répertoire sélectionné !"
        Me.LblRestoreUsmtPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreUsmtPath.UseCustomForeColor = True
        '
        'LblRestoreUsmt
        '
        Me.LblRestoreUsmt.BackColor = System.Drawing.Color.IndianRed
        Me.LblRestoreUsmt.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblRestoreUsmt.ForeColor = System.Drawing.Color.White
        Me.LblRestoreUsmt.Location = New System.Drawing.Point(0, 0)
        Me.LblRestoreUsmt.Name = "LblRestoreUsmt"
        Me.LblRestoreUsmt.Size = New System.Drawing.Size(329, 29)
        Me.LblRestoreUsmt.TabIndex = 2
        Me.LblRestoreUsmt.Text = "Répertoire des outils de migration USMT"
        Me.LblRestoreUsmt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreUsmt.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblRestoreUsmt.UseCustomBackColor = True
        Me.LblRestoreUsmt.UseCustomForeColor = True
        '
        'PnlRestoreUsers
        '
        Me.PnlRestoreUsers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlRestoreUsers.Controls.Add(Me.LblRestoreUser)
        Me.PnlRestoreUsers.Controls.Add(Me.BtnRestoreUsers)
        Me.PnlRestoreUsers.Controls.Add(Me.LblRestoreUsers)
        Me.PnlRestoreUsers.HorizontalScrollbarBarColor = True
        Me.PnlRestoreUsers.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlRestoreUsers.HorizontalScrollbarSize = 10
        Me.PnlRestoreUsers.Location = New System.Drawing.Point(342, 321)
        Me.PnlRestoreUsers.Name = "PnlRestoreUsers"
        Me.PnlRestoreUsers.Size = New System.Drawing.Size(330, 214)
        Me.PnlRestoreUsers.TabIndex = 9
        Me.PnlRestoreUsers.VerticalScrollbarBarColor = True
        Me.PnlRestoreUsers.VerticalScrollbarHighlightOnWheel = False
        Me.PnlRestoreUsers.VerticalScrollbarSize = 10
        '
        'LblRestoreUser
        '
        Me.LblRestoreUser.Location = New System.Drawing.Point(3, 168)
        Me.LblRestoreUser.Name = "LblRestoreUser"
        Me.LblRestoreUser.Size = New System.Drawing.Size(322, 19)
        Me.LblRestoreUser.TabIndex = 10
        Me.LblRestoreUser.Text = "Pas d'utilisateur sélectionné !"
        Me.LblRestoreUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnRestoreUsers
        '
        Me.BtnRestoreUsers.BackColor = System.Drawing.Color.Transparent
        Me.BtnRestoreUsers.BackgroundImage = CType(resources.GetObject("BtnRestoreUsers.BackgroundImage"), System.Drawing.Image)
        Me.BtnRestoreUsers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestoreUsers.FlatAppearance.BorderSize = 0
        Me.BtnRestoreUsers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnRestoreUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestoreUsers.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnRestoreUsers.Location = New System.Drawing.Point(114, 56)
        Me.BtnRestoreUsers.Name = "BtnRestoreUsers"
        Me.BtnRestoreUsers.Size = New System.Drawing.Size(100, 100)
        Me.BtnRestoreUsers.TabIndex = 9
        Me.BtnRestoreUsers.UseVisualStyleBackColor = False
        '
        'LblRestoreUsers
        '
        Me.LblRestoreUsers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblRestoreUsers.BackColor = System.Drawing.Color.IndianRed
        Me.LblRestoreUsers.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblRestoreUsers.ForeColor = System.Drawing.Color.White
        Me.LblRestoreUsers.Location = New System.Drawing.Point(0, 0)
        Me.LblRestoreUsers.Name = "LblRestoreUsers"
        Me.LblRestoreUsers.Size = New System.Drawing.Size(328, 29)
        Me.LblRestoreUsers.TabIndex = 3
        Me.LblRestoreUsers.Text = "Sélection des utilisateurs"
        Me.LblRestoreUsers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreUsers.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblRestoreUsers.UseCustomBackColor = True
        Me.LblRestoreUsers.UseCustomForeColor = True
        '
        'BtnRestore
        '
        Me.BtnRestore.BackColor = System.Drawing.Color.IndianRed
        Me.BtnRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestore.Enabled = False
        Me.BtnRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestore.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRestore.ForeColor = System.Drawing.Color.White
        Me.BtnRestore.Location = New System.Drawing.Point(3, 541)
        Me.BtnRestore.Name = "BtnRestore"
        Me.BtnRestore.Size = New System.Drawing.Size(671, 54)
        Me.BtnRestore.TabIndex = 8
        Me.BtnRestore.Text = "LANCER LA RESTAURATION"
        Me.BtnRestore.UseVisualStyleBackColor = False
        '
        'PnlRestoreFile
        '
        Me.PnlRestoreFile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlRestoreFile.Controls.Add(Me.BtnRestoreFileInfos)
        Me.PnlRestoreFile.Controls.Add(Me.LblRestoreFilePath)
        Me.PnlRestoreFile.Controls.Add(Me.BtnRestoreFile)
        Me.PnlRestoreFile.Controls.Add(Me.LblRestoreFile)
        Me.PnlRestoreFile.HorizontalScrollbarBarColor = True
        Me.PnlRestoreFile.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlRestoreFile.HorizontalScrollbarSize = 10
        Me.PnlRestoreFile.Location = New System.Drawing.Point(342, 100)
        Me.PnlRestoreFile.Name = "PnlRestoreFile"
        Me.PnlRestoreFile.Size = New System.Drawing.Size(330, 214)
        Me.PnlRestoreFile.TabIndex = 6
        Me.PnlRestoreFile.VerticalScrollbarBarColor = True
        Me.PnlRestoreFile.VerticalScrollbarHighlightOnWheel = False
        Me.PnlRestoreFile.VerticalScrollbarSize = 10
        '
        'BtnRestoreFileInfos
        '
        Me.BtnRestoreFileInfos.BackColor = System.Drawing.Color.Transparent
        Me.BtnRestoreFileInfos.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.migInfo
        Me.BtnRestoreFileInfos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestoreFileInfos.FlatAppearance.BorderSize = 0
        Me.BtnRestoreFileInfos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnRestoreFileInfos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestoreFileInfos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnRestoreFileInfos.Location = New System.Drawing.Point(293, 32)
        Me.BtnRestoreFileInfos.Name = "BtnRestoreFileInfos"
        Me.BtnRestoreFileInfos.Size = New System.Drawing.Size(32, 32)
        Me.BtnRestoreFileInfos.TabIndex = 8
        Me.BtnRestoreFileInfos.UseVisualStyleBackColor = False
        Me.BtnRestoreFileInfos.Visible = False
        '
        'LblRestoreFilePath
        '
        Me.LblRestoreFilePath.Location = New System.Drawing.Point(3, 169)
        Me.LblRestoreFilePath.Name = "LblRestoreFilePath"
        Me.LblRestoreFilePath.Size = New System.Drawing.Size(322, 19)
        Me.LblRestoreFilePath.TabIndex = 7
        Me.LblRestoreFilePath.Text = "Pas de fichier sélectionné !"
        Me.LblRestoreFilePath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnRestoreFile
        '
        Me.BtnRestoreFile.BackColor = System.Drawing.Color.Transparent
        Me.BtnRestoreFile.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Folder
        Me.BtnRestoreFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestoreFile.FlatAppearance.BorderSize = 0
        Me.BtnRestoreFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnRestoreFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestoreFile.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnRestoreFile.Location = New System.Drawing.Point(114, 56)
        Me.BtnRestoreFile.Name = "BtnRestoreFile"
        Me.BtnRestoreFile.Size = New System.Drawing.Size(100, 100)
        Me.BtnRestoreFile.TabIndex = 6
        Me.BtnRestoreFile.UseVisualStyleBackColor = False
        '
        'LblRestoreFile
        '
        Me.LblRestoreFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblRestoreFile.BackColor = System.Drawing.Color.IndianRed
        Me.LblRestoreFile.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblRestoreFile.ForeColor = System.Drawing.Color.White
        Me.LblRestoreFile.Location = New System.Drawing.Point(0, 0)
        Me.LblRestoreFile.Name = "LblRestoreFile"
        Me.LblRestoreFile.Size = New System.Drawing.Size(328, 29)
        Me.LblRestoreFile.TabIndex = 3
        Me.LblRestoreFile.Text = "Sélection du fichier de migration"
        Me.LblRestoreFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreFile.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblRestoreFile.UseCustomBackColor = True
        Me.LblRestoreFile.UseCustomForeColor = True
        '
        'PnlRestoreDecryption
        '
        Me.PnlRestoreDecryption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlRestoreDecryption.Controls.Add(Me.LblRestoreDecrypt)
        Me.PnlRestoreDecryption.Controls.Add(Me.BtnRestoreDecrypt)
        Me.PnlRestoreDecryption.Controls.Add(Me.LblRestoreDecryption)
        Me.PnlRestoreDecryption.HorizontalScrollbarBarColor = True
        Me.PnlRestoreDecryption.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlRestoreDecryption.HorizontalScrollbarSize = 10
        Me.PnlRestoreDecryption.Location = New System.Drawing.Point(5, 321)
        Me.PnlRestoreDecryption.Name = "PnlRestoreDecryption"
        Me.PnlRestoreDecryption.Size = New System.Drawing.Size(330, 214)
        Me.PnlRestoreDecryption.TabIndex = 5
        Me.PnlRestoreDecryption.VerticalScrollbarBarColor = True
        Me.PnlRestoreDecryption.VerticalScrollbarHighlightOnWheel = False
        Me.PnlRestoreDecryption.VerticalScrollbarSize = 10
        '
        'LblRestoreDecrypt
        '
        Me.LblRestoreDecrypt.Location = New System.Drawing.Point(3, 168)
        Me.LblRestoreDecrypt.Name = "LblRestoreDecrypt"
        Me.LblRestoreDecrypt.Size = New System.Drawing.Size(322, 19)
        Me.LblRestoreDecrypt.TabIndex = 9
        Me.LblRestoreDecrypt.Text = "Pas de mot de passe !"
        Me.LblRestoreDecrypt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnRestoreDecrypt
        '
        Me.BtnRestoreDecrypt.BackColor = System.Drawing.Color.Transparent
        Me.BtnRestoreDecrypt.BackgroundImage = CType(resources.GetObject("BtnRestoreDecrypt.BackgroundImage"), System.Drawing.Image)
        Me.BtnRestoreDecrypt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnRestoreDecrypt.FlatAppearance.BorderSize = 0
        Me.BtnRestoreDecrypt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnRestoreDecrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRestoreDecrypt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnRestoreDecrypt.Location = New System.Drawing.Point(115, 56)
        Me.BtnRestoreDecrypt.Name = "BtnRestoreDecrypt"
        Me.BtnRestoreDecrypt.Size = New System.Drawing.Size(100, 100)
        Me.BtnRestoreDecrypt.TabIndex = 8
        Me.BtnRestoreDecrypt.UseVisualStyleBackColor = False
        '
        'LblRestoreDecryption
        '
        Me.LblRestoreDecryption.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblRestoreDecryption.BackColor = System.Drawing.Color.IndianRed
        Me.LblRestoreDecryption.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblRestoreDecryption.ForeColor = System.Drawing.Color.White
        Me.LblRestoreDecryption.Location = New System.Drawing.Point(0, 0)
        Me.LblRestoreDecryption.Name = "LblRestoreDecryption"
        Me.LblRestoreDecryption.Size = New System.Drawing.Size(328, 29)
        Me.LblRestoreDecryption.TabIndex = 2
        Me.LblRestoreDecryption.Text = "Déchiffrement de l'archive"
        Me.LblRestoreDecryption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreDecryption.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblRestoreDecryption.UseCustomBackColor = True
        Me.LblRestoreDecryption.UseCustomForeColor = True
        '
        'PnlRestoreOS
        '
        Me.PnlRestoreOS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlRestoreOS.Controls.Add(Me.PcbRestoreOsLogo)
        Me.PnlRestoreOS.Controls.Add(Me.LblRestoreOsName)
        Me.PnlRestoreOS.Controls.Add(Me.LblRestoreOS)
        Me.PnlRestoreOS.HorizontalScrollbarBarColor = True
        Me.PnlRestoreOS.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlRestoreOS.HorizontalScrollbarSize = 10
        Me.PnlRestoreOS.Location = New System.Drawing.Point(5, 3)
        Me.PnlRestoreOS.Name = "PnlRestoreOS"
        Me.PnlRestoreOS.Size = New System.Drawing.Size(667, 91)
        Me.PnlRestoreOS.TabIndex = 3
        Me.PnlRestoreOS.VerticalScrollbarBarColor = True
        Me.PnlRestoreOS.VerticalScrollbarHighlightOnWheel = False
        Me.PnlRestoreOS.VerticalScrollbarSize = 10
        '
        'PcbRestoreOsLogo
        '
        Me.PcbRestoreOsLogo.BackColor = System.Drawing.Color.Transparent
        Me.PcbRestoreOsLogo.Location = New System.Drawing.Point(599, 32)
        Me.PcbRestoreOsLogo.Name = "PcbRestoreOsLogo"
        Me.PcbRestoreOsLogo.Size = New System.Drawing.Size(54, 54)
        Me.PcbRestoreOsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbRestoreOsLogo.TabIndex = 5
        Me.PcbRestoreOsLogo.TabStop = False
        '
        'LblRestoreOsName
        '
        Me.LblRestoreOsName.Location = New System.Drawing.Point(95, 50)
        Me.LblRestoreOsName.Name = "LblRestoreOsName"
        Me.LblRestoreOsName.Size = New System.Drawing.Size(479, 19)
        Me.LblRestoreOsName.TabIndex = 4
        Me.LblRestoreOsName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LblRestoreOS
        '
        Me.LblRestoreOS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblRestoreOS.BackColor = System.Drawing.Color.IndianRed
        Me.LblRestoreOS.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblRestoreOS.ForeColor = System.Drawing.Color.White
        Me.LblRestoreOS.Location = New System.Drawing.Point(0, 0)
        Me.LblRestoreOS.Name = "LblRestoreOS"
        Me.LblRestoreOS.Size = New System.Drawing.Size(665, 29)
        Me.LblRestoreOS.TabIndex = 2
        Me.LblRestoreOS.Text = "Système d'exploitation cible"
        Me.LblRestoreOS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblRestoreOS.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblRestoreOS.UseCustomBackColor = True
        Me.LblRestoreOS.UseCustomForeColor = True
        '
        'TpAbout
        '
        Me.TpAbout.Controls.Add(Me.PnlAbout)
        Me.TpAbout.HorizontalScrollbarBarColor = True
        Me.TpAbout.HorizontalScrollbarHighlightOnWheel = False
        Me.TpAbout.HorizontalScrollbarSize = 10
        Me.TpAbout.Location = New System.Drawing.Point(4, 41)
        Me.TpAbout.Name = "TpAbout"
        Me.TpAbout.Size = New System.Drawing.Size(677, 598)
        Me.TpAbout.TabIndex = 2
        Me.TpAbout.Text = "A propos"
        Me.TpAbout.VerticalScrollbarBarColor = True
        Me.TpAbout.VerticalScrollbarHighlightOnWheel = False
        Me.TpAbout.VerticalScrollbarSize = 10
        '
        'PnlAbout
        '
        Me.PnlAbout.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlAbout.Controls.Add(Me.PcbAboutLogo)
        Me.PnlAbout.Controls.Add(Me.LblAboutUsmtVersion)
        Me.PnlAbout.Controls.Add(Me.LblAboutProgramVersion)
        Me.PnlAbout.Controls.Add(Me.LblAboutDeveloper)
        Me.PnlAbout.Controls.Add(Me.LblAboutUsmtVersionTitle)
        Me.PnlAbout.Controls.Add(Me.LblAboutProgramVersionTitle)
        Me.PnlAbout.Controls.Add(Me.LblAboutDeveloperTitle)
        Me.PnlAbout.Controls.Add(Me.LblAboutTitle)
        Me.PnlAbout.HorizontalScrollbarBarColor = True
        Me.PnlAbout.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlAbout.HorizontalScrollbarSize = 10
        Me.PnlAbout.Location = New System.Drawing.Point(5, 3)
        Me.PnlAbout.Name = "PnlAbout"
        Me.PnlAbout.Size = New System.Drawing.Size(667, 592)
        Me.PnlAbout.TabIndex = 4
        Me.PnlAbout.VerticalScrollbarBarColor = True
        Me.PnlAbout.VerticalScrollbarHighlightOnWheel = False
        Me.PnlAbout.VerticalScrollbarSize = 10
        '
        'PcbAboutLogo
        '
        Me.PcbAboutLogo.BackColor = System.Drawing.Color.Transparent
        Me.PcbAboutLogo.Image = Global.USMT_Mig63.My.Resources.Resources.USMTM63
        Me.PcbAboutLogo.Location = New System.Drawing.Point(46, 72)
        Me.PcbAboutLogo.Name = "PcbAboutLogo"
        Me.PcbAboutLogo.Size = New System.Drawing.Size(69, 69)
        Me.PcbAboutLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PcbAboutLogo.TabIndex = 10
        Me.PcbAboutLogo.TabStop = False
        '
        'LblAboutUsmtVersion
        '
        Me.LblAboutUsmtVersion.Location = New System.Drawing.Point(277, 127)
        Me.LblAboutUsmtVersion.Name = "LblAboutUsmtVersion"
        Me.LblAboutUsmtVersion.Size = New System.Drawing.Size(316, 19)
        Me.LblAboutUsmtVersion.TabIndex = 9
        Me.LblAboutUsmtVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutProgramVersion
        '
        Me.LblAboutProgramVersion.Location = New System.Drawing.Point(277, 98)
        Me.LblAboutProgramVersion.Name = "LblAboutProgramVersion"
        Me.LblAboutProgramVersion.Size = New System.Drawing.Size(316, 19)
        Me.LblAboutProgramVersion.TabIndex = 8
        Me.LblAboutProgramVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutDeveloper
        '
        Me.LblAboutDeveloper.Location = New System.Drawing.Point(277, 69)
        Me.LblAboutDeveloper.Name = "LblAboutDeveloper"
        Me.LblAboutDeveloper.Size = New System.Drawing.Size(316, 19)
        Me.LblAboutDeveloper.TabIndex = 7
        Me.LblAboutDeveloper.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutUsmtVersionTitle
        '
        Me.LblAboutUsmtVersionTitle.Location = New System.Drawing.Point(156, 127)
        Me.LblAboutUsmtVersionTitle.Name = "LblAboutUsmtVersionTitle"
        Me.LblAboutUsmtVersionTitle.Size = New System.Drawing.Size(108, 19)
        Me.LblAboutUsmtVersionTitle.TabIndex = 6
        Me.LblAboutUsmtVersionTitle.Text = "Version USMT : "
        Me.LblAboutUsmtVersionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutProgramVersionTitle
        '
        Me.LblAboutProgramVersionTitle.Location = New System.Drawing.Point(156, 98)
        Me.LblAboutProgramVersionTitle.Name = "LblAboutProgramVersionTitle"
        Me.LblAboutProgramVersionTitle.Size = New System.Drawing.Size(108, 19)
        Me.LblAboutProgramVersionTitle.TabIndex = 5
        Me.LblAboutProgramVersionTitle.Text = "Version : "
        Me.LblAboutProgramVersionTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutDeveloperTitle
        '
        Me.LblAboutDeveloperTitle.Location = New System.Drawing.Point(156, 69)
        Me.LblAboutDeveloperTitle.Name = "LblAboutDeveloperTitle"
        Me.LblAboutDeveloperTitle.Size = New System.Drawing.Size(108, 19)
        Me.LblAboutDeveloperTitle.TabIndex = 4
        Me.LblAboutDeveloperTitle.Text = "Développé par : "
        Me.LblAboutDeveloperTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblAboutTitle
        '
        Me.LblAboutTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblAboutTitle.BackColor = System.Drawing.Color.DimGray
        Me.LblAboutTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblAboutTitle.ForeColor = System.Drawing.Color.White
        Me.LblAboutTitle.Location = New System.Drawing.Point(0, 0)
        Me.LblAboutTitle.Name = "LblAboutTitle"
        Me.LblAboutTitle.Size = New System.Drawing.Size(665, 29)
        Me.LblAboutTitle.TabIndex = 2
        Me.LblAboutTitle.Text = "Informations du logiciel"
        Me.LblAboutTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblAboutTitle.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblAboutTitle.UseCustomBackColor = True
        Me.LblAboutTitle.UseCustomForeColor = True
        '
        'TtInfos
        '
        Me.TtInfos.AutomaticDelay = 200
        Me.TtInfos.AutoPopDelay = 20000
        Me.TtInfos.InitialDelay = 200
        Me.TtInfos.ReshowDelay = 40
        '
        'LblLoading
        '
        Me.LblLoading.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.LblLoading.Location = New System.Drawing.Point(23, 63)
        Me.LblLoading.Name = "LblLoading"
        Me.LblLoading.Size = New System.Drawing.Size(685, 35)
        Me.LblLoading.TabIndex = 1
        Me.LblLoading.Text = "Chargement des paramètres ..."
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 729)
        Me.Controls.Add(Me.TbcMain)
        Me.Controls.Add(Me.LblLoading)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Silver
        Me.Text = "USMT Mig63"
        Me.TbcMain.ResumeLayout(False)
        Me.TpCapture.ResumeLayout(False)
        Me.PnlCaptureUsmt.ResumeLayout(False)
        Me.PnlCaptureEncryption.ResumeLayout(False)
        Me.PnlCaptureOS.ResumeLayout(False)
        CType(Me.PcbCaptureOsLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlCaptureUsers.ResumeLayout(False)
        Me.PnlCaptureSave.ResumeLayout(False)
        Me.TpRestore.ResumeLayout(False)
        Me.PnlRestoreUsmt.ResumeLayout(False)
        Me.PnlRestoreUsers.ResumeLayout(False)
        Me.PnlRestoreFile.ResumeLayout(False)
        Me.PnlRestoreDecryption.ResumeLayout(False)
        Me.PnlRestoreOS.ResumeLayout(False)
        CType(Me.PcbRestoreOsLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TpAbout.ResumeLayout(False)
        Me.PnlAbout.ResumeLayout(False)
        CType(Me.PcbAboutLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TbcMain As MetroFramework.Controls.MetroTabControl
    Friend WithEvents TpCapture As MetroFramework.Controls.MetroTabPage
    Friend WithEvents TpRestore As MetroFramework.Controls.MetroTabPage
    Friend WithEvents PnlCaptureOS As MetroFramework.Controls.MetroPanel
    Friend WithEvents PnlCaptureSave As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblCaptureOS As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblCaptureSave As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlCaptureEncryption As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblCaptureEncryption As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlCaptureUsers As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblCaptureUsers As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblCaptureOsName As MetroFramework.Controls.MetroLabel
    Friend WithEvents PcbCaptureOsLogo As PictureBox
    Friend WithEvents PnlRestoreOS As MetroFramework.Controls.MetroPanel
    Friend WithEvents PcbRestoreOsLogo As PictureBox
    Friend WithEvents LblRestoreOsName As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblRestoreOS As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnCaptureSaveBrowse As Button
    Friend WithEvents BtnCaptureEncrypt As Button
    Friend WithEvents LblCaptureSavePath As MetroFramework.Controls.MetroLabel
    Friend WithEvents TtInfos As ToolTip
    Friend WithEvents BtnCaptureUsers As Button
    Friend WithEvents LblCaptureEncrypt As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblCaptureUser As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlRestoreDecryption As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblRestoreDecrypt As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnRestoreDecrypt As Button
    Friend WithEvents LblRestoreDecryption As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlRestoreFile As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblRestoreFilePath As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnRestoreFile As Button
    Friend WithEvents LblRestoreFile As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnCapture As Button
    Friend WithEvents BtnRestore As Button
    Friend WithEvents LblLoading As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlRestoreUsers As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblRestoreUser As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnRestoreUsers As Button
    Friend WithEvents LblRestoreUsers As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlCaptureUsmt As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblCaptureUsmtPath As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblCaptureUsmt As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlRestoreUsmt As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblRestoreUsmtPath As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblRestoreUsmt As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnCaptureUsmtBrowse As Button
    Friend WithEvents BtnRestoreUsmtBrowse As Button
    Friend WithEvents BtnRestoreFileInfos As Button
    Friend WithEvents TpAbout As MetroFramework.Controls.MetroTabPage
    Friend WithEvents PnlAbout As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblAboutProgramVersionTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutDeveloperTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutUsmtVersionTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutUsmtVersion As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutProgramVersion As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblAboutDeveloper As MetroFramework.Controls.MetroLabel
    Friend WithEvents PcbAboutLogo As PictureBox
End Class
