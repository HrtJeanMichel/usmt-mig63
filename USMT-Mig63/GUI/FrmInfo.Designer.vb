﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmInfo
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmInfo))
        Me.LblInfosMessage = New MetroFramework.Controls.MetroLabel()
        Me.BtnInfoCancel = New System.Windows.Forms.Button()
        Me.BtnInfoValid = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblInfosMessage
        '
        Me.LblInfosMessage.Location = New System.Drawing.Point(23, 60)
        Me.LblInfosMessage.Name = "LblInfosMessage"
        Me.LblInfosMessage.Size = New System.Drawing.Size(426, 86)
        Me.LblInfosMessage.TabIndex = 16
        Me.LblInfosMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblInfosMessage.WrapToLine = True
        '
        'BtnInfoCancel
        '
        Me.BtnInfoCancel.BackColor = System.Drawing.Color.Transparent
        Me.BtnInfoCancel.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Cancel
        Me.BtnInfoCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnInfoCancel.FlatAppearance.BorderSize = 0
        Me.BtnInfoCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnInfoCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnInfoCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnInfoCancel.Location = New System.Drawing.Point(430, 154)
        Me.BtnInfoCancel.Name = "BtnInfoCancel"
        Me.BtnInfoCancel.Size = New System.Drawing.Size(30, 20)
        Me.BtnInfoCancel.TabIndex = 18
        Me.BtnInfoCancel.UseVisualStyleBackColor = False
        Me.BtnInfoCancel.Visible = False
        '
        'BtnInfoValid
        '
        Me.BtnInfoValid.BackColor = System.Drawing.Color.Transparent
        Me.BtnInfoValid.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Valid
        Me.BtnInfoValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnInfoValid.FlatAppearance.BorderSize = 0
        Me.BtnInfoValid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnInfoValid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnInfoValid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnInfoValid.Location = New System.Drawing.Point(394, 154)
        Me.BtnInfoValid.Name = "BtnInfoValid"
        Me.BtnInfoValid.Size = New System.Drawing.Size(30, 20)
        Me.BtnInfoValid.TabIndex = 17
        Me.BtnInfoValid.UseVisualStyleBackColor = False
        Me.BtnInfoValid.Visible = False
        '
        'FrmInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(472, 189)
        Me.Controls.Add(Me.BtnInfoCancel)
        Me.Controls.Add(Me.BtnInfoValid)
        Me.Controls.Add(Me.LblInfosMessage)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmInfo"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Style = MetroFramework.MetroColorStyle.Yellow
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblInfosMessage As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnInfoCancel As Button
    Friend WithEvents BtnInfoValid As Button
End Class
