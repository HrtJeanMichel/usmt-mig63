﻿Imports MetroFramework.Forms
Imports USMT_Mig63.Helper

Public Class FrmCrypt
    Inherits MetroForm

#Region " Fields "
    Private m_pwd As String
    Private m_pwdMd5 As String
    Private m_Decrypt As Boolean
#End Region

#Region " Constructor "
    Public Sub New(Pwd As String, Optional ByVal Decrypt As Boolean = False, Optional ByVal PwdMd5 As String = "")
        InitializeComponent()
        If Decrypt Then
            m_Decrypt = Decrypt
            m_pwdMd5 = PwdMd5
            Text = "Déchiffrement"
            Style = MetroFramework.MetroColorStyle.IndianRed
            Invalidate()
        End If
        m_pwd = Pwd

        If Pwd = String.Empty Then
            BtnCaptureEncryptValid.Visible = False
        Else
            BtnCaptureEncryptValid.Visible = True
            TxbCryptPassword.Text = Pwd
            TxbCryptPasswordConfirm.Text = Pwd
        End If
    End Sub
#End Region

#Region " Methods "
    Private Sub TxbCryptPassword_TextChanged(sender As Object, e As EventArgs) Handles TxbCryptPassword.TextChanged
        If TxbCryptPasswordConfirm.Text = TxbCryptPassword.Text Then
            If m_pwd <> TxbCryptPasswordConfirm.Text Then
                BtnCaptureEncryptValid.Visible = True
            Else
                BtnCaptureEncryptValid.Visible = False
            End If
        Else
            BtnCaptureEncryptValid.Visible = False
        End If
    End Sub

    Private Sub TxbCryptPasswordConfirm_TextChanged(sender As Object, e As EventArgs) Handles TxbCryptPasswordConfirm.TextChanged
        If TxbCryptPasswordConfirm.Text = TxbCryptPassword.Text Then
            If m_pwd <> TxbCryptPasswordConfirm.Text Then
                BtnCaptureEncryptValid.Visible = True
            Else
                BtnCaptureEncryptValid.Visible = False
            End If
        Else
            BtnCaptureEncryptValid.Visible = False
        End If
    End Sub

    Private Sub BtnCaptureEncryptValid_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptValid.Click
        If m_Decrypt Then
            If Mig.VerifyPasswordMd5Hash(TxbCryptPassword.Text, m_pwdMd5) Then
                DialogResult = DialogResult.OK
            Else
                Using Info As New FrmInfo("Attention", "Le mot de passe que vous avez saisi pour ce fichier .MIG est incorrect !" & vbNewLine & "Voulez-vous le saisir de nouveau ?", True)
                    If Info.ShowDialog() = DialogResult.OK Then
                        Return
                    Else
                        DialogResult = DialogResult.Cancel
                    End If
                End Using
            End If
        Else
            DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub BtnCaptureEncryptCancel_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptCancel.Click
        DialogResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub ChbCryptPasswordShow_CheckedChanged(sender As Object, e As EventArgs) Handles ChbCryptPasswordShow.CheckedChanged
        If ChbCryptPasswordShow.Checked = True Then
            TxbCryptPasswordConfirm.UseSystemPasswordChar = False
            TxbCryptPasswordConfirm.PasswordChar = ""
            TxbCryptPassword.UseSystemPasswordChar = False
            TxbCryptPassword.PasswordChar = ""
        Else
            TxbCryptPasswordConfirm.UseSystemPasswordChar = True
            TxbCryptPasswordConfirm.PasswordChar = "●"
            TxbCryptPassword.UseSystemPasswordChar = True
            TxbCryptPassword.PasswordChar = "●"
        End If
    End Sub
#End Region

End Class