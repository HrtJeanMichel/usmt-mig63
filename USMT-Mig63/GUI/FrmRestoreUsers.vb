﻿Imports System.Net.NetworkInformation
Imports System.Security.Principal
Imports MetroFramework.Controls
Imports MetroFramework.Forms
Imports Microsoft.Win32
Imports USMT_Mig63.Helper

Public Class FrmRestoreUsers
    Inherits MetroForm

    Public Property UsersList As List(Of String)
    Private m_UserPCSids As List(Of String)

    Public Sub New(Usids As List(Of String))
        InitializeComponent()
        UsersList = Usids
        m_UserPCSids = New List(Of String)
        LoadPCUsers()
        If Not Usids.Count = 0 Then
            For Each refU In UsersList
                Dim str As String() = New String(1) {}
                str(0) = refU.Split("|")(0) 'accName
                str(1) = refU.Split("|")(1) 'accType
                'str(2) = If(m_UserPCSids.Contains(refU.Split("|")(0) & "|" & refU.Split("|")(1)), "Oui", "Non")
                Dim lvi As New MetroListViewItem(str) With {
                    .Checked = refU.Split("|")(2)
                }
                LvUsers.Items.Add(lvi)
            Next
        End If
        BtnCaptureEncryptValid.Visible = False
    End Sub

    Private Sub LoadPCUsers()
        Using regkey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList")
            For Each rk In regkey.GetSubKeyNames
                If rk.ToUpper.StartsWith("S-1-5-21-") Then
                    If Not rk = Utils.LoggedInSid Then
                        Dim SidSplitted = rk.Split("-")
                        If SidSplitted.Count = 8 Then
                            Dim rid = SidSplitted(7)
                            If Not String.IsNullOrEmpty(rid) AndAlso IsNumeric(rid) Then
                                If CInt(rid) >= 1000 AndAlso CInt(rid) <= 1209 Then
                                    FillPCAccounts(regkey, rk, "Local")
                                ElseIf CInt(rid) > 1209 Then
                                    If IPGlobalProperties.GetIPGlobalProperties().DomainName.Trim <> String.Empty Then
                                        FillPCAccounts(regkey, rk, "Domaine")
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        End Using
    End Sub

    Private Sub FillPCAccounts(regkey As RegistryKey, rk As String, accType As String)
        Dim currentKey = regkey.OpenSubKey(rk)
        Dim ProfileImagePath = currentKey.GetValue("ProfileImagePath").ToString
        Dim Sid = rk

        Try
            Dim Secsid As SecurityIdentifier = New SecurityIdentifier(Sid)
            Dim acct As NTAccount = CType(Secsid.Translate(GetType(NTAccount)), NTAccount)
            Dim AccName = acct.Value
            m_UserPCSids.Add(AccName & "|" & accType)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BtnCaptureEncryptValid_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptValid.Click
        UsersList.Clear()
        For Each u As MetroListViewItem In LvUsers.Items
            Dim AccName = u.Text
            Dim AccType = u.SubItems(1).Text
            UsersList.Add(AccName & "|" & AccType & "|" & u.Checked.ToString)
        Next
        DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub BtnCaptureEncryptCancel_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptCancel.Click
        DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub LvUsers_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles LvUsers.ItemCheck
        Dim newList As New List(Of String)
        If e.NewValue = CheckState.Checked Then
            ShowApplyButton(newList, True, e.Index)
        ElseIf e.NewValue = CheckState.Unchecked Then
            If LvUsers.CheckedItems.Count - 1 <= 0 Then
                Using Info As New FrmInfo("Sélection invalide", "Vous devez sélectionner au moins un compte utilisateur pour restaurer les données !")
                    Info.ShowDialog()
                End Using
                e.NewValue = e.CurrentValue
            Else
                ShowApplyButton(newList, False, e.Index)
            End If
        End If
    End Sub

    Private Sub ShowApplyButton(newList As List(Of String), Checked As Boolean, index As Integer)
        Try
            If Checked Then
                For Each u As MetroListViewItem In LvUsers.Items
                    If Not u.Index = index Then
                        newList.Add(u.Text & "|" & u.SubItems(1).Text & "|" & u.Checked.ToString)
                    End If
                Next
                Dim v = TryCast(LvUsers.Items(index), MetroListViewItem)
                newList.Add(v.Text & "|" & v.SubItems(1).Text & "|True")
            Else
                For Each u As MetroListViewItem In LvUsers.Items
                    If Not u.Index = index Then
                        newList.Add(u.Text & "|" & u.SubItems(1).Text & "|" & u.Checked.ToString)
                    End If
                Next
                Dim v = TryCast(LvUsers.Items(index), MetroListViewItem)
                newList.Add(v.Text & "|" & v.SubItems(1).Text & "|False")
            End If

            BtnCaptureEncryptValid.Visible = If(Utils.CompareLists(UsersList, newList), False, True)
        Catch ex As Exception
            MsgBox("ShowApplyButton Exception : " & ex.ToString)
        End Try
    End Sub
End Class