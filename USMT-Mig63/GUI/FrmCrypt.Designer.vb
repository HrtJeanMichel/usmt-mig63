﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCrypt
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCrypt))
        Me.LblCryptPassword = New MetroFramework.Controls.MetroLabel()
        Me.LblCryptPasswordConfirm = New MetroFramework.Controls.MetroLabel()
        Me.TxbCryptPasswordConfirm = New MetroFramework.Controls.MetroTextBox()
        Me.TxbCryptPassword = New MetroFramework.Controls.MetroTextBox()
        Me.ChbCryptPasswordShow = New MetroFramework.Controls.MetroCheckBox()
        Me.BtnCaptureEncryptCancel = New System.Windows.Forms.Button()
        Me.BtnCaptureEncryptValid = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblCryptPassword
        '
        Me.LblCryptPassword.Location = New System.Drawing.Point(23, 70)
        Me.LblCryptPassword.Name = "LblCryptPassword"
        Me.LblCryptPassword.Size = New System.Drawing.Size(309, 19)
        Me.LblCryptPassword.TabIndex = 13
        Me.LblCryptPassword.Text = "Mot de passe :"
        Me.LblCryptPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCryptPasswordConfirm
        '
        Me.LblCryptPasswordConfirm.Location = New System.Drawing.Point(23, 118)
        Me.LblCryptPasswordConfirm.Name = "LblCryptPasswordConfirm"
        Me.LblCryptPasswordConfirm.Size = New System.Drawing.Size(309, 19)
        Me.LblCryptPasswordConfirm.TabIndex = 12
        Me.LblCryptPasswordConfirm.Text = "Confirmation mot de passe :"
        Me.LblCryptPasswordConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxbCryptPasswordConfirm
        '
        '
        '
        '
        Me.TxbCryptPasswordConfirm.CustomButton.Image = Nothing
        Me.TxbCryptPasswordConfirm.CustomButton.Location = New System.Drawing.Point(287, 1)
        Me.TxbCryptPasswordConfirm.CustomButton.Name = ""
        Me.TxbCryptPasswordConfirm.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbCryptPasswordConfirm.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbCryptPasswordConfirm.CustomButton.TabIndex = 1
        Me.TxbCryptPasswordConfirm.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbCryptPasswordConfirm.CustomButton.UseSelectable = True
        Me.TxbCryptPasswordConfirm.CustomButton.Visible = False
        Me.TxbCryptPasswordConfirm.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbCryptPasswordConfirm.Lines = New String(-1) {}
        Me.TxbCryptPasswordConfirm.Location = New System.Drawing.Point(23, 140)
        Me.TxbCryptPasswordConfirm.MaxLength = 32767
        Me.TxbCryptPasswordConfirm.Name = "TxbCryptPasswordConfirm"
        Me.TxbCryptPasswordConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TxbCryptPasswordConfirm.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbCryptPasswordConfirm.SelectedText = ""
        Me.TxbCryptPasswordConfirm.SelectionLength = 0
        Me.TxbCryptPasswordConfirm.SelectionStart = 0
        Me.TxbCryptPasswordConfirm.ShortcutsEnabled = True
        Me.TxbCryptPasswordConfirm.Size = New System.Drawing.Size(309, 23)
        Me.TxbCryptPasswordConfirm.TabIndex = 11
        Me.TxbCryptPasswordConfirm.UseSelectable = True
        Me.TxbCryptPasswordConfirm.UseSystemPasswordChar = True
        Me.TxbCryptPasswordConfirm.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbCryptPasswordConfirm.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TxbCryptPassword
        '
        '
        '
        '
        Me.TxbCryptPassword.CustomButton.Image = Nothing
        Me.TxbCryptPassword.CustomButton.Location = New System.Drawing.Point(287, 1)
        Me.TxbCryptPassword.CustomButton.Name = ""
        Me.TxbCryptPassword.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbCryptPassword.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbCryptPassword.CustomButton.TabIndex = 1
        Me.TxbCryptPassword.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbCryptPassword.CustomButton.UseSelectable = True
        Me.TxbCryptPassword.CustomButton.Visible = False
        Me.TxbCryptPassword.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbCryptPassword.Lines = New String(-1) {}
        Me.TxbCryptPassword.Location = New System.Drawing.Point(23, 92)
        Me.TxbCryptPassword.MaxLength = 32767
        Me.TxbCryptPassword.Name = "TxbCryptPassword"
        Me.TxbCryptPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TxbCryptPassword.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbCryptPassword.SelectedText = ""
        Me.TxbCryptPassword.SelectionLength = 0
        Me.TxbCryptPassword.SelectionStart = 0
        Me.TxbCryptPassword.ShortcutsEnabled = True
        Me.TxbCryptPassword.Size = New System.Drawing.Size(309, 23)
        Me.TxbCryptPassword.TabIndex = 10
        Me.TxbCryptPassword.UseSelectable = True
        Me.TxbCryptPassword.UseSystemPasswordChar = True
        Me.TxbCryptPassword.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbCryptPassword.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ChbCryptPasswordShow
        '
        Me.ChbCryptPasswordShow.AutoSize = True
        Me.ChbCryptPasswordShow.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.ChbCryptPasswordShow.Location = New System.Drawing.Point(23, 181)
        Me.ChbCryptPasswordShow.Name = "ChbCryptPasswordShow"
        Me.ChbCryptPasswordShow.Size = New System.Drawing.Size(218, 19)
        Me.ChbCryptPasswordShow.TabIndex = 14
        Me.ChbCryptPasswordShow.Text = "Afficher le mot de passe en clair"
        Me.ChbCryptPasswordShow.UseSelectable = True
        '
        'BtnCaptureEncryptCancel
        '
        Me.BtnCaptureEncryptCancel.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureEncryptCancel.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Cancel
        Me.BtnCaptureEncryptCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureEncryptCancel.FlatAppearance.BorderSize = 0
        Me.BtnCaptureEncryptCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureEncryptCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureEncryptCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureEncryptCancel.Location = New System.Drawing.Point(302, 219)
        Me.BtnCaptureEncryptCancel.Name = "BtnCaptureEncryptCancel"
        Me.BtnCaptureEncryptCancel.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureEncryptCancel.TabIndex = 16
        Me.BtnCaptureEncryptCancel.UseVisualStyleBackColor = False
        '
        'BtnCaptureEncryptValid
        '
        Me.BtnCaptureEncryptValid.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureEncryptValid.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Valid
        Me.BtnCaptureEncryptValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureEncryptValid.FlatAppearance.BorderSize = 0
        Me.BtnCaptureEncryptValid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureEncryptValid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureEncryptValid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureEncryptValid.Location = New System.Drawing.Point(266, 219)
        Me.BtnCaptureEncryptValid.Name = "BtnCaptureEncryptValid"
        Me.BtnCaptureEncryptValid.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureEncryptValid.TabIndex = 15
        Me.BtnCaptureEncryptValid.UseVisualStyleBackColor = False
        Me.BtnCaptureEncryptValid.Visible = False
        '
        'FrmCrypt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 262)
        Me.ControlBox = False
        Me.Controls.Add(Me.BtnCaptureEncryptCancel)
        Me.Controls.Add(Me.BtnCaptureEncryptValid)
        Me.Controls.Add(Me.ChbCryptPasswordShow)
        Me.Controls.Add(Me.LblCryptPassword)
        Me.Controls.Add(Me.LblCryptPasswordConfirm)
        Me.Controls.Add(Me.TxbCryptPasswordConfirm)
        Me.Controls.Add(Me.TxbCryptPassword)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCrypt"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.SteelBlue
        Me.Text = "Chiffrement"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblCryptPassword As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblCryptPasswordConfirm As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxbCryptPasswordConfirm As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TxbCryptPassword As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ChbCryptPasswordShow As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents BtnCaptureEncryptValid As Button
    Friend WithEvents BtnCaptureEncryptCancel As Button
End Class
