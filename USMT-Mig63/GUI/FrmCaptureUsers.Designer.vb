﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCaptureUsers
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCaptureUsers))
        Me.LvUsers = New MetroFramework.Controls.MetroListView()
        Me.ClhAccountName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhAccountType = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhAccountSid = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ClhAccountProfilPath = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.BtnCaptureEncryptCancel = New System.Windows.Forms.Button()
        Me.BtnCaptureEncryptValid = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LvUsers
        '
        Me.LvUsers.AllowColumnReorder = True
        Me.LvUsers.AllowSorting = True
        Me.LvUsers.CheckBoxes = True
        Me.LvUsers.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClhAccountName, Me.ClhAccountType, Me.ClhAccountSid, Me.ClhAccountProfilPath})
        Me.LvUsers.Font = New System.Drawing.Font("Segoe UI", 13.0!)
        Me.LvUsers.FullRowSelect = True
        Me.LvUsers.Location = New System.Drawing.Point(23, 63)
        Me.LvUsers.Name = "LvUsers"
        Me.LvUsers.OwnerDraw = True
        Me.LvUsers.ShowItemToolTips = True
        Me.LvUsers.Size = New System.Drawing.Size(655, 225)
        Me.LvUsers.TabIndex = 17
        Me.LvUsers.UseCompatibleStateImageBehavior = False
        Me.LvUsers.UseSelectable = True
        Me.LvUsers.View = System.Windows.Forms.View.Details
        '
        'ClhAccountName
        '
        Me.ClhAccountName.Text = "Compte"
        Me.ClhAccountName.Width = 146
        '
        'ClhAccountType
        '
        Me.ClhAccountType.Text = "Type"
        Me.ClhAccountType.Width = 72
        '
        'ClhAccountSid
        '
        Me.ClhAccountSid.Text = "SID"
        Me.ClhAccountSid.Width = 272
        '
        'ClhAccountProfilPath
        '
        Me.ClhAccountProfilPath.Text = "Chemin du profil"
        Me.ClhAccountProfilPath.Width = 270
        '
        'BtnCaptureEncryptCancel
        '
        Me.BtnCaptureEncryptCancel.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureEncryptCancel.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Cancel
        Me.BtnCaptureEncryptCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureEncryptCancel.FlatAppearance.BorderSize = 0
        Me.BtnCaptureEncryptCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureEncryptCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureEncryptCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureEncryptCancel.Location = New System.Drawing.Point(648, 306)
        Me.BtnCaptureEncryptCancel.Name = "BtnCaptureEncryptCancel"
        Me.BtnCaptureEncryptCancel.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureEncryptCancel.TabIndex = 16
        Me.BtnCaptureEncryptCancel.UseVisualStyleBackColor = False
        '
        'BtnCaptureEncryptValid
        '
        Me.BtnCaptureEncryptValid.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureEncryptValid.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Valid
        Me.BtnCaptureEncryptValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureEncryptValid.FlatAppearance.BorderSize = 0
        Me.BtnCaptureEncryptValid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureEncryptValid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureEncryptValid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureEncryptValid.Location = New System.Drawing.Point(612, 306)
        Me.BtnCaptureEncryptValid.Name = "BtnCaptureEncryptValid"
        Me.BtnCaptureEncryptValid.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureEncryptValid.TabIndex = 15
        Me.BtnCaptureEncryptValid.UseVisualStyleBackColor = False
        Me.BtnCaptureEncryptValid.Visible = False
        '
        'FrmCaptureUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(701, 349)
        Me.ControlBox = False
        Me.Controls.Add(Me.LvUsers)
        Me.Controls.Add(Me.BtnCaptureEncryptCancel)
        Me.Controls.Add(Me.BtnCaptureEncryptValid)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCaptureUsers"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.SteelBlue
        Me.Text = "Utilisateurs"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnCaptureEncryptValid As Button
    Friend WithEvents BtnCaptureEncryptCancel As Button
    Friend WithEvents LvUsers As MetroFramework.Controls.MetroListView
    Friend WithEvents ClhAccountName As ColumnHeader
    Friend WithEvents ClhAccountType As ColumnHeader
    Friend WithEvents ClhAccountSid As ColumnHeader
    Friend WithEvents ClhAccountProfilPath As ColumnHeader
End Class
