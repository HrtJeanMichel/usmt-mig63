﻿Imports System.IO
Imports MetroFramework.Forms
Imports USMT_Mig63.Helper
Imports USMT_Mig63.Core
Imports USMT_Mig63.Engine
Imports USMT_Mig63.Helper.Cmd

Public Class FrmTask
    Inherits MetroForm
    Implements IDisposable

#Region " Fields "
    Private m_TaskOngoing As Boolean
    Private m_UsmtRootPath As String
    Private m_TaskStore As String
    Private m_TaskPwd As String
    Private m_TaskUsers As List(Of String)
    Private m_XmlMigFiles As String()
    Private m_XmlMigFilesInLine As String
    Private m_fsWatcher As Watcher
    Private m_applyingCollectingFound As Boolean
    Private m_TaskCmd As Cmd
    Private m_RestoreState As Boolean
    Private m_TaskName As String
    Private m_StoreOriginalName As String
    Private m_SizeMbToTransfert As String
    Private m_SummaryUsmt As String
    Private m_ExecutionTotalTime As String
    Private m_Aborted As Boolean
#End Region

#Region " Delegates "
    Private Delegate Sub UpdateFsDelegate(Str As String)
    Private Delegate Sub UpdateCmdDelegate(Str As String)
#End Region

#Region " Constructor "
    Public Sub New(UsmtRootPath As String, Store As String, Pwd As String, Users As List(Of String), XmlFiles As String(), Optional ByVal Restore As Boolean = False)
        InitializeComponent()
        m_XmlMigFiles = XmlFiles
        m_RestoreState = Restore
        m_TaskName = "ScanState"
        If Restore Then
            m_StoreOriginalName = Store
            Dim fi As New FileInfo(Store)
            Dim usmtFolder = fi.DirectoryName
            My.Computer.FileSystem.RenameFile(Store, "USMT.MIG")
            Store = New DirectoryInfo(usmtFolder).Parent.FullName
            Text = "USMT Mig63 - Restauration"
            m_TaskName = "LoadState"
            LblTaskCommand.BackColor = Color.IndianRed
            LblTaskCommand.Text = "Commande " & m_TaskName
            LblTaskProgress.BackColor = Color.IndianRed
            LblTaskProgress.Text = "Progression " & m_TaskName
            Style = MetroFramework.MetroColorStyle.IndianRed
            PgbTaskProgress.Style = MetroFramework.MetroColorStyle.IndianRed
            Invalidate()
        End If
        m_UsmtRootPath = UsmtRootPath
        m_TaskStore = Store
        m_TaskPwd = Pwd
        m_TaskUsers = Users
    End Sub
#End Region

#Region " Main "
    Private Sub FrmTask_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TxbTaskCommand.Text = CommandArgsFormat()
        TxbTaskCommand.Select(0, 0)
        Utils.KillProcess(m_TaskName)
        StartTask()
    End Sub

    Private Sub FrmTask_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            If e.CloseReason.Equals(CloseReason.UserClosing) Then
                If m_TaskOngoing Then
                    Using Info As New FrmInfo("Arrêt de la tâche", "Etes-vous certain de vouloir interrompre " & m_TaskName & " ?", True)
                        If Info.ShowDialog = DialogResult.OK Then
                            RemoveHandler m_TaskCmd.OutputLine, AddressOf m_TaskCmd_OutputLine
                            m_fsWatcher.StopWatch()
                            If m_TaskCmd.Abort() = False Then
                                m_TaskCmd.Close()
                            End If
                        Else
                            e.Cancel = True
                        End If
                    End Using
                End If
            ElseIf e.CloseReason.Equals(CloseReason.WindowsShutDown) Then
                If m_TaskOngoing Then
                    Utils.ShutdownCancel()
                    Using Info As New FrmInfo("Fermeture non requise", "Vous ne pouvez pas interrompre la tâche durant son exécution." & vbNewLine & "La fermeture a été annulée, veuillez attendre la fin de la tâche !")
                        Info.ShowDialog()
                    End Using
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub FrmTask_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        If m_RestoreState Then
            Try
                If File.Exists(Path.Combine(m_TaskStore, "USMT\USMT.MIG")) Then
                    My.Computer.FileSystem.RenameFile(Path.Combine(m_TaskStore, "USMT\USMT.MIG"), New FileInfo(m_StoreOriginalName).Name)
                    For i As Integer = 0 To 99
                        Dim numval = If(i.ToString.Length = 1, "0", "") & i.ToString
                        If File.Exists(Path.Combine(m_TaskStore, "USMT\USMT.MIG" & numval)) Then
                            Try
                                File.Delete(Path.Combine(m_TaskStore, "USMT\USMT.MIG" & numval))
                            Catch ex As Exception
                            End Try
                        End If
                    Next
                End If
            Catch ex As Exception
            End Try
        End If
        DialogResult = DialogResult.OK
    End Sub
#End Region

#Region " Methods "
    Private Sub StartTask()
        Try
            If File.Exists(Path.Combine(m_UsmtRootPath, m_TaskName & ".exe")) Then

                Try
                    Directory.Delete(m_StoreOriginalName, True)
                Catch ex As Exception
                End Try

                m_TaskOngoing = True

                m_fsWatcher = New Watcher(m_UsmtRootPath, m_TaskName & "Progress.log", m_RestoreState)
                AddHandler m_fsWatcher.OutputFsLine, AddressOf m_TaskFs_OutputLine
                m_fsWatcher.StartWatch()

                m_TaskCmd = New Cmd(Path.Combine(m_UsmtRootPath, m_TaskName & ".exe"), CommandArgsFormat())
                AddHandler m_TaskCmd.Aborted, AddressOf m_TaskCmd_Aborted
                AddHandler m_TaskCmd.Exited, AddressOf m_TaskCmd_Exited
                AddHandler m_TaskCmd.OutputLine, AddressOf m_TaskCmd_OutputLine
                m_TaskCmd.Start()
            End If
        Catch ex As Exception
            MsgBox("StartTask Exception :" & ex.ToString)
        End Try
    End Sub

    Private Function CommandArgsFormat() As String
        Dim AccNames As String = String.Empty
        Dim AccContainsLocal As Boolean

        If m_RestoreState Then
            For Each user In m_TaskUsers
                If user.Split("|")(2) = "True" Then
                    If AccContainsLocal = False Then
                        If user.Split("|")(1) = "Local" Then
                            AccContainsLocal = True
                        End If
                    End If
                    AccNames &= " /ui:" & user.Split("|")(0)
                End If
            Next
        Else
            For Each user In m_TaskUsers
                If AccContainsLocal = False Then
                    If user.Split("|")(1) = "Local" Then
                        AccContainsLocal = True
                    End If
                End If
                AccNames &= " /ui:" & user.Split("|")(0)
            Next
        End If

        For i As Integer = 0 To m_XmlMigFiles.Count - 1
            If File.Exists(m_XmlMigFiles(i)) Then
                If i = 0 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "miguser.xml" Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                ElseIf i = 1 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "migapp.xml" Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                ElseIf i = 2 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "migdocs.xml" Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                ElseIf i = 3 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "config.xml" Then
                        m_XmlMigFilesInLine &= " /config:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                ElseIf i = 4 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "excludedrives.xml" Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                ElseIf i = 5 Then
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower = "excludesystem.xml" Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                Else
                    If Path.GetFileName(m_XmlMigFiles(i)).ToLower.EndsWith(".xml") Then
                        m_XmlMigFilesInLine &= " /i:" & Chr(34) & m_XmlMigFiles(i) & Chr(34)
                    End If
                End If
            End If
        Next

        Dim Encrypt = If(m_TaskPwd <> "", If(m_RestoreState, " /decrypt", " /encrypt") & " /key:" & Chr(34) & m_TaskPwd & Chr(34), "")
        Dim ListFiles = If(m_RestoreState, "", " /listfiles:" & Chr(34) & Path.Combine(m_UsmtRootPath, m_TaskName & "ListFiles.txt") & Chr(34))
        'Dim VolShadowCopy_LocalOnly_Overwrite = If(m_RestoreState, "", " /vsc /localonly /o")
        Dim VolShadowCopy_LocalOnly_Overwrite = If(m_RestoreState, "", " /vsc /o")
        Dim LacLae = If(m_RestoreState, If(AccContainsLocal, " /lac /lae", ""), "")

        Return Chr(34) & m_TaskStore & Chr(34) & AccNames & " /ue:*\*" & LacLae & m_XmlMigFilesInLine & Encrypt & " /Progress:" & Chr(34) & Path.Combine(m_UsmtRootPath, m_TaskName & "Progress.log") & Chr(34) & " /l:" & Chr(34) & Path.Combine(m_UsmtRootPath, m_TaskName & ".log") & Chr(34) & ListFiles & " /v:13" & VolShadowCopy_LocalOnly_Overwrite & " /c"
    End Function

    Private Sub UpdateCmd(Str As String)
        RtbTaskProgressOutput.AppendText(Str)
        Dim totalLines As Integer = RtbTaskProgressOutput.Lines.Length
        Dim lastLine As String = RtbTaskProgressOutput.Lines(totalLines - 1)

        If lastLine.Contains("Examining the system ") Then
            LblTaskProgress.Text = "Progression " & m_TaskName & " : Analyse ..."
        ElseIf lastLine.Contains("Estimating total file size ") Then
            LblTaskProgress.Text = "Progression " & m_TaskName & " : Estimation ..."
        ElseIf lastLine.Contains(If(m_RestoreState, "Applying data", "Gathering data")) Then
            LblTaskProgress.Text = "Progression " & m_TaskName & If(m_RestoreState, " : Restauration ...", " : Collecte ...")
        ElseIf lastLine.Contains("Failed.") Then
            LblTaskProgress.Text = "Progression " & m_TaskName & " : Failed."
        ElseIf lastLine.Contains("Success.") Then
            LblTaskProgress.Text = "Progression " & m_TaskName & " : Success."
        End If
    End Sub

    Private Sub m_TaskFs_OutputLine(ender As Object, e As Watcher.OutputFsLineEventArgs)
        If RtbTaskProgress.InvokeRequired Then
            If e.Clear Then
                RtbTaskProgress.Invoke(New MethodInvoker(Sub()
                                                             RtbTaskProgress.Clear()
                                                         End Sub))
            Else
                RtbTaskProgress.Invoke(New UpdateFsDelegate(AddressOf UpdateFileSystemWatcherState), e.Text)
            End If
        End If
    End Sub

    Private Sub UpdateFileSystemWatcherState(Str As String)
        If Not Str.ToLower.Contains("program, " & m_TaskName.ToLower & ".exe, productversion, ") Then
            If Str.Contains("PHASE,") OrElse Str.Contains("processingUser") OrElse (Str.Contains("detectedUser") AndAlso Str.Contains("includedInMigration, Yes")) OrElse (Str.Contains("forUser") AndAlso Str.Contains("includedInMigration, Yes")) OrElse Str.Contains("totalSizeInMBToTransfer") OrElse (Str.Contains("errorCode, ") AndAlso Str.Contains("message, ")) Then
                RtbTaskProgress.AppendText(Str)

                If Str.Contains("PHASE, Initializing") Then
                    PgbTaskProgress.Value = 5
                ElseIf Str.Contains("PHASE, Scanning") Then
                    PgbTaskProgress.Value = 10
                ElseIf Str.Contains("PHASE, Estimating") Then
                    PgbTaskProgress.Value = 15
                ElseIf Str.Contains("PHASE, " & If(m_RestoreState, "Applying", "Collecting")) Then
                    m_applyingCollectingFound = True
                ElseIf Str.Contains("totalSizeInMBToTransfer") Then
                    Dim splitted = Str.Split(",")
                    m_SizeMbToTransfert = splitted(4).Trim
                End If
            End If
        End If

        If m_applyingCollectingFound Then
            If Str.Contains("totalPercentageCompleted") Then
                Dim splitted = Str.Split(",")
                Dim perc = splitted(4).Trim
                If IsNumeric(perc) Then
                    Dim Percentage = CInt(perc)
                    If Percentage >= 15 Then
                        PgbTaskProgress.Value = Percentage
                    ElseIf Percentage = 100 Then
                        m_ExecutionTotalTime = splitted(2).Trim
                    End If
                End If
            End If
        End If

        If Str.Contains("USMT ERROR SUMMARY") Then
            RtbTaskProgress.AppendText(Str)
        End If
    End Sub

    Private Sub m_TaskCmd_OutputLine(ender As Object, e As OutputLineEventArgs)
        If RtbTaskProgressOutput.InvokeRequired Then
            RtbTaskProgressOutput.Invoke(New UpdateCmdDelegate(AddressOf UpdateCmd), e.Text)
        End If
    End Sub

    Private Sub m_TaskCmd_Aborted(sender As Object, e As AbortedEventArgs)
        m_Aborted = True
    End Sub

    Private Sub m_TaskCmd_Exited(ender As Object, e As ExitedEventArgs)
        m_TaskOngoing = False
        Try
            If Not e.ExitCode = -1 Then
                If File.Exists(Path.Combine(m_UsmtRootPath, m_TaskName & ".log")) Then
                    Dim Lines() = File.ReadAllLines(Path.Combine(m_UsmtRootPath, m_TaskName & ".log"))
                    Dim Summaryidx As Integer
                    Dim SummaryText As String = ""
                    For i As Integer = 0 To Lines.Count - 1
                        If Lines(i).Contains("USMT ERROR SUMMARY") Then
                            Summaryidx = i
                            Exit For
                        End If
                    Next
                    For i As Integer = Summaryidx To Lines.Count - 1
                        m_SummaryUsmt &= Lines(i).Substring(Lines(i).IndexOf("]") + 1) & vbNewLine
                    Next
                    If RtbTaskProgress.InvokeRequired Then
                        RtbTaskProgress.Invoke(New UpdateFsDelegate(AddressOf UpdateFileSystemWatcherState), m_SummaryUsmt)
                    End If

                    If LblTaskProgress.InvokeRequired Then
                        LblTaskProgress.Invoke(New MethodInvoker(Sub()
                                                                     If e.ExitCode = 0 OrElse e.ExitCode = 3 OrElse e.ExitCode = 61 Then
                                                                         LblTaskProgress.Text = "Progression " & m_TaskName & " : Success."
                                                                     Else
                                                                         LblTaskProgress.Text = "Progression " & m_TaskName & " : Failed."
                                                                     End If
                                                                 End Sub))
                    End If
                End If

                If m_RestoreState Then
                    If e.ExitCode = UsmtExitCodes.USMT_SUCCESS OrElse
                        e.ExitCode = UsmtExitCodes.USMT_WOULD_HAVE_FAILED OrElse
                         e.ExitCode = UsmtExitCodes.USMT_MIGRATION_STOPPED_NONFATAL Then

                        If e.ExitCode = UsmtExitCodes.USMT_WOULD_HAVE_FAILED OrElse
                            e.ExitCode = UsmtExitCodes.USMT_MIGRATION_STOPPED_NONFATAL Then
                            If InvokeRequired Then
                                Invoke(New MethodInvoker(Sub()
                                                             Dim win32 = New WindowWrapper(Me)
                                                             Using Info As New FrmInfo("Restauration terminée avec Warning", "Le fichier a été restauré avec succès.")
                                                                 Info.ShowDialog(win32)
                                                             End Using
                                                         End Sub))
                            End If

                        Else
                            If InvokeRequired Then
                                Invoke(New MethodInvoker(Sub()
                                                             Dim win32 = New WindowWrapper(Me)
                                                             Using Info As New FrmInfo("Restauration complète terminée", "Le fichier a été restauré avec succès.")
                                                                 Info.ShowDialog(win32)
                                                             End Using
                                                         End Sub))
                            End If
                        End If
                    Else
                        If InvokeRequired Then
                            Invoke(New MethodInvoker(Sub()
                                                         Dim win32 = New WindowWrapper(Me)
                                                         Using Info As New FrmInfo("Echec restauration", "La restauration a échoué, veuillez vérifier la définition du code retournée : " & vbNewLine & e.ExitCode.ToString)
                                                             Info.ShowDialog(win32)
                                                         End Using
                                                     End Sub))
                        End If
                    End If
                Else
                    If e.ExitCode = UsmtExitCodes.USMT_SUCCESS OrElse
                        e.ExitCode = UsmtExitCodes.USMT_WOULD_HAVE_FAILED OrElse
                         e.ExitCode = UsmtExitCodes.USMT_MIGRATION_STOPPED_NONFATAL Then

                        Dim startTime = m_TaskCmd.StartTime.ToString("yyyyMMddHHmmss")
                        Dim differenceTime = m_TaskCmd.ExitTime - m_TaskCmd.StartTime
                        Dim migFilePath = Path.Combine(m_TaskStore, "USMT\USMT.MIG")
                        Dim migFileRootPath = Path.Combine(m_TaskStore, "USMT")
                        Dim migFileWithoutExt = "USMTMIG63" & "_" & startTime
                        Dim MigFileName = migFileWithoutExt & ".MIG"
                        If File.Exists(migFilePath) Then

                            My.Computer.FileSystem.RenameFile(migFilePath, MigFileName)

                            Dim CaptureMigContent = New MigContent With {
                                .ScanDuration = differenceTime.Hours & ":" & differenceTime.Minutes & ":" & differenceTime.Seconds,
                                .SizeMBToTransfert = m_SizeMbToTransfert,
                                .FromComputerName = Environment.MachineName,
                                .PasswordMd5 = If(m_TaskPwd = String.Empty, "", Mig.GetPasswordMd5Hash(m_TaskPwd))
                            }
                            Dim os = New OperatingSys
                            CaptureMigContent.FromOperatingSystem = os.FullName

                            Dim fCount As Integer
                            Dim AllLines = File.ReadAllLines(Path.Combine(m_UsmtRootPath, m_TaskName & "ListFiles.txt"))
                            Dim Paths As New List(Of PathInfos)
                            For Each line In AllLines
                                Dim pth As New PathInfos
                                With pth
                                    .FullName = line
                                    If File.Exists(line) Then
                                        pth.IsFile = True
                                        fCount += 1
                                    Else
                                        pth.IsFile = False
                                    End If
                                End With
                                Paths.Add(pth)
                            Next
                            CaptureMigContent.FilesCount = fCount

                            For i As Integer = 0 To m_XmlMigFiles.Count - 1
                                If File.Exists(m_XmlMigFiles(i)) Then
                                    File.Copy(m_XmlMigFiles(i), Path.Combine(migFileRootPath, New FileInfo(m_XmlMigFiles(i)).Name), True)
                                    m_XmlMigFiles(i) = New FileInfo(m_XmlMigFiles(i)).Name
                                End If
                            Next
                            CaptureMigContent.XmlFiles = m_XmlMigFiles

                            Dim Accounts As New List(Of AccountInfos)
                            For Each acc In m_TaskUsers
                                Dim accInfo As New AccountInfos
                                With accInfo
                                    .Name = acc.Split("|")(0)
                                    .Type = acc.Split("|")(1)
                                    .Include = True
                                End With
                                Accounts.Add(accInfo)
                            Next
                            CaptureMigContent.Accounts = Accounts
                            CaptureMigContent.Paths = Paths

                            Dim m_XmlPath = Path.Combine(migFileRootPath, migFileWithoutExt & ".xml")
                            CaptureMigContent.SaveFile(m_XmlPath)

                            Dim MigSysFileOriginalPath = Path.Combine(m_UsmtRootPath, "migsys.dll")
                            Dim MigSysFile = Path.Combine(migFileRootPath, "migsys.dll")

                            File.Copy(MigSysFileOriginalPath, MigSysFile, True)

                            If e.ExitCode = UsmtExitCodes.USMT_WOULD_HAVE_FAILED OrElse
                                e.ExitCode = UsmtExitCodes.USMT_MIGRATION_STOPPED_NONFATAL Then
                                If InvokeRequired Then
                                    Invoke(New MethodInvoker(Sub()
                                                                 Dim win32 = New WindowWrapper(Me)
                                                                 Using Info As New FrmInfo("Capture terminée avec Warning", "Des fichiers ont été créé (.MIG, .XML et .DLL)." & vbNewLine & "Veillez à ne pas les renommer et à les laisser cote à cote !")
                                                                     Info.ShowDialog(win32)
                                                                 End Using
                                                             End Sub))
                                End If
                            Else
                                If InvokeRequired Then
                                    Invoke(New MethodInvoker(Sub()
                                                                 Dim win32 = New WindowWrapper(Me)
                                                                 Using Info As New FrmInfo("Capture complète terminée", "Des fichiers ont été créé (.MIG, .XML et .DLL)." & vbNewLine & "Veillez à ne pas les renommer et à les laisser cote à cote !")
                                                                     Info.ShowDialog(win32)
                                                                 End Using
                                                             End Sub))
                                End If
                            End If
                        End If
                    Else

                        If InvokeRequired Then
                            Invoke(New MethodInvoker(Sub()
                                                         Dim win32 = New WindowWrapper(Me)
                                                         Using Info As New FrmInfo("Echec capture", "La capture a échoué, veuillez vérifier la définition du code retournée : " & vbNewLine & e.ExitCode.ToString)
                                                             Info.ShowDialog(win32)
                                                         End Using
                                                     End Sub))
                        End If
                    End If
                End If
                If Not m_fsWatcher Is Nothing Then
                    m_fsWatcher.StopWatch()
                End If
            Else
                m_TaskCmd.Close()
                m_TaskCmd = Nothing
                If Me.IsHandleCreated Then
                    Me.Invoke(New MethodInvoker(Sub()
                                                    Me.Close()
                                                End Sub))
                Else
                    Try
                        If File.Exists(Path.Combine(m_TaskStore, "USMT\USMT.MIG")) Then
                            My.Computer.FileSystem.RenameFile(Path.Combine(m_TaskStore, "USMT\USMT.MIG"), New FileInfo(m_StoreOriginalName).Name)
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If
        Catch ex As Exception
            'MsgBox("m_TaskCmd_Exited Exception : " & vbNewLine & ex.ToString)
        End Try

    End Sub
#End Region

End Class
