﻿Imports MetroFramework.Forms
Public Class FrmInfo
    Inherits MetroForm

    Public Sub New(Title As String, Message As String, Optional ByVal ShowButtons As Boolean = False)
        InitializeComponent()
        If ShowButtons Then
            BtnInfoValid.Visible = True
            BtnInfoCancel.Visible = True
            ControlBox = False
            StartPosition = FormStartPosition.CenterParent
        End If
        Text = Title
        LblInfosMessage.Text = Message
    End Sub

    Private Sub BtnInfoValid_Click(sender As Object, e As EventArgs) Handles BtnInfoValid.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub BtnInfoCancel_Click(sender As Object, e As EventArgs) Handles BtnInfoCancel.Click
        DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class