﻿Imports System.IO
Imports MetroFramework.Forms
Imports USMT_Mig63.Core
Imports USMT_Mig63.Engine
Imports USMT_Mig63.Helper
Imports USMT_Mig63.Win32

Public Class FrmMigView
    Inherits MetroForm

#Region " Fields "
    Private m_migview As MigContent
    Private m_numElements As Integer = 0
    Private m_Paths As List(Of PathInfos)
#End Region

#Region " Constructor "
    Public Sub New(migView As MigContent, CreationDate As Date, fSize As Long)
        InitializeComponent()
        m_migview = migView
        LblMigInfosDate.Text &= " le " & CreationDate.ToString("dd/MM/yyyy") & " à " & CreationDate.ToString("HH:mm:ss")
        LblMigInfosSize.Text &= " " & Utils.StrFormatByteSize(fSize)
        m_numElements = migView.Paths.Count
        LblMigInfosCountFiles.Text &= " " & migView.FilesCount.ToString
        LblMigInfosOS.Text &= " " & migView.FromOperatingSystem
        LblMigInfosProtected.Text &= " " & If(migView.PasswordMd5 = String.Empty, "Non", "Oui")
        LblMigInfosMachineName.Text &= " " & migView.FromComputerName
        LblMigInfosScanDuration.Text &= " " & migView.ScanDuration
        LblMigInfosAccounts.Text &= " " & migView.Accounts.Count.ToString
        m_Paths = migView.Paths
    End Sub
#End Region

#Region " Methods "
    Private Sub FrmMigView_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If Not BgwFill.IsBusy Then
            BgwFill.RunWorkerAsync(m_Paths)
        End If
    End Sub

    Private Sub FrmMigView_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If BgwFill.IsBusy Then
            e.Cancel = True
            BgwFill.CancelAsync()
        End If
    End Sub

    Private Sub BgwFill_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BgwFill.DoWork
        Try
            Dim paths As List(Of PathInfos) = e.Argument

            Dim iElementCount As Integer = 0
            Dim lastNode As TreeNode = Nothing
            Dim subPathAgg As String

            For Each fPath In paths
                subPathAgg = String.Empty

                Dim fPaths = fPath.FullName.Split("\")
                Dim Levels = fPaths.Count
                Dim level As Integer = 0
                Dim isFile As Boolean = fPath.IsFile

                For Each subPath As String In fPaths

                    If BgwFill.CancellationPending Then
                        e.Cancel = True
                        Exit Sub
                    End If

                    subPathAgg &= subPath & "\"

                    Dim nodes As TreeNode() = Nothing

                    If TvView.InvokeRequired Then
                        TvView.Invoke(New MethodInvoker(Sub()
                                                            nodes = TvView.Nodes.Find(subPathAgg, True)
                                                        End Sub))
                    Else
                        nodes = TvView.Nodes.Find(subPathAgg, True)
                    End If

                    If nodes.Length = 0 Then
                        If lastNode Is Nothing Then
                            If TvView.InvokeRequired Then
                                TvView.Invoke(New MethodInvoker(Sub()
                                                                    lastNode = TvView.Nodes.Add(subPathAgg, subPath)
                                                                    lastNode.SelectedImageIndex = 3
                                                                    lastNode.ImageIndex = 3
                                                                    lastNode.Tag = 2 '1 = est un lecteur
                                                                End Sub))
                            Else
                                lastNode = TvView.Nodes.Add(subPathAgg, subPath)
                                lastNode.SelectedImageIndex = 3
                                lastNode.ImageIndex = 3
                                lastNode.Tag = 2 '1 = est un lecteur
                            End If
                        Else

                            If TvView.InvokeRequired Then
                                TvView.Invoke(New MethodInvoker(Sub()
                                                                    lastNode = lastNode.Nodes.Add(subPathAgg, subPath)
                                                                    iElementCount += 1
                                                                End Sub))
                            Else
                                lastNode = lastNode.Nodes.Add(subPathAgg, subPath)
                            End If

                            If isFile And (level + 1) = Levels Then
                                If lastNode.Name.Contains(".") Then
                                    Dim newpathwithoutAntiSlash = lastNode.Name.TrimEnd("\")
                                    Dim fi As New FileInfo(newpathwithoutAntiSlash)
                                    Dim keyname = fi.Extension

                                    Try
                                        Dim ContainsKey As Boolean

                                        If TvView.InvokeRequired Then
                                            TvView.Invoke(New MethodInvoker(Sub()
                                                                                ContainsKey = ImlMigview.Images.ContainsKey(keyname)
                                                                            End Sub))
                                        Else
                                            ContainsKey = ImlMigview.Images.ContainsKey(keyname)
                                        End If

                                        If ContainsKey = False Then
                                            Dim ic As Icon = Utils.IconFromExtensionShell(keyname, NativeEnum.IconSize.SmallIcon)
                                            If Not ic Is Nothing Then
                                                If TvView.InvokeRequired Then
                                                    TvView.Invoke(New MethodInvoker(Sub()
                                                                                        ImlMigview.Images.Add(ic.ToBitmap)
                                                                                        Dim idx = ImlMigview.Images.Count - 1
                                                                                        lastNode.SelectedImageKey = keyname
                                                                                        lastNode.SelectedImageIndex = idx
                                                                                        lastNode.ImageKey = keyname
                                                                                        lastNode.ImageIndex = idx
                                                                                        ImlMigview.Images.SetKeyName(idx, keyname)
                                                                                    End Sub))
                                                Else
                                                    ImlMigview.Images.Add(ic.ToBitmap)
                                                    Dim idx = ImlMigview.Images.Count - 1
                                                    lastNode.SelectedImageKey = keyname
                                                    lastNode.SelectedImageIndex = idx
                                                    lastNode.ImageKey = keyname
                                                    lastNode.ImageIndex = idx
                                                    ImlMigview.Images.SetKeyName(idx, keyname)
                                                End If
                                            End If
                                        Else
                                            If TvView.InvokeRequired Then
                                                TvView.Invoke(New MethodInvoker(Sub()
                                                                                    Dim idx = ImlMigview.Images.IndexOfKey(keyname)
                                                                                    lastNode.SelectedImageKey = keyname
                                                                                    lastNode.SelectedImageIndex = idx
                                                                                    lastNode.ImageKey = keyname
                                                                                    lastNode.ImageIndex = idx
                                                                                End Sub))
                                            Else
                                                Dim idx = ImlMigview.Images.IndexOfKey(keyname)
                                                lastNode.SelectedImageKey = keyname
                                                lastNode.SelectedImageIndex = idx
                                                lastNode.ImageKey = keyname
                                                lastNode.ImageIndex = idx
                                            End If
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.ToString)
                                    End Try
                                Else
                                    If TvView.InvokeRequired Then
                                        TvView.Invoke(New MethodInvoker(Sub()
                                                                            lastNode.SelectedImageIndex = 4
                                                                            lastNode.ImageIndex = 4
                                                                        End Sub))
                                    Else
                                        lastNode.SelectedImageIndex = 4
                                        lastNode.ImageIndex = 4
                                    End If
                                End If

                                If TvView.InvokeRequired Then
                                    TvView.Invoke(New MethodInvoker(Sub()
                                                                        lastNode.Tag = 1 '1 = est un fichier
                                                                    End Sub))
                                Else
                                    lastNode.Tag = 1 '1 = est un fichier
                                End If
                            Else
                                If TvView.InvokeRequired Then
                                    TvView.Invoke(New MethodInvoker(Sub()
                                                                        lastNode.SelectedImageIndex = 1
                                                                        lastNode.ImageIndex = 1
                                                                        lastNode.Tag = 0 '0 = est un répertoire
                                                                    End Sub))
                                Else
                                    lastNode.SelectedImageIndex = 1
                                    lastNode.ImageIndex = 1
                                    lastNode.Tag = 0 '0 = est un répertoire
                                End If
                            End If
                        End If
                    Else
                        If TvView.InvokeRequired Then
                            TvView.Invoke(New MethodInvoker(Sub()
                                                                lastNode = nodes(0)
                                                            End Sub))
                        Else
                            lastNode = nodes(0)
                        End If
                    End If

                    BgwFill.ReportProgress(iElementCount * 100 / m_numElements)

                    'Déplier les noeuds de niveau 0
                    If TvView.InvokeRequired Then
                        TvView.Invoke(New MethodInvoker(Sub()
                                                            If lastNode.Level = 0 Then lastNode.Expand()
                                                        End Sub))
                    Else
                        If lastNode.Level = 0 Then lastNode.Expand()
                    End If

                    level += 1
                Next
                lastNode = Nothing
            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BgwFill_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles BgwFill.ProgressChanged
        If Not e.ProgressPercentage > 100 Then
            PgbViewProgress.Value = e.ProgressPercentage
        End If
    End Sub

    Private Sub BgwFill_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgwFill.RunWorkerCompleted
        If e.Cancelled Then
            Close()
        End If
    End Sub

    Private Sub FrmMigView_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        DialogResult = DialogResult.OK
    End Sub

    Private Sub TvView_AfterExpand(sender As Object, e As TreeViewEventArgs) Handles TvView.AfterExpand
        If e.Node.Tag = 0 Then
            e.Node.ImageIndex = 2
            e.Node.SelectedImageIndex = 2
        End If
    End Sub

    Private Sub TvView_AfterCollapse(sender As Object, e As TreeViewEventArgs) Handles TvView.AfterCollapse
        If e.Node.Tag = 0 Then
            e.Node.ImageIndex = 1
            e.Node.SelectedImageIndex = 1
        End If
    End Sub

#End Region

End Class