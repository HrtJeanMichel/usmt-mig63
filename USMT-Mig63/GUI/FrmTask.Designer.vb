﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTask
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTask))
        Me.TtInfos = New System.Windows.Forms.ToolTip(Me.components)
        Me.PnlTaskCommand = New MetroFramework.Controls.MetroPanel()
        Me.TxbTaskCommand = New MetroFramework.Controls.MetroTextBox()
        Me.LblTaskCommand = New MetroFramework.Controls.MetroLabel()
        Me.RtbTaskProgressOutput = New System.Windows.Forms.RichTextBox()
        Me.PgbTaskProgress = New MetroFramework.Controls.MetroProgressBar()
        Me.RtbTaskProgress = New System.Windows.Forms.RichTextBox()
        Me.PnlTaskProgress = New MetroFramework.Controls.MetroPanel()
        Me.LblTaskProgress = New MetroFramework.Controls.MetroLabel()
        Me.PnlTaskCommand.SuspendLayout()
        Me.PnlTaskProgress.SuspendLayout()
        Me.SuspendLayout()
        '
        'TtInfos
        '
        Me.TtInfos.AutomaticDelay = 200
        Me.TtInfos.AutoPopDelay = 20000
        Me.TtInfos.InitialDelay = 200
        Me.TtInfos.ReshowDelay = 40
        '
        'PnlTaskCommand
        '
        Me.PnlTaskCommand.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlTaskCommand.Controls.Add(Me.TxbTaskCommand)
        Me.PnlTaskCommand.Controls.Add(Me.LblTaskCommand)
        Me.PnlTaskCommand.HorizontalScrollbarBarColor = True
        Me.PnlTaskCommand.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlTaskCommand.HorizontalScrollbarSize = 10
        Me.PnlTaskCommand.Location = New System.Drawing.Point(23, 63)
        Me.PnlTaskCommand.Name = "PnlTaskCommand"
        Me.PnlTaskCommand.Size = New System.Drawing.Size(1003, 181)
        Me.PnlTaskCommand.TabIndex = 3
        Me.PnlTaskCommand.VerticalScrollbarBarColor = True
        Me.PnlTaskCommand.VerticalScrollbarHighlightOnWheel = False
        Me.PnlTaskCommand.VerticalScrollbarSize = 10
        '
        'TxbTaskCommand
        '
        Me.TxbTaskCommand.BackColor = System.Drawing.SystemColors.WindowFrame
        '
        '
        '
        Me.TxbTaskCommand.CustomButton.Image = Nothing
        Me.TxbTaskCommand.CustomButton.Location = New System.Drawing.Point(854, 1)
        Me.TxbTaskCommand.CustomButton.Name = ""
        Me.TxbTaskCommand.CustomButton.Size = New System.Drawing.Size(149, 149)
        Me.TxbTaskCommand.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbTaskCommand.CustomButton.TabIndex = 1
        Me.TxbTaskCommand.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbTaskCommand.CustomButton.UseSelectable = True
        Me.TxbTaskCommand.CustomButton.Visible = False
        Me.TxbTaskCommand.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbTaskCommand.ForeColor = System.Drawing.Color.White
        Me.TxbTaskCommand.Lines = New String(-1) {}
        Me.TxbTaskCommand.Location = New System.Drawing.Point(-1, 29)
        Me.TxbTaskCommand.MaxLength = 32767
        Me.TxbTaskCommand.Multiline = True
        Me.TxbTaskCommand.Name = "TxbTaskCommand"
        Me.TxbTaskCommand.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbTaskCommand.ReadOnly = True
        Me.TxbTaskCommand.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxbTaskCommand.SelectedText = ""
        Me.TxbTaskCommand.SelectionLength = 0
        Me.TxbTaskCommand.SelectionStart = 0
        Me.TxbTaskCommand.ShortcutsEnabled = True
        Me.TxbTaskCommand.Size = New System.Drawing.Size(1004, 151)
        Me.TxbTaskCommand.TabIndex = 3
        Me.TxbTaskCommand.UseCustomBackColor = True
        Me.TxbTaskCommand.UseCustomForeColor = True
        Me.TxbTaskCommand.UseSelectable = True
        Me.TxbTaskCommand.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbTaskCommand.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblTaskCommand
        '
        Me.LblTaskCommand.BackColor = System.Drawing.Color.SteelBlue
        Me.LblTaskCommand.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblTaskCommand.ForeColor = System.Drawing.Color.White
        Me.LblTaskCommand.Location = New System.Drawing.Point(0, 0)
        Me.LblTaskCommand.Name = "LblTaskCommand"
        Me.LblTaskCommand.Size = New System.Drawing.Size(1002, 29)
        Me.LblTaskCommand.TabIndex = 2
        Me.LblTaskCommand.Text = "Commande Scanstate"
        Me.LblTaskCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblTaskCommand.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblTaskCommand.UseCustomBackColor = True
        Me.LblTaskCommand.UseCustomForeColor = True
        '
        'RtbTaskProgressOutput
        '
        Me.RtbTaskProgressOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RtbTaskProgressOutput.Location = New System.Drawing.Point(24, 237)
        Me.RtbTaskProgressOutput.Name = "RtbTaskProgressOutput"
        Me.RtbTaskProgressOutput.Size = New System.Drawing.Size(1002, 11)
        Me.RtbTaskProgressOutput.TabIndex = 4
        Me.RtbTaskProgressOutput.Text = ""
        Me.RtbTaskProgressOutput.Visible = False
        '
        'PgbTaskProgress
        '
        Me.PgbTaskProgress.HideProgressText = False
        Me.PgbTaskProgress.Location = New System.Drawing.Point(23, 563)
        Me.PgbTaskProgress.Name = "PgbTaskProgress"
        Me.PgbTaskProgress.Size = New System.Drawing.Size(1003, 32)
        Me.PgbTaskProgress.Style = MetroFramework.MetroColorStyle.SteelBlue
        Me.PgbTaskProgress.TabIndex = 5
        Me.PgbTaskProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'RtbTaskProgress
        '
        Me.RtbTaskProgress.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RtbTaskProgress.Location = New System.Drawing.Point(-2, 28)
        Me.RtbTaskProgress.Name = "RtbTaskProgress"
        Me.RtbTaskProgress.ReadOnly = True
        Me.RtbTaskProgress.Size = New System.Drawing.Size(1004, 277)
        Me.RtbTaskProgress.TabIndex = 7
        Me.RtbTaskProgress.Text = ""
        '
        'PnlTaskProgress
        '
        Me.PnlTaskProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlTaskProgress.Controls.Add(Me.RtbTaskProgress)
        Me.PnlTaskProgress.Controls.Add(Me.LblTaskProgress)
        Me.PnlTaskProgress.HorizontalScrollbarBarColor = True
        Me.PnlTaskProgress.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlTaskProgress.HorizontalScrollbarSize = 10
        Me.PnlTaskProgress.Location = New System.Drawing.Point(23, 250)
        Me.PnlTaskProgress.Name = "PnlTaskProgress"
        Me.PnlTaskProgress.Size = New System.Drawing.Size(1003, 307)
        Me.PnlTaskProgress.TabIndex = 8
        Me.PnlTaskProgress.VerticalScrollbarBarColor = True
        Me.PnlTaskProgress.VerticalScrollbarHighlightOnWheel = False
        Me.PnlTaskProgress.VerticalScrollbarSize = 10
        '
        'LblTaskProgress
        '
        Me.LblTaskProgress.BackColor = System.Drawing.Color.SteelBlue
        Me.LblTaskProgress.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblTaskProgress.ForeColor = System.Drawing.Color.White
        Me.LblTaskProgress.Location = New System.Drawing.Point(0, 0)
        Me.LblTaskProgress.Name = "LblTaskProgress"
        Me.LblTaskProgress.Size = New System.Drawing.Size(1002, 29)
        Me.LblTaskProgress.TabIndex = 2
        Me.LblTaskProgress.Text = "Progression Scanstate"
        Me.LblTaskProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblTaskProgress.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblTaskProgress.UseCustomBackColor = True
        Me.LblTaskProgress.UseCustomForeColor = True
        '
        'FrmTask
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1049, 618)
        Me.Controls.Add(Me.PnlTaskProgress)
        Me.Controls.Add(Me.PgbTaskProgress)
        Me.Controls.Add(Me.PnlTaskCommand)
        Me.Controls.Add(Me.RtbTaskProgressOutput)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmTask"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.SteelBlue
        Me.Text = "USMT Mig63 - Capture"
        Me.PnlTaskCommand.ResumeLayout(False)
        Me.PnlTaskProgress.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TtInfos As ToolTip
    Friend WithEvents PnlTaskCommand As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblTaskCommand As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxbTaskCommand As MetroFramework.Controls.MetroTextBox
    Friend WithEvents RtbTaskProgressOutput As RichTextBox
    Friend WithEvents PgbTaskProgress As MetroFramework.Controls.MetroProgressBar
    Friend WithEvents RtbTaskProgress As RichTextBox
    Friend WithEvents PnlTaskProgress As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblTaskProgress As MetroFramework.Controls.MetroLabel
End Class
