﻿Imports System.IO
Imports System.Xml
Imports MetroFramework.Controls
Imports MetroFramework.Forms
Imports USMT_Mig63.Core
Imports USMT_Mig63.Engine
Imports USMT_Mig63.Helper

Public Class FrmMain
    Inherits MetroForm

#Region " Fields "
    Private m_Manager As Manager
#End Region

#Region " Constructor "
    Public Sub New()
        InitializeComponent()
        m_Manager = New Manager(Me)

    End Sub
#End Region

#Region " Main "
    Private Sub TpCapture_Enter(sender As Object, e As EventArgs) Handles TpCapture.Enter, TpRestore.Enter, TpAbout.Enter
        Dim tp As MetroTabPage = TryCast(sender, MetroTabPage)
        Select Case tp.Name
            Case "TpCapture"
                Style = MetroFramework.MetroColorStyle.SteelBlue
                Invalidate()
                TbcMain.Style = MetroFramework.MetroColorStyle.SteelBlue
                TbcMain.Invalidate()
            Case "TpRestore"
                Style = MetroFramework.MetroColorStyle.IndianRed
                Invalidate()
                TbcMain.Style = MetroFramework.MetroColorStyle.IndianRed
                TbcMain.Invalidate()
            Case "TpAbout"
                Style = MetroFramework.MetroColorStyle.Silver
                Invalidate()
                TbcMain.Style = MetroFramework.MetroColorStyle.Silver
                TbcMain.Invalidate()
                LblAboutDeveloper.Text = My.Application.Info.CompanyName
                LblAboutProgramVersion.Text = My.Application.Info.Version.ToString
                Dim usmt As New Usmt(True)
                LblAboutUsmtVersion.Text = usmt.AdkInfo
        End Select
    End Sub

    Private Sub LblInfos_MouseHover(sender As Object, e As EventArgs) Handles LblCaptureUsmtPath.MouseHover,
                                                                                        LblCaptureSavePath.MouseHover,
                                                                                        LblCaptureOsName.MouseHover,
                                                                                        LblCaptureEncrypt.MouseHover,
                                                                                        LblCaptureUser.MouseHover,
                                                                                        LblRestoreUsmtPath.MouseHover,
                                                                                        LblRestoreFilePath.MouseHover,
                                                                                        LblRestoreOsName.MouseHover,
                                                                                        LblRestoreDecrypt.MouseHover,
                                                                                        LblRestoreUser.MouseHover
        Dim Lbl As MetroLabel = TryCast(sender, MetroLabel)
        m_Manager.ShowLabelInfos(Lbl)
    End Sub

#End Region

#Region " Capture "

    Private Sub BtnCaptureUsmtBrowse_Click(sender As Object, e As EventArgs) Handles BtnCaptureUsmtBrowse.Click
        m_Manager.SettingUSMT()
    End Sub

    Private Sub BtnCaptureSaveBrowse_Click(sender As Object, e As EventArgs) Handles BtnCaptureSaveBrowse.Click
        Using fbd As New FolderBrowserDialog
            With fbd
                .RootFolder = Environment.SpecialFolder.MyComputer
                .Description = "Sélectionnez l'emplacement de sauvegarde :"
                .ShowNewFolderButton = False

                If Directory.Exists(LblCaptureSavePath.Text) Then
                    .SelectedPath = If(Utils.IsDirectoryPath(LblCaptureSavePath.Text), LblCaptureSavePath.Text, Environment.SpecialFolder.MyComputer.ToString)
                Else
                    .SelectedPath = My.Application.Info.DirectoryPath
                End If

                Dim OldSelected = .SelectedPath
                If .ShowDialog = DialogResult.OK Then
                    m_Manager.CaptureSaveBrowse(OldSelected, .SelectedPath)
                End If
            End With
        End Using
    End Sub

    Private Sub BtnCaptureEncrypt_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncrypt.Click
        m_Manager.SettingCaptureEncrypt()
    End Sub

    Private Sub BtnCaptureUsers_Click(sender As Object, e As EventArgs) Handles BtnCaptureUsers.Click
        m_Manager.SettingCaptureUsers()
    End Sub

    Private Sub BtnCapture_Click(sender As Object, e As EventArgs) Handles BtnCapture.Click
        m_Manager.ApplyCapture()
    End Sub
#End Region

#Region " Restore "
    Private Sub BtnInfos_MouseHover(sender As Object, e As EventArgs) Handles BtnRestoreUsmtBrowse.MouseHover,
                                                                              BtnCaptureUsmtBrowse.MouseHover,
                                                                              BtnRestoreFile.MouseHover,
                                                                              BtnCaptureSaveBrowse.MouseHover,
                                                                              BtnRestoreDecrypt.MouseHover,
                                                                              BtnCaptureEncrypt.MouseHover,
                                                                              BtnRestoreUsers.MouseHover,
                                                                              BtnCaptureUsers.MouseHover,
                                                                              BtnRestoreFileInfos.MouseHover
        Dim btn As Button = TryCast(sender, Button)
        Select Case btn.Name
            Case "BtnCaptureUsmtBrowse", "BtnRestoreUsmtBrowse"
                m_Manager.ShowUSMTInfos(btn, (btn.Name = "BtnRestoreUsmtBrowse"))
            Case "BtnCaptureSaveBrowse", "BtnRestoreFile"
                m_Manager.ShowStoreInfos(btn, (btn.Name = "BtnRestoreFile"))
            Case "BtnCaptureEncrypt", "BtnRestoreDecrypt"
                m_Manager.ShowCryptInfos(btn, (btn.Name = "BtnRestoreDecrypt"))
            Case "BtnCaptureUsers", "BtnRestoreUsers"
                m_Manager.ShowUsersInfos(btn, (btn.Name = "BtnRestoreUsers"))
            Case "BtnRestoreFileInfos"
                m_Manager.ShowMigInfos(btn)
        End Select

    End Sub

    Private Sub BtnRestoreUsmtBrowse_Click(sender As Object, e As EventArgs) Handles BtnRestoreUsmtBrowse.Click
        m_Manager.SettingUSMT(True)
    End Sub

    Private Sub BtnRestoreFile_Click(sender As Object, e As EventArgs) Handles BtnRestoreFile.Click
        Using ofd = New OpenFileDialog
            With ofd
                .Title = "Sélectionnez votre fichier de migration :"
                .Filter = "Fichier de migration|*.mig;*.mig"
                .CheckFileExists = True
                .Multiselect = False
                .InitialDirectory = Environment.SpecialFolder.MyComputer.ToString
                If .ShowDialog() = DialogResult.OK Then
                    m_Manager.LoadMigFileInfos(.FileName)
                End If
            End With
        End Using
    End Sub

    Private Sub BtnRestoreDecrypt_Click(sender As Object, e As EventArgs) Handles BtnRestoreDecrypt.Click
        m_Manager.LoadMigFileInfos(LblRestoreFilePath.Text)
    End Sub

    Private Sub BtnRestoreUsers_Click(sender As Object, e As EventArgs) Handles BtnRestoreUsers.Click
        m_Manager.SettingRestoreUsers()
    End Sub

    Private Sub BtnRestoreUsers_EnabledChanged(sender As Object, e As EventArgs) Handles BtnRestoreUsers.EnabledChanged
        BtnRestore.Enabled = If(LblRestoreUser.Text.EndsWith("!"), False, True)
    End Sub

    Private Sub BtnRestore_Click(sender As Object, e As EventArgs) Handles BtnRestore.Click
        m_Manager.ApplyRestore()
    End Sub

    Private Sub BtnRestoreFileInfos_Click(sender As Object, e As EventArgs) Handles BtnRestoreFileInfos.Click
        m_Manager.MigFileInfosViewer()
    End Sub
#End Region

End Class
