﻿Imports MetroFramework.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSettings
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSettings))
        Me.TtInfos = New System.Windows.Forms.ToolTip(Me.components)
        Me.PnlSettingsUsmtMigFilesCustom = New MetroFramework.Controls.MetroPanel()
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2 = New System.Windows.Forms.Button()
        Me.LblSettingsUsmtMigFilesCustomXml2 = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2 = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2 = New MetroFramework.Controls.MetroTextBox()
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1 = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesCustomConfigDel = New System.Windows.Forms.Button()
        Me.LblSettingsUsmtMigFilesCustomXml1 = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1 = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1 = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesExcludeSystem = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesExcludeSystem = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesExcludeSystem = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesExcludeDrives = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesExcludeDrives = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesExcludeDrives = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesCustomConfig = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesCustomConfigPath = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesCustom = New MetroFramework.Controls.MetroLabel()
        Me.PnlSettingsUsmtMigFilesOther = New MetroFramework.Controls.MetroPanel()
        Me.BtnSettingsUsmtMigFilesOtherDocsDel = New System.Windows.Forms.Button()
        Me.LblSettingsUsmtMigFilesOtherDocs = New MetroFramework.Controls.MetroLabel()
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse = New System.Windows.Forms.Button()
        Me.TxbSettingsUsmtMigFilesOtherDocsPath = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesOther = New MetroFramework.Controls.MetroLabel()
        Me.PnlSettingsUsmtMigFiles = New MetroFramework.Controls.MetroPanel()
        Me.BtnSettingsUsmtMigFilesAppDel = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesUserDel = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesAppBrowse = New System.Windows.Forms.Button()
        Me.BtnSettingsUsmtMigFilesUserBrowse = New System.Windows.Forms.Button()
        Me.LblSettingsUsmtMigFilesApp = New MetroFramework.Controls.MetroLabel()
        Me.TxbSettingsUsmtMigFilesAppPath = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFilesUser = New MetroFramework.Controls.MetroLabel()
        Me.TxbSettingsUsmtMigFilesUserPath = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtMigFiles = New MetroFramework.Controls.MetroLabel()
        Me.PnlSettingsUSMT = New MetroFramework.Controls.MetroPanel()
        Me.LblSettingsUsmtRoot = New MetroFramework.Controls.MetroLabel()
        Me.LblSettingsUsmtRootPathAdkInfo = New MetroFramework.Controls.MetroLabel()
        Me.TxbSettingsUsmtRootPath = New MetroFramework.Controls.MetroTextBox()
        Me.LblSettingsUsmtRootPath = New MetroFramework.Controls.MetroLabel()
        Me.BtnCaptureSettingsCancel = New System.Windows.Forms.Button()
        Me.BtnCaptureSettingsValid = New System.Windows.Forms.Button()
        Me.PnlSettingsUsmtMigFilesCustom.SuspendLayout()
        Me.PnlSettingsUsmtMigFilesOther.SuspendLayout()
        Me.PnlSettingsUsmtMigFiles.SuspendLayout()
        Me.PnlSettingsUSMT.SuspendLayout()
        Me.SuspendLayout()
        '
        'TtInfos
        '
        Me.TtInfos.AutomaticDelay = 200
        Me.TtInfos.AutoPopDelay = 20000
        Me.TtInfos.InitialDelay = 200
        Me.TtInfos.ReshowDelay = 40
        '
        'PnlSettingsUsmtMigFilesCustom
        '
        Me.PnlSettingsUsmtMigFilesCustom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomXmlDel2)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesCustomXml2)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.TxbSettingsUsmtMigFilesCustomXmlPath2)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomXmlDel1)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesExcludeSystemDel)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesExcludeDrivesDel)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomConfigDel)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesCustomXml1)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.TxbSettingsUsmtMigFilesCustomXmlPath1)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesExcludeSystem)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesExcludeSystem)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.TxbSettingsUsmtMigFilesExcludeSystem)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesExcludeDrives)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesExcludeDrives)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.TxbSettingsUsmtMigFilesExcludeDrives)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesCustomConfig)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.BtnSettingsUsmtMigFilesCustomConfigBrowse)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.TxbSettingsUsmtMigFilesCustomConfigPath)
        Me.PnlSettingsUsmtMigFilesCustom.Controls.Add(Me.LblSettingsUsmtMigFilesCustom)
        Me.PnlSettingsUsmtMigFilesCustom.HorizontalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFilesCustom.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFilesCustom.HorizontalScrollbarSize = 10
        Me.PnlSettingsUsmtMigFilesCustom.Location = New System.Drawing.Point(23, 354)
        Me.PnlSettingsUsmtMigFilesCustom.Name = "PnlSettingsUsmtMigFilesCustom"
        Me.PnlSettingsUsmtMigFilesCustom.Size = New System.Drawing.Size(671, 190)
        Me.PnlSettingsUsmtMigFilesCustom.TabIndex = 10
        Me.PnlSettingsUsmtMigFilesCustom.VerticalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFilesCustom.VerticalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFilesCustom.VerticalScrollbarSize = 10
        '
        'BtnSettingsUsmtMigFilesCustomXmlDel2
        '
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.Location = New System.Drawing.Point(595, 156)
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.Name = "BtnSettingsUsmtMigFilesCustomXmlDel2"
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.TabIndex = 22
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.Text = "X"
        Me.BtnSettingsUsmtMigFilesCustomXmlDel2.UseVisualStyleBackColor = False
        '
        'LblSettingsUsmtMigFilesCustomXml2
        '
        Me.LblSettingsUsmtMigFilesCustomXml2.Location = New System.Drawing.Point(81, 156)
        Me.LblSettingsUsmtMigFilesCustomXml2.Name = "LblSettingsUsmtMigFilesCustomXml2"
        Me.LblSettingsUsmtMigFilesCustomXml2.Size = New System.Drawing.Size(46, 23)
        Me.LblSettingsUsmtMigFilesCustomXml2.TabIndex = 21
        Me.LblSettingsUsmtMigFilesCustomXml2.Text = "*.xml :"
        Me.LblSettingsUsmtMigFilesCustomXml2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesCustomXmlBrowse2
        '
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.Location = New System.Drawing.Point(621, 156)
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.Name = "BtnSettingsUsmtMigFilesCustomXmlBrowse2"
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.TabIndex = 20
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.Text = "..."
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse2.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesCustomXmlPath2
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.Location = New System.Drawing.Point(133, 156)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.Name = "TxbSettingsUsmtMigFilesCustomXmlPath2"
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.TabIndex = 19
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.TabStop = False
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesCustomXmlPath2.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'BtnSettingsUsmtMigFilesCustomXmlDel1
        '
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.Location = New System.Drawing.Point(595, 127)
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.Name = "BtnSettingsUsmtMigFilesCustomXmlDel1"
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.TabIndex = 18
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.Text = "X"
        Me.BtnSettingsUsmtMigFilesCustomXmlDel1.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesExcludeSystemDel
        '
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.Location = New System.Drawing.Point(595, 98)
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.Name = "BtnSettingsUsmtMigFilesExcludeSystemDel"
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.TabIndex = 17
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesExcludeSystemDel.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesExcludeDrivesDel
        '
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.Location = New System.Drawing.Point(595, 69)
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.Name = "BtnSettingsUsmtMigFilesExcludeDrivesDel"
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.TabIndex = 16
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesExcludeDrivesDel.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesCustomConfigDel
        '
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.Location = New System.Drawing.Point(595, 40)
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.Name = "BtnSettingsUsmtMigFilesCustomConfigDel"
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.TabIndex = 15
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesCustomConfigDel.UseVisualStyleBackColor = False
        '
        'LblSettingsUsmtMigFilesCustomXml1
        '
        Me.LblSettingsUsmtMigFilesCustomXml1.Location = New System.Drawing.Point(81, 127)
        Me.LblSettingsUsmtMigFilesCustomXml1.Name = "LblSettingsUsmtMigFilesCustomXml1"
        Me.LblSettingsUsmtMigFilesCustomXml1.Size = New System.Drawing.Size(46, 23)
        Me.LblSettingsUsmtMigFilesCustomXml1.TabIndex = 14
        Me.LblSettingsUsmtMigFilesCustomXml1.Text = "*.xml :"
        Me.LblSettingsUsmtMigFilesCustomXml1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesCustomXmlBrowse1
        '
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.Location = New System.Drawing.Point(621, 127)
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.Name = "BtnSettingsUsmtMigFilesCustomXmlBrowse1"
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.TabIndex = 13
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.Text = "..."
        Me.BtnSettingsUsmtMigFilesCustomXmlBrowse1.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesCustomXmlPath1
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.Location = New System.Drawing.Point(133, 127)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.Name = "TxbSettingsUsmtMigFilesCustomXmlPath1"
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.TabIndex = 12
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.TabStop = False
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesCustomXmlPath1.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesExcludeSystem
        '
        Me.LblSettingsUsmtMigFilesExcludeSystem.Location = New System.Drawing.Point(2, 98)
        Me.LblSettingsUsmtMigFilesExcludeSystem.Name = "LblSettingsUsmtMigFilesExcludeSystem"
        Me.LblSettingsUsmtMigFilesExcludeSystem.Size = New System.Drawing.Size(127, 23)
        Me.LblSettingsUsmtMigFilesExcludeSystem.TabIndex = 11
        Me.LblSettingsUsmtMigFilesExcludeSystem.Text = "ExcludeSystem.xml :"
        Me.LblSettingsUsmtMigFilesExcludeSystem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesExcludeSystem
        '
        Me.BtnSettingsUsmtMigFilesExcludeSystem.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesExcludeSystem.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeSystem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeSystem.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesExcludeSystem.Location = New System.Drawing.Point(621, 98)
        Me.BtnSettingsUsmtMigFilesExcludeSystem.Name = "BtnSettingsUsmtMigFilesExcludeSystem"
        Me.BtnSettingsUsmtMigFilesExcludeSystem.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesExcludeSystem.TabIndex = 10
        Me.BtnSettingsUsmtMigFilesExcludeSystem.Text = "..."
        Me.BtnSettingsUsmtMigFilesExcludeSystem.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesExcludeSystem
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesExcludeSystem.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesExcludeSystem.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesExcludeSystem.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesExcludeSystem.Location = New System.Drawing.Point(133, 98)
        Me.TxbSettingsUsmtMigFilesExcludeSystem.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesExcludeSystem.Name = "TxbSettingsUsmtMigFilesExcludeSystem"
        Me.TxbSettingsUsmtMigFilesExcludeSystem.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesExcludeSystem.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesExcludeSystem.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesExcludeSystem.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesExcludeSystem.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesExcludeSystem.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesExcludeSystem.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesExcludeSystem.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesExcludeSystem.TabIndex = 9
        Me.TxbSettingsUsmtMigFilesExcludeSystem.TabStop = False
        Me.TxbSettingsUsmtMigFilesExcludeSystem.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesExcludeSystem.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesExcludeSystem.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesExcludeDrives
        '
        Me.LblSettingsUsmtMigFilesExcludeDrives.Location = New System.Drawing.Point(8, 69)
        Me.LblSettingsUsmtMigFilesExcludeDrives.Name = "LblSettingsUsmtMigFilesExcludeDrives"
        Me.LblSettingsUsmtMigFilesExcludeDrives.Size = New System.Drawing.Size(120, 23)
        Me.LblSettingsUsmtMigFilesExcludeDrives.TabIndex = 8
        Me.LblSettingsUsmtMigFilesExcludeDrives.Text = "ExcludeDrives.xml :"
        Me.LblSettingsUsmtMigFilesExcludeDrives.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesExcludeDrives
        '
        Me.BtnSettingsUsmtMigFilesExcludeDrives.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesExcludeDrives.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeDrives.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesExcludeDrives.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesExcludeDrives.Location = New System.Drawing.Point(621, 69)
        Me.BtnSettingsUsmtMigFilesExcludeDrives.Name = "BtnSettingsUsmtMigFilesExcludeDrives"
        Me.BtnSettingsUsmtMigFilesExcludeDrives.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesExcludeDrives.TabIndex = 7
        Me.BtnSettingsUsmtMigFilesExcludeDrives.Text = "..."
        Me.BtnSettingsUsmtMigFilesExcludeDrives.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesExcludeDrives
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesExcludeDrives.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesExcludeDrives.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesExcludeDrives.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesExcludeDrives.Location = New System.Drawing.Point(133, 69)
        Me.TxbSettingsUsmtMigFilesExcludeDrives.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesExcludeDrives.Name = "TxbSettingsUsmtMigFilesExcludeDrives"
        Me.TxbSettingsUsmtMigFilesExcludeDrives.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesExcludeDrives.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesExcludeDrives.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesExcludeDrives.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesExcludeDrives.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesExcludeDrives.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesExcludeDrives.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesExcludeDrives.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesExcludeDrives.TabIndex = 6
        Me.TxbSettingsUsmtMigFilesExcludeDrives.TabStop = False
        Me.TxbSettingsUsmtMigFilesExcludeDrives.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesExcludeDrives.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesExcludeDrives.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesCustomConfig
        '
        Me.LblSettingsUsmtMigFilesCustomConfig.Location = New System.Drawing.Point(48, 40)
        Me.LblSettingsUsmtMigFilesCustomConfig.Name = "LblSettingsUsmtMigFilesCustomConfig"
        Me.LblSettingsUsmtMigFilesCustomConfig.Size = New System.Drawing.Size(79, 23)
        Me.LblSettingsUsmtMigFilesCustomConfig.TabIndex = 5
        Me.LblSettingsUsmtMigFilesCustomConfig.Text = "Config.xml :"
        Me.LblSettingsUsmtMigFilesCustomConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesCustomConfigBrowse
        '
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.Location = New System.Drawing.Point(621, 40)
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.Name = "BtnSettingsUsmtMigFilesCustomConfigBrowse"
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.TabIndex = 4
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.Text = "..."
        Me.BtnSettingsUsmtMigFilesCustomConfigBrowse.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesCustomConfigPath
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.Location = New System.Drawing.Point(133, 40)
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.Name = "TxbSettingsUsmtMigFilesCustomConfigPath"
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.TabIndex = 3
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.TabStop = False
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesCustomConfigPath.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesCustom
        '
        Me.LblSettingsUsmtMigFilesCustom.BackColor = System.Drawing.Color.SteelBlue
        Me.LblSettingsUsmtMigFilesCustom.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblSettingsUsmtMigFilesCustom.ForeColor = System.Drawing.Color.White
        Me.LblSettingsUsmtMigFilesCustom.Location = New System.Drawing.Point(0, 0)
        Me.LblSettingsUsmtMigFilesCustom.Name = "LblSettingsUsmtMigFilesCustom"
        Me.LblSettingsUsmtMigFilesCustom.Size = New System.Drawing.Size(670, 29)
        Me.LblSettingsUsmtMigFilesCustom.TabIndex = 2
        Me.LblSettingsUsmtMigFilesCustom.Text = "Fichiers customisés de migration (.xml)"
        Me.LblSettingsUsmtMigFilesCustom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblSettingsUsmtMigFilesCustom.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblSettingsUsmtMigFilesCustom.UseCustomBackColor = True
        Me.LblSettingsUsmtMigFilesCustom.UseCustomForeColor = True
        '
        'PnlSettingsUsmtMigFilesOther
        '
        Me.PnlSettingsUsmtMigFilesOther.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlSettingsUsmtMigFilesOther.Controls.Add(Me.BtnSettingsUsmtMigFilesOtherDocsDel)
        Me.PnlSettingsUsmtMigFilesOther.Controls.Add(Me.LblSettingsUsmtMigFilesOtherDocs)
        Me.PnlSettingsUsmtMigFilesOther.Controls.Add(Me.BtnSettingsUsmtMigFilesOtherDocsBrowse)
        Me.PnlSettingsUsmtMigFilesOther.Controls.Add(Me.TxbSettingsUsmtMigFilesOtherDocsPath)
        Me.PnlSettingsUsmtMigFilesOther.Controls.Add(Me.LblSettingsUsmtMigFilesOther)
        Me.PnlSettingsUsmtMigFilesOther.HorizontalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFilesOther.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFilesOther.HorizontalScrollbarSize = 10
        Me.PnlSettingsUsmtMigFilesOther.Location = New System.Drawing.Point(23, 271)
        Me.PnlSettingsUsmtMigFilesOther.Name = "PnlSettingsUsmtMigFilesOther"
        Me.PnlSettingsUsmtMigFilesOther.Size = New System.Drawing.Size(671, 77)
        Me.PnlSettingsUsmtMigFilesOther.TabIndex = 9
        Me.PnlSettingsUsmtMigFilesOther.VerticalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFilesOther.VerticalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFilesOther.VerticalScrollbarSize = 10
        '
        'BtnSettingsUsmtMigFilesOtherDocsDel
        '
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.Location = New System.Drawing.Point(595, 39)
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.Name = "BtnSettingsUsmtMigFilesOtherDocsDel"
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.TabIndex = 13
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesOtherDocsDel.UseVisualStyleBackColor = False
        '
        'LblSettingsUsmtMigFilesOtherDocs
        '
        Me.LblSettingsUsmtMigFilesOtherDocs.Location = New System.Drawing.Point(35, 39)
        Me.LblSettingsUsmtMigFilesOtherDocs.Name = "LblSettingsUsmtMigFilesOtherDocs"
        Me.LblSettingsUsmtMigFilesOtherDocs.Size = New System.Drawing.Size(92, 23)
        Me.LblSettingsUsmtMigFilesOtherDocs.TabIndex = 5
        Me.LblSettingsUsmtMigFilesOtherDocs.Text = "MigDocs.xml :"
        Me.LblSettingsUsmtMigFilesOtherDocs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BtnSettingsUsmtMigFilesOtherDocsBrowse
        '
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.Location = New System.Drawing.Point(621, 39)
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.Name = "BtnSettingsUsmtMigFilesOtherDocsBrowse"
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.TabIndex = 4
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.Text = "..."
        Me.BtnSettingsUsmtMigFilesOtherDocsBrowse.UseVisualStyleBackColor = False
        '
        'TxbSettingsUsmtMigFilesOtherDocsPath
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.Location = New System.Drawing.Point(133, 39)
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.Name = "TxbSettingsUsmtMigFilesOtherDocsPath"
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.TabIndex = 3
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.TabStop = False
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesOtherDocsPath.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesOther
        '
        Me.LblSettingsUsmtMigFilesOther.BackColor = System.Drawing.Color.SteelBlue
        Me.LblSettingsUsmtMigFilesOther.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblSettingsUsmtMigFilesOther.ForeColor = System.Drawing.Color.White
        Me.LblSettingsUsmtMigFilesOther.Location = New System.Drawing.Point(0, 0)
        Me.LblSettingsUsmtMigFilesOther.Name = "LblSettingsUsmtMigFilesOther"
        Me.LblSettingsUsmtMigFilesOther.Size = New System.Drawing.Size(670, 29)
        Me.LblSettingsUsmtMigFilesOther.TabIndex = 2
        Me.LblSettingsUsmtMigFilesOther.Text = "Fichier supplémentaire de migration (.xml)"
        Me.LblSettingsUsmtMigFilesOther.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblSettingsUsmtMigFilesOther.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblSettingsUsmtMigFilesOther.UseCustomBackColor = True
        Me.LblSettingsUsmtMigFilesOther.UseCustomForeColor = True
        '
        'PnlSettingsUsmtMigFiles
        '
        Me.PnlSettingsUsmtMigFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.BtnSettingsUsmtMigFilesAppDel)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.BtnSettingsUsmtMigFilesUserDel)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.BtnSettingsUsmtMigFilesAppBrowse)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.BtnSettingsUsmtMigFilesUserBrowse)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.LblSettingsUsmtMigFilesApp)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.TxbSettingsUsmtMigFilesAppPath)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.LblSettingsUsmtMigFilesUser)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.TxbSettingsUsmtMigFilesUserPath)
        Me.PnlSettingsUsmtMigFiles.Controls.Add(Me.LblSettingsUsmtMigFiles)
        Me.PnlSettingsUsmtMigFiles.HorizontalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFiles.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFiles.HorizontalScrollbarSize = 10
        Me.PnlSettingsUsmtMigFiles.Location = New System.Drawing.Point(23, 159)
        Me.PnlSettingsUsmtMigFiles.Name = "PnlSettingsUsmtMigFiles"
        Me.PnlSettingsUsmtMigFiles.Size = New System.Drawing.Size(671, 106)
        Me.PnlSettingsUsmtMigFiles.TabIndex = 8
        Me.PnlSettingsUsmtMigFiles.VerticalScrollbarBarColor = True
        Me.PnlSettingsUsmtMigFiles.VerticalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUsmtMigFiles.VerticalScrollbarSize = 10
        '
        'BtnSettingsUsmtMigFilesAppDel
        '
        Me.BtnSettingsUsmtMigFilesAppDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesAppDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesAppDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesAppDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesAppDel.Location = New System.Drawing.Point(595, 68)
        Me.BtnSettingsUsmtMigFilesAppDel.Name = "BtnSettingsUsmtMigFilesAppDel"
        Me.BtnSettingsUsmtMigFilesAppDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesAppDel.TabIndex = 13
        Me.BtnSettingsUsmtMigFilesAppDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesAppDel.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesUserDel
        '
        Me.BtnSettingsUsmtMigFilesUserDel.BackColor = System.Drawing.Color.LavenderBlush
        Me.BtnSettingsUsmtMigFilesUserDel.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesUserDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesUserDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesUserDel.Location = New System.Drawing.Point(595, 39)
        Me.BtnSettingsUsmtMigFilesUserDel.Name = "BtnSettingsUsmtMigFilesUserDel"
        Me.BtnSettingsUsmtMigFilesUserDel.Size = New System.Drawing.Size(20, 23)
        Me.BtnSettingsUsmtMigFilesUserDel.TabIndex = 12
        Me.BtnSettingsUsmtMigFilesUserDel.Text = "X"
        Me.BtnSettingsUsmtMigFilesUserDel.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesAppBrowse
        '
        Me.BtnSettingsUsmtMigFilesAppBrowse.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesAppBrowse.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesAppBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesAppBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesAppBrowse.Location = New System.Drawing.Point(621, 68)
        Me.BtnSettingsUsmtMigFilesAppBrowse.Name = "BtnSettingsUsmtMigFilesAppBrowse"
        Me.BtnSettingsUsmtMigFilesAppBrowse.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesAppBrowse.TabIndex = 11
        Me.BtnSettingsUsmtMigFilesAppBrowse.Text = "..."
        Me.BtnSettingsUsmtMigFilesAppBrowse.UseVisualStyleBackColor = False
        '
        'BtnSettingsUsmtMigFilesUserBrowse
        '
        Me.BtnSettingsUsmtMigFilesUserBrowse.BackColor = System.Drawing.Color.AliceBlue
        Me.BtnSettingsUsmtMigFilesUserBrowse.FlatAppearance.BorderColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesUserBrowse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver
        Me.BtnSettingsUsmtMigFilesUserBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSettingsUsmtMigFilesUserBrowse.Location = New System.Drawing.Point(621, 39)
        Me.BtnSettingsUsmtMigFilesUserBrowse.Name = "BtnSettingsUsmtMigFilesUserBrowse"
        Me.BtnSettingsUsmtMigFilesUserBrowse.Size = New System.Drawing.Size(39, 23)
        Me.BtnSettingsUsmtMigFilesUserBrowse.TabIndex = 10
        Me.BtnSettingsUsmtMigFilesUserBrowse.Text = "..."
        Me.BtnSettingsUsmtMigFilesUserBrowse.UseVisualStyleBackColor = False
        '
        'LblSettingsUsmtMigFilesApp
        '
        Me.LblSettingsUsmtMigFilesApp.Location = New System.Drawing.Point(39, 68)
        Me.LblSettingsUsmtMigFilesApp.Name = "LblSettingsUsmtMigFilesApp"
        Me.LblSettingsUsmtMigFilesApp.Size = New System.Drawing.Size(88, 23)
        Me.LblSettingsUsmtMigFilesApp.TabIndex = 8
        Me.LblSettingsUsmtMigFilesApp.Text = "MigApp.xml :"
        Me.LblSettingsUsmtMigFilesApp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxbSettingsUsmtMigFilesAppPath
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesAppPath.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesAppPath.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesAppPath.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesAppPath.Location = New System.Drawing.Point(133, 68)
        Me.TxbSettingsUsmtMigFilesAppPath.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesAppPath.Name = "TxbSettingsUsmtMigFilesAppPath"
        Me.TxbSettingsUsmtMigFilesAppPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesAppPath.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesAppPath.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesAppPath.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesAppPath.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesAppPath.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesAppPath.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesAppPath.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesAppPath.TabIndex = 6
        Me.TxbSettingsUsmtMigFilesAppPath.TabStop = False
        Me.TxbSettingsUsmtMigFilesAppPath.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesAppPath.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesAppPath.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFilesUser
        '
        Me.LblSettingsUsmtMigFilesUser.Location = New System.Drawing.Point(39, 39)
        Me.LblSettingsUsmtMigFilesUser.Name = "LblSettingsUsmtMigFilesUser"
        Me.LblSettingsUsmtMigFilesUser.Size = New System.Drawing.Size(88, 23)
        Me.LblSettingsUsmtMigFilesUser.TabIndex = 5
        Me.LblSettingsUsmtMigFilesUser.Text = "MigUser.xml :"
        Me.LblSettingsUsmtMigFilesUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxbSettingsUsmtMigFilesUserPath
        '
        '
        '
        '
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Name = ""
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesUserPath.CustomButton.Visible = False
        Me.TxbSettingsUsmtMigFilesUserPath.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtMigFilesUserPath.Lines = New String(-1) {}
        Me.TxbSettingsUsmtMigFilesUserPath.Location = New System.Drawing.Point(133, 39)
        Me.TxbSettingsUsmtMigFilesUserPath.MaxLength = 32767
        Me.TxbSettingsUsmtMigFilesUserPath.Name = "TxbSettingsUsmtMigFilesUserPath"
        Me.TxbSettingsUsmtMigFilesUserPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtMigFilesUserPath.ReadOnly = True
        Me.TxbSettingsUsmtMigFilesUserPath.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtMigFilesUserPath.SelectedText = ""
        Me.TxbSettingsUsmtMigFilesUserPath.SelectionLength = 0
        Me.TxbSettingsUsmtMigFilesUserPath.SelectionStart = 0
        Me.TxbSettingsUsmtMigFilesUserPath.ShortcutsEnabled = True
        Me.TxbSettingsUsmtMigFilesUserPath.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtMigFilesUserPath.TabIndex = 3
        Me.TxbSettingsUsmtMigFilesUserPath.TabStop = False
        Me.TxbSettingsUsmtMigFilesUserPath.UseSelectable = True
        Me.TxbSettingsUsmtMigFilesUserPath.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtMigFilesUserPath.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtMigFiles
        '
        Me.LblSettingsUsmtMigFiles.BackColor = System.Drawing.Color.SteelBlue
        Me.LblSettingsUsmtMigFiles.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblSettingsUsmtMigFiles.ForeColor = System.Drawing.Color.White
        Me.LblSettingsUsmtMigFiles.Location = New System.Drawing.Point(0, 0)
        Me.LblSettingsUsmtMigFiles.Name = "LblSettingsUsmtMigFiles"
        Me.LblSettingsUsmtMigFiles.Size = New System.Drawing.Size(670, 29)
        Me.LblSettingsUsmtMigFiles.TabIndex = 2
        Me.LblSettingsUsmtMigFiles.Text = "Fichiers principaux de migration (.xml)"
        Me.LblSettingsUsmtMigFiles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblSettingsUsmtMigFiles.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblSettingsUsmtMigFiles.UseCustomBackColor = True
        Me.LblSettingsUsmtMigFiles.UseCustomForeColor = True
        '
        'PnlSettingsUSMT
        '
        Me.PnlSettingsUSMT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlSettingsUSMT.Controls.Add(Me.LblSettingsUsmtRoot)
        Me.PnlSettingsUSMT.Controls.Add(Me.LblSettingsUsmtRootPathAdkInfo)
        Me.PnlSettingsUSMT.Controls.Add(Me.TxbSettingsUsmtRootPath)
        Me.PnlSettingsUSMT.Controls.Add(Me.LblSettingsUsmtRootPath)
        Me.PnlSettingsUSMT.HorizontalScrollbarBarColor = True
        Me.PnlSettingsUSMT.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUSMT.HorizontalScrollbarSize = 10
        Me.PnlSettingsUSMT.Location = New System.Drawing.Point(23, 63)
        Me.PnlSettingsUSMT.Name = "PnlSettingsUSMT"
        Me.PnlSettingsUSMT.Size = New System.Drawing.Size(671, 90)
        Me.PnlSettingsUSMT.TabIndex = 7
        Me.PnlSettingsUSMT.VerticalScrollbarBarColor = True
        Me.PnlSettingsUSMT.VerticalScrollbarHighlightOnWheel = False
        Me.PnlSettingsUSMT.VerticalScrollbarSize = 10
        '
        'LblSettingsUsmtRoot
        '
        Me.LblSettingsUsmtRoot.Location = New System.Drawing.Point(73, 39)
        Me.LblSettingsUsmtRoot.Name = "LblSettingsUsmtRoot"
        Me.LblSettingsUsmtRoot.Size = New System.Drawing.Size(54, 23)
        Me.LblSettingsUsmtRoot.TabIndex = 6
        Me.LblSettingsUsmtRoot.Text = "USMT :"
        Me.LblSettingsUsmtRoot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblSettingsUsmtRootPathAdkInfo
        '
        Me.LblSettingsUsmtRootPathAdkInfo.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblSettingsUsmtRootPathAdkInfo.ForeColor = System.Drawing.Color.Green
        Me.LblSettingsUsmtRootPathAdkInfo.Location = New System.Drawing.Point(133, 65)
        Me.LblSettingsUsmtRootPathAdkInfo.Name = "LblSettingsUsmtRootPathAdkInfo"
        Me.LblSettingsUsmtRootPathAdkInfo.Size = New System.Drawing.Size(482, 19)
        Me.LblSettingsUsmtRootPathAdkInfo.TabIndex = 5
        Me.LblSettingsUsmtRootPathAdkInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LblSettingsUsmtRootPathAdkInfo.UseCustomForeColor = True
        '
        'TxbSettingsUsmtRootPath
        '
        '
        '
        '
        Me.TxbSettingsUsmtRootPath.CustomButton.Image = Nothing
        Me.TxbSettingsUsmtRootPath.CustomButton.Location = New System.Drawing.Point(434, 1)
        Me.TxbSettingsUsmtRootPath.CustomButton.Name = ""
        Me.TxbSettingsUsmtRootPath.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxbSettingsUsmtRootPath.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxbSettingsUsmtRootPath.CustomButton.TabIndex = 1
        Me.TxbSettingsUsmtRootPath.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxbSettingsUsmtRootPath.CustomButton.UseSelectable = True
        Me.TxbSettingsUsmtRootPath.CustomButton.Visible = False
        Me.TxbSettingsUsmtRootPath.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxbSettingsUsmtRootPath.Lines = New String(-1) {}
        Me.TxbSettingsUsmtRootPath.Location = New System.Drawing.Point(133, 39)
        Me.TxbSettingsUsmtRootPath.MaxLength = 32767
        Me.TxbSettingsUsmtRootPath.Name = "TxbSettingsUsmtRootPath"
        Me.TxbSettingsUsmtRootPath.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxbSettingsUsmtRootPath.ReadOnly = True
        Me.TxbSettingsUsmtRootPath.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxbSettingsUsmtRootPath.SelectedText = ""
        Me.TxbSettingsUsmtRootPath.SelectionLength = 0
        Me.TxbSettingsUsmtRootPath.SelectionStart = 0
        Me.TxbSettingsUsmtRootPath.ShortcutsEnabled = True
        Me.TxbSettingsUsmtRootPath.Size = New System.Drawing.Size(456, 23)
        Me.TxbSettingsUsmtRootPath.TabIndex = 3
        Me.TxbSettingsUsmtRootPath.TabStop = False
        Me.TxbSettingsUsmtRootPath.UseSelectable = True
        Me.TxbSettingsUsmtRootPath.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxbSettingsUsmtRootPath.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'LblSettingsUsmtRootPath
        '
        Me.LblSettingsUsmtRootPath.BackColor = System.Drawing.Color.SteelBlue
        Me.LblSettingsUsmtRootPath.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblSettingsUsmtRootPath.ForeColor = System.Drawing.Color.White
        Me.LblSettingsUsmtRootPath.Location = New System.Drawing.Point(0, 0)
        Me.LblSettingsUsmtRootPath.Name = "LblSettingsUsmtRootPath"
        Me.LblSettingsUsmtRootPath.Size = New System.Drawing.Size(670, 29)
        Me.LblSettingsUsmtRootPath.TabIndex = 2
        Me.LblSettingsUsmtRootPath.Text = "Répertoire racine des outils de migration USMT"
        Me.LblSettingsUsmtRootPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblSettingsUsmtRootPath.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblSettingsUsmtRootPath.UseCustomBackColor = True
        Me.LblSettingsUsmtRootPath.UseCustomForeColor = True
        '
        'BtnCaptureSettingsCancel
        '
        Me.BtnCaptureSettingsCancel.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureSettingsCancel.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Cancel
        Me.BtnCaptureSettingsCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureSettingsCancel.FlatAppearance.BorderSize = 0
        Me.BtnCaptureSettingsCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureSettingsCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureSettingsCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureSettingsCancel.Location = New System.Drawing.Point(671, 557)
        Me.BtnCaptureSettingsCancel.Name = "BtnCaptureSettingsCancel"
        Me.BtnCaptureSettingsCancel.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureSettingsCancel.TabIndex = 18
        Me.BtnCaptureSettingsCancel.UseVisualStyleBackColor = False
        '
        'BtnCaptureSettingsValid
        '
        Me.BtnCaptureSettingsValid.BackColor = System.Drawing.Color.Transparent
        Me.BtnCaptureSettingsValid.BackgroundImage = Global.USMT_Mig63.My.Resources.Resources.Valid
        Me.BtnCaptureSettingsValid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCaptureSettingsValid.FlatAppearance.BorderSize = 0
        Me.BtnCaptureSettingsValid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.BtnCaptureSettingsValid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCaptureSettingsValid.ForeColor = System.Drawing.SystemColors.ControlText
        Me.BtnCaptureSettingsValid.Location = New System.Drawing.Point(635, 557)
        Me.BtnCaptureSettingsValid.Name = "BtnCaptureSettingsValid"
        Me.BtnCaptureSettingsValid.Size = New System.Drawing.Size(30, 20)
        Me.BtnCaptureSettingsValid.TabIndex = 17
        Me.BtnCaptureSettingsValid.UseVisualStyleBackColor = False
        Me.BtnCaptureSettingsValid.Visible = False
        '
        'FrmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(719, 591)
        Me.ControlBox = False
        Me.Controls.Add(Me.BtnCaptureSettingsCancel)
        Me.Controls.Add(Me.BtnCaptureSettingsValid)
        Me.Controls.Add(Me.PnlSettingsUsmtMigFilesCustom)
        Me.Controls.Add(Me.PnlSettingsUsmtMigFilesOther)
        Me.Controls.Add(Me.PnlSettingsUsmtMigFiles)
        Me.Controls.Add(Me.PnlSettingsUSMT)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmSettings"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Style = MetroFramework.MetroColorStyle.SteelBlue
        Me.Text = "Paramètres USMT"
        Me.PnlSettingsUsmtMigFilesCustom.ResumeLayout(False)
        Me.PnlSettingsUsmtMigFilesOther.ResumeLayout(False)
        Me.PnlSettingsUsmtMigFiles.ResumeLayout(False)
        Me.PnlSettingsUSMT.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TtInfos As ToolTip
    Friend WithEvents PnlSettingsUsmtMigFilesCustom As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnSettingsUsmtMigFilesCustomXmlDel2 As Button
    Friend WithEvents LblSettingsUsmtMigFilesCustomXml2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesCustomXmlBrowse2 As Button
    Friend WithEvents TxbSettingsUsmtMigFilesCustomXmlPath2 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents BtnSettingsUsmtMigFilesCustomXmlDel1 As Button
    Friend WithEvents BtnSettingsUsmtMigFilesExcludeSystemDel As Button
    Friend WithEvents BtnSettingsUsmtMigFilesExcludeDrivesDel As Button
    Friend WithEvents BtnSettingsUsmtMigFilesCustomConfigDel As Button
    Friend WithEvents LblSettingsUsmtMigFilesCustomXml1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesCustomXmlBrowse1 As Button
    Friend WithEvents TxbSettingsUsmtMigFilesCustomXmlPath1 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesExcludeSystem As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesExcludeSystem As Button
    Friend WithEvents TxbSettingsUsmtMigFilesExcludeSystem As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesExcludeDrives As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesExcludeDrives As Button
    Friend WithEvents TxbSettingsUsmtMigFilesExcludeDrives As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesCustomConfig As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesCustomConfigBrowse As Button
    Friend WithEvents TxbSettingsUsmtMigFilesCustomConfigPath As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesCustom As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlSettingsUsmtMigFilesOther As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnSettingsUsmtMigFilesOtherDocsDel As Button
    Friend WithEvents LblSettingsUsmtMigFilesOtherDocs As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnSettingsUsmtMigFilesOtherDocsBrowse As Button
    Friend WithEvents TxbSettingsUsmtMigFilesOtherDocsPath As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesOther As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlSettingsUsmtMigFiles As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnSettingsUsmtMigFilesAppDel As Button
    Friend WithEvents BtnSettingsUsmtMigFilesUserDel As Button
    Friend WithEvents BtnSettingsUsmtMigFilesAppBrowse As Button
    Friend WithEvents BtnSettingsUsmtMigFilesUserBrowse As Button
    Friend WithEvents LblSettingsUsmtMigFilesApp As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxbSettingsUsmtMigFilesAppPath As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFilesUser As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxbSettingsUsmtMigFilesUserPath As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtMigFiles As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlSettingsUSMT As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblSettingsUsmtRoot As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblSettingsUsmtRootPathAdkInfo As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxbSettingsUsmtRootPath As MetroFramework.Controls.MetroTextBox
    Friend WithEvents LblSettingsUsmtRootPath As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnCaptureSettingsCancel As Button
    Friend WithEvents BtnCaptureSettingsValid As Button
End Class
