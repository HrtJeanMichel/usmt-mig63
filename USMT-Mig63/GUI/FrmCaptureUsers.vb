﻿Imports System.Net.NetworkInformation
Imports System.Security.Principal
Imports MetroFramework.Controls
Imports MetroFramework.Forms
Imports Microsoft.Win32
Imports USMT_Mig63.Helper

Public Class FrmCaptureUsers
    Inherits MetroForm

    Public Property UserSids As New List(Of String)

    Public Sub New(Usids As List(Of String))
        InitializeComponent()
        UserSids = Usids
        LoadUsers()
        If Not UserSids.Count = 0 Then
            For Each u As MetroListViewItem In LvUsers.Items
                For Each refU In UserSids
                    If u.SubItems(2).Text = refU.Split("|")(2) Then
                        u.Checked = True
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub LoadUsers()
        Using regkey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList")
            For Each rk In regkey.GetSubKeyNames
                If rk.ToUpper.StartsWith("S-1-5-21-") Then
                    If Not rk = Utils.LoggedInSid Then
                        Dim SidSplitted = rk.Split("-")
                        If SidSplitted.Count = 8 Then
                            Dim rid = SidSplitted(7)
                            If Not String.IsNullOrEmpty(rid) AndAlso IsNumeric(rid) Then
                                If CInt(rid) >= 1000 AndAlso CInt(rid) <= 1209 Then
                                    FillListView(regkey, rk, "Local")
                                ElseIf CInt(rid) > 1209 Then
                                    If IPGlobalProperties.GetIPGlobalProperties().DomainName.Trim <> String.Empty Then
                                        FillListView(regkey, rk, "Domaine")
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        End Using
    End Sub

    Private Sub FillListView(regkey As RegistryKey, rk As String, accType As String)
        Dim currentKey = regkey.OpenSubKey(rk)
        Dim str As String() = New String(4) {}
        Dim ProfileImagePath = currentKey.GetValue("ProfileImagePath").ToString
        Dim Sid = rk

        Try
            Dim Secsid As SecurityIdentifier = New SecurityIdentifier(Sid)
            Dim acct As NTAccount = CType(Secsid.Translate(GetType(NTAccount)), NTAccount)

            Dim AccName = acct.Value

            str(0) = AccName
            str(1) = accType
            str(2) = Sid
            str(3) = ProfileImagePath

            Dim lvi As New MetroListViewItem(str)

            LvUsers.Items.Add(lvi)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BtnCaptureEncryptValid_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptValid.Click
        UserSids.Clear()
        For Each u As MetroListViewItem In LvUsers.CheckedItems
            Dim AccName = u.Text
            Dim AccType = u.SubItems(1).Text
            Dim Sid = u.SubItems(2).Text
            Dim ProfileImagePath = u.SubItems(3).Text
            UserSids.Add(AccName & "|" & AccType & "|" & Sid)
        Next
        DialogResult = DialogResult.OK
        Close()
    End Sub

    Private Sub BtnCaptureEncryptCancel_Click(sender As Object, e As EventArgs) Handles BtnCaptureEncryptCancel.Click
        DialogResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub LvUsers_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles LvUsers.ItemCheck
        Dim newList As New List(Of String)
        If e.NewValue = CheckState.Checked Then
            ShowApplyButton(newList, True, e.Index)
        ElseIf e.NewValue = CheckState.Unchecked Then
            ShowApplyButton(newList, False, e.Index)
        End If
    End Sub

    Private Sub ShowApplyButton(newList As List(Of String), Checked As Boolean, index As Integer)
        If Checked Then
            For Each u As MetroListViewItem In LvUsers.CheckedItems
                newList.Add(u.Text & "|" & u.SubItems(1).Text & "|" & u.SubItems(2).Text)
            Next
            Dim v = TryCast(LvUsers.Items(index), MetroListViewItem)
            newList.Add(v.Text & "|" & v.SubItems(1).Text & "|" & v.SubItems(2).Text)
        Else
            For Each u As MetroListViewItem In LvUsers.CheckedItems
                If Not index = u.Index Then
                    newList.Add(u.Text & "|" & u.SubItems(1).Text & "|" & u.SubItems(2).Text)
                End If
            Next
        End If
        If Utils.CompareLists(UserSids, newList) Then
            BtnCaptureEncryptValid.Visible = False
        Else
            BtnCaptureEncryptValid.Visible = True
        End If

    End Sub

End Class