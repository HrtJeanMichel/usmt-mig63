﻿Imports MetroFramework.Controls
Imports MetroFramework.Forms
Imports USMT_Mig63.Core
Imports USMT_Mig63.Helper

Public Class FrmSettings
    Inherits MetroForm
    Implements IDisposable

    Private m_Usmt As Usmt

    Public Sub New(Usmt As Usmt, Optional ByVal isRestore As Boolean = False)
        m_Usmt = Usmt
        InitializeComponent()

        If isRestore Then
            LblSettingsUsmtRootPath.BackColor = Color.IndianRed
            LblSettingsUsmtMigFiles.BackColor = Color.IndianRed
            LblSettingsUsmtMigFilesOther.BackColor = Color.IndianRed
            LblSettingsUsmtMigFilesCustom.BackColor = Color.IndianRed

            Style = MetroFramework.MetroColorStyle.IndianRed
            Invalidate()
        End If

        TxbSettingsUsmtRootPath.Text = Usmt.RootPath
        TxbSettingsUsmtMigFilesUserPath.Text = Usmt.MigUserPath
        TxbSettingsUsmtMigFilesAppPath.Text = Usmt.MigAppPath
        TxbSettingsUsmtMigFilesOtherDocsPath.Text = Usmt.MigDocsPath
        TxbSettingsUsmtMigFilesCustomConfigPath.Text = Usmt.ConfigPath
        TxbSettingsUsmtMigFilesExcludeDrives.Text = Usmt.ExcludeDrivesPath
        TxbSettingsUsmtMigFilesExcludeSystem.Text = Usmt.ExcludeSystemPath
        TxbSettingsUsmtMigFilesCustomXmlPath1.Text = Usmt.Config1Path
        TxbSettingsUsmtMigFilesCustomXmlPath2.Text = Usmt.Config2Path
        LblSettingsUsmtRootPathAdkInfo.Text = Usmt.AdkInfo

        ShowOrHideTab()
    End Sub

    Private Sub BtnSettingsUsmtMigFilesUserPath_Click(sender As Object, e As EventArgs) Handles BtnSettingsUsmtMigFilesUserBrowse.Click,
                                                                                                BtnSettingsUsmtMigFilesAppBrowse.Click,
                                                                                                BtnSettingsUsmtMigFilesOtherDocsBrowse.Click,
                                                                                                BtnSettingsUsmtMigFilesCustomConfigBrowse.Click,
                                                                                                BtnSettingsUsmtMigFilesExcludeDrives.Click,
                                                                                                BtnSettingsUsmtMigFilesExcludeSystem.Click
        Dim Btn As Button = TryCast(sender, Button)
        Dim txb As MetroTextBox = Nothing
        Dim fileName As String = ""
        Select Case Btn.Name
            Case "BtnSettingsUsmtMigFilesUserBrowse"
                txb = TxbSettingsUsmtMigFilesUserPath
                fileName = "MigUser.xml"
            Case "BtnSettingsUsmtMigFilesAppBrowse"
                txb = TxbSettingsUsmtMigFilesAppPath
                fileName = "MigApp.xml"
            Case "BtnSettingsUsmtMigFilesOtherDocsBrowse"
                txb = TxbSettingsUsmtMigFilesOtherDocsPath
                fileName = "MigDocs.xml"
            Case "BtnSettingsUsmtMigFilesCustomConfigBrowse"
                txb = TxbSettingsUsmtMigFilesCustomConfigPath
                fileName = "config.xml"
            Case "BtnSettingsUsmtMigFilesExcludeDrives"
                txb = TxbSettingsUsmtMigFilesExcludeDrives
                fileName = "ExcludeDrives.xml"
            Case "BtnSettingsUsmtMigFilesExcludeSystem"
                txb = TxbSettingsUsmtMigFilesExcludeSystem
                fileName = "ExcludeSystem.xml"
        End Select

        Using ofd = New OpenFileDialog
            With ofd
                .Title = "Sélectionnez votre fichier " & fileName & " :"
                .Filter = fileName & "|*.xml;*.xml"
                .CheckFileExists = True
                .Multiselect = False
                .FileName = fileName
                .ShowReadOnly = True
                .InitialDirectory = TxbSettingsUsmtRootPath.Text
                If .ShowDialog() = DialogResult.OK Then
                    If .SafeFileName.ToLower = fileName.ToLower Then
                        If Utils.XmlFileValidation(.FileName) Then
                            If fileName = "MigDocs.xml" Then
                                If TxbSettingsUsmtMigFilesUserPath.Text <> "" Then
                                    Using Info As New FrmInfo("Attention", "Microsoft recommande de ne pas utiliser conjointement les fichiers :" & vbNewLine & """MigUser.xml"" et ""MigDocs.xml"" !")
                                        Info.ShowDialog()
                                    End Using
                                Else
                                    txb.Text = .FileName
                                    ShowOrHideTab(txb)
                                End If
                            ElseIf fileName = "MigUser.xml" Then
                                If TxbSettingsUsmtMigFilesOtherDocsPath.Text <> "" Then
                                    Using Info As New FrmInfo("Attention", "Microsoft recommande de ne pas utiliser conjointement les fichiers :" & vbNewLine & """MigUser.xml"" et ""MigDocs.xml"" !")
                                        Info.ShowDialog()
                                    End Using
                                Else
                                    txb.Text = .FileName
                                    ShowOrHideTab(txb)
                                End If
                            Else
                                txb.Text = .FileName
                                ShowOrHideTab(txb)
                            End If
                        End If
                    Else
                        Using Info As New FrmInfo("Sélection invalide", "Le nom du fichier doit être """ & fileName & " "" !")
                            Info.ShowDialog()
                        End Using
                    End If
                End If
            End With
        End Using
    End Sub

    Private Sub BtnSettingsUsmtMigFilesCustomXmlBrowse1_Click(sender As Object, e As EventArgs) Handles BtnSettingsUsmtMigFilesCustomXmlBrowse1.Click,
                                                                                                        BtnSettingsUsmtMigFilesCustomXmlBrowse2.Click
        Dim Btn As Button = TryCast(sender, Button)
        Dim txb As MetroTextBox = Nothing
        Select Case Btn.Name
            Case "BtnSettingsUsmtMigFilesCustomXmlBrowse1"
                txb = TxbSettingsUsmtMigFilesCustomXmlPath1
            Case "BtnSettingsUsmtMigFilesCustomXmlBrowse2"
                txb = TxbSettingsUsmtMigFilesCustomXmlPath2
        End Select
        Using ofd = New OpenFileDialog
            With ofd
                .Title = "Sélectionnez votre fichier .xml :"
                .Filter = ".xml|*.xml;*.xml"
                .CheckFileExists = True
                .Multiselect = False
                .InitialDirectory = TxbSettingsUsmtRootPath.Text
                If .ShowDialog() = DialogResult.OK Then
                    If .SafeFileName.ToLower.EndsWith(".xml") Then
                        Dim XmlTextboxes = New MetroTextBox() {TxbSettingsUsmtMigFilesUserPath, TxbSettingsUsmtMigFilesAppPath, TxbSettingsUsmtMigFilesOtherDocsPath, TxbSettingsUsmtMigFilesCustomConfigPath, TxbSettingsUsmtMigFilesExcludeDrives, TxbSettingsUsmtMigFilesExcludeSystem, TxbSettingsUsmtMigFilesCustomXmlPath1}
                        If XmlTextboxes.Any(Function(f) f.Text.ToLower = .FileName.ToLower) Then
                            Using Info As New FrmInfo("Sélection invalide", "Ce fichier de configuration a déjà été sélectionné !")
                                Info.ShowDialog()
                            End Using
                        Else
                            txb.Text = .FileName
                            ShowOrHideTab(txb)
                        End If
                    Else
                        Using Info As New FrmInfo("Sélection invalide", "Le nom du fichier doit se terminer par "".xml"" !")
                            Info.ShowDialog()
                        End Using
                    End If
                End If
            End With
        End Using
    End Sub

    Private Sub BtnSettingsUsmtMigFilesUserDel_Click(sender As Object, e As EventArgs) Handles BtnSettingsUsmtMigFilesUserDel.Click,
                                                                                                BtnSettingsUsmtMigFilesAppDel.Click,
                                                                                                BtnSettingsUsmtMigFilesOtherDocsDel.Click,
                                                                                                BtnSettingsUsmtMigFilesCustomConfigDel.Click,
                                                                                                BtnSettingsUsmtMigFilesExcludeDrivesDel.Click,
                                                                                                BtnSettingsUsmtMigFilesExcludeSystemDel.Click,
                                                                                                BtnSettingsUsmtMigFilesCustomXmlDel1.Click,
                                                                                                BtnSettingsUsmtMigFilesCustomXmlDel2.Click
        Dim Btn As Button = TryCast(sender, Button)
        Dim txb As MetroTextBox = Nothing
        Select Case Btn.Name
            Case "BtnSettingsUsmtMigFilesUserDel"
                txb = TxbSettingsUsmtMigFilesUserPath
            Case "BtnSettingsUsmtMigFilesAppDel"
                txb = TxbSettingsUsmtMigFilesAppPath
            Case "BtnSettingsUsmtMigFilesOtherDocsDel"
                txb = TxbSettingsUsmtMigFilesOtherDocsPath
            Case "BtnSettingsUsmtMigFilesCustomConfigDel"
                txb = TxbSettingsUsmtMigFilesCustomConfigPath
            Case "BtnSettingsUsmtMigFilesExcludeDrivesDel"
                txb = TxbSettingsUsmtMigFilesExcludeDrives
            Case "BtnSettingsUsmtMigFilesExcludeSystemDel"
                txb = TxbSettingsUsmtMigFilesExcludeSystem
            Case "BtnSettingsUsmtMigFilesCustomXmlDel1"
                txb = TxbSettingsUsmtMigFilesCustomXmlPath1
            Case "BtnSettingsUsmtMigFilesCustomXmlDel2"
                txb = TxbSettingsUsmtMigFilesCustomXmlPath2
        End Select
        txb.Text = ""
        ShowOrHideTab(txb)
    End Sub

    Private Sub ShowOrHideTab(Optional ByVal FromTextBox As MetroTextBox = Nothing)
        Dim m_TxtBoxes = New MetroTextBox() {TxbSettingsUsmtMigFilesUserPath, TxbSettingsUsmtMigFilesAppPath, TxbSettingsUsmtMigFilesOtherDocsPath, TxbSettingsUsmtMigFilesCustomConfigPath, TxbSettingsUsmtMigFilesExcludeDrives, TxbSettingsUsmtMigFilesExcludeSystem, TxbSettingsUsmtMigFilesCustomXmlPath1, TxbSettingsUsmtMigFilesCustomXmlPath2}

        If Not FromTextBox Is Nothing Then
            Dim Txb As MetroTextBox = FromTextBox
            Dim Btn As Button = XmlFilePathDelButtonState(Txb)
            Btn.Enabled = If(Txb.Text = "", False, True)
            ShowApplyButton()
        Else
            For Each txb In m_TxtBoxes
                Dim Btn As Button = XmlFilePathDelButtonState(txb)
                Btn.Enabled = If(txb.Text = "", False, True)
            Next
        End If
    End Sub

    Private Function XmlFilePathDelButtonState(txb As MetroTextBox) As Button
        Dim Btn As Button = Nothing
        Select Case txb.Name
            Case "TxbSettingsUsmtMigFilesUserPath"
                Btn = BtnSettingsUsmtMigFilesUserDel
            Case "TxbSettingsUsmtMigFilesAppPath"
                Btn = BtnSettingsUsmtMigFilesAppDel
            Case "TxbSettingsUsmtMigFilesOtherDocsPath"
                Btn = BtnSettingsUsmtMigFilesOtherDocsDel
            Case "TxbSettingsUsmtMigFilesCustomConfigPath"
                Btn = BtnSettingsUsmtMigFilesCustomConfigDel
            Case "TxbSettingsUsmtMigFilesExcludeDrives"
                Btn = BtnSettingsUsmtMigFilesExcludeDrivesDel
            Case "TxbSettingsUsmtMigFilesExcludeSystem"
                Btn = BtnSettingsUsmtMigFilesExcludeSystemDel
            Case "TxbSettingsUsmtMigFilesCustomXmlPath1"
                Btn = BtnSettingsUsmtMigFilesCustomXmlDel1
            Case "TxbSettingsUsmtMigFilesCustomXmlPath2"
                Btn = BtnSettingsUsmtMigFilesCustomXmlDel2
        End Select
        Return Btn
    End Function

    Private Sub BtnCaptureSettingsCancel_Click(sender As Object, e As EventArgs) Handles BtnCaptureSettingsCancel.Click
        DialogResult = DialogResult.Cancel
        Close()
    End Sub

    Private Sub BtnCaptureSettingsValid_Click(sender As Object, e As EventArgs) Handles BtnCaptureSettingsValid.Click
        Dim m_XmlFiles = New String() {TxbSettingsUsmtMigFilesUserPath.Text, TxbSettingsUsmtMigFilesAppPath.Text, TxbSettingsUsmtMigFilesOtherDocsPath.Text, TxbSettingsUsmtMigFilesCustomConfigPath.Text, TxbSettingsUsmtMigFilesExcludeDrives.Text, TxbSettingsUsmtMigFilesExcludeSystem.Text, TxbSettingsUsmtMigFilesCustomXmlPath1.Text, TxbSettingsUsmtMigFilesCustomXmlPath2.Text}

        If m_XmlFiles.All(Function(f) f = "") Then
            Using Info As New FrmInfo("Paramètres invalides", "Vous devez sélectionner au moins un fichier de configuration "".xml"" !")
                Info.ShowDialog()
            End Using
        Else
            DialogResult = DialogResult.OK
            Close()
        End If
    End Sub

    Private Sub ShowApplyButton()
        If TxbSettingsUsmtRootPath.Text = m_Usmt.RootPath AndAlso
            TxbSettingsUsmtMigFilesUserPath.Text = m_Usmt.MigUserPath AndAlso
            TxbSettingsUsmtMigFilesAppPath.Text = m_Usmt.MigAppPath AndAlso
            TxbSettingsUsmtMigFilesOtherDocsPath.Text = m_Usmt.MigDocsPath AndAlso
            TxbSettingsUsmtMigFilesCustomConfigPath.Text = m_Usmt.ConfigPath AndAlso
            TxbSettingsUsmtMigFilesExcludeDrives.Text = m_Usmt.ExcludeDrivesPath AndAlso
            TxbSettingsUsmtMigFilesExcludeSystem.Text = m_Usmt.ExcludeSystemPath AndAlso
            TxbSettingsUsmtMigFilesCustomXmlPath1.Text = m_Usmt.Config1Path AndAlso
            TxbSettingsUsmtMigFilesCustomXmlPath2.Text = m_Usmt.Config2Path Then

            BtnCaptureSettingsValid.Visible = False
        Else
            BtnCaptureSettingsValid.Visible = True
        End If
    End Sub
End Class
