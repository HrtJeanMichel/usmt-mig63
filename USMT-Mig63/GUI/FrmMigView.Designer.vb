﻿Imports MetroFramework.Forms
Imports USMT_Mig63.Extensions

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMigView
    Inherits MetroForm

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMigView))
        Me.PnlMigInfosMain = New MetroFramework.Controls.MetroPanel()
        Me.LblMigInfosAccounts = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosScanDuration = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosMachineName = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosProtected = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosOS = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosCountFiles = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosSize = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosDate = New MetroFramework.Controls.MetroLabel()
        Me.LblMigInfosMainTitle = New MetroFramework.Controls.MetroLabel()
        Me.PnlMigInfosTree = New MetroFramework.Controls.MetroPanel()
        Me.PgbViewProgress = New MetroFramework.Controls.MetroProgressBar()
        Me.TvView = New USMT_Mig63.Extensions.TreeViewEx()
        Me.ImlMigview = New System.Windows.Forms.ImageList(Me.components)
        Me.LblMigInfosTreeTitle = New MetroFramework.Controls.MetroLabel()
        Me.BgwFill = New System.ComponentModel.BackgroundWorker()
        Me.PnlMigInfosMain.SuspendLayout()
        Me.PnlMigInfosTree.SuspendLayout()
        Me.SuspendLayout()
        '
        'PnlMigInfosMain
        '
        Me.PnlMigInfosMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosAccounts)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosScanDuration)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosMachineName)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosProtected)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosOS)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosCountFiles)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosSize)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosDate)
        Me.PnlMigInfosMain.Controls.Add(Me.LblMigInfosMainTitle)
        Me.PnlMigInfosMain.HorizontalScrollbarBarColor = True
        Me.PnlMigInfosMain.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlMigInfosMain.HorizontalScrollbarSize = 10
        Me.PnlMigInfosMain.Location = New System.Drawing.Point(23, 63)
        Me.PnlMigInfosMain.Name = "PnlMigInfosMain"
        Me.PnlMigInfosMain.Size = New System.Drawing.Size(685, 119)
        Me.PnlMigInfosMain.TabIndex = 4
        Me.PnlMigInfosMain.VerticalScrollbarBarColor = True
        Me.PnlMigInfosMain.VerticalScrollbarHighlightOnWheel = False
        Me.PnlMigInfosMain.VerticalScrollbarSize = 10
        '
        'LblMigInfosAccounts
        '
        Me.LblMigInfosAccounts.Location = New System.Drawing.Point(354, 71)
        Me.LblMigInfosAccounts.Name = "LblMigInfosAccounts"
        Me.LblMigInfosAccounts.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosAccounts.TabIndex = 12
        Me.LblMigInfosAccounts.Text = "Nombre de comptes :"
        Me.LblMigInfosAccounts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosScanDuration
        '
        Me.LblMigInfosScanDuration.Location = New System.Drawing.Point(5, 90)
        Me.LblMigInfosScanDuration.Name = "LblMigInfosScanDuration"
        Me.LblMigInfosScanDuration.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosScanDuration.TabIndex = 11
        Me.LblMigInfosScanDuration.Text = "Durée totale de la capture (h:m:s) :"
        Me.LblMigInfosScanDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosMachineName
        '
        Me.LblMigInfosMachineName.Location = New System.Drawing.Point(354, 52)
        Me.LblMigInfosMachineName.Name = "LblMigInfosMachineName"
        Me.LblMigInfosMachineName.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosMachineName.TabIndex = 10
        Me.LblMigInfosMachineName.Text = "Nom machine source :"
        Me.LblMigInfosMachineName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosProtected
        '
        Me.LblMigInfosProtected.Location = New System.Drawing.Point(5, 71)
        Me.LblMigInfosProtected.Name = "LblMigInfosProtected"
        Me.LblMigInfosProtected.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosProtected.TabIndex = 9
        Me.LblMigInfosProtected.Text = "Protégé par mot de passe :"
        Me.LblMigInfosProtected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosOS
        '
        Me.LblMigInfosOS.Location = New System.Drawing.Point(354, 90)
        Me.LblMigInfosOS.Name = "LblMigInfosOS"
        Me.LblMigInfosOS.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosOS.TabIndex = 8
        Me.LblMigInfosOS.Text = "OS source :"
        Me.LblMigInfosOS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosCountFiles
        '
        Me.LblMigInfosCountFiles.Location = New System.Drawing.Point(354, 33)
        Me.LblMigInfosCountFiles.Name = "LblMigInfosCountFiles"
        Me.LblMigInfosCountFiles.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosCountFiles.TabIndex = 7
        Me.LblMigInfosCountFiles.Text = "Nombre de fichiers :"
        Me.LblMigInfosCountFiles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosSize
        '
        Me.LblMigInfosSize.Location = New System.Drawing.Point(5, 52)
        Me.LblMigInfosSize.Name = "LblMigInfosSize"
        Me.LblMigInfosSize.Size = New System.Drawing.Size(326, 19)
        Me.LblMigInfosSize.TabIndex = 5
        Me.LblMigInfosSize.Text = "Taille du fichier :"
        Me.LblMigInfosSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosDate
        '
        Me.LblMigInfosDate.Location = New System.Drawing.Point(5, 33)
        Me.LblMigInfosDate.Name = "LblMigInfosDate"
        Me.LblMigInfosDate.Size = New System.Drawing.Size(330, 19)
        Me.LblMigInfosDate.TabIndex = 4
        Me.LblMigInfosDate.Text = "Date de création :"
        Me.LblMigInfosDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMigInfosMainTitle
        '
        Me.LblMigInfosMainTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblMigInfosMainTitle.BackColor = System.Drawing.Color.IndianRed
        Me.LblMigInfosMainTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblMigInfosMainTitle.ForeColor = System.Drawing.Color.White
        Me.LblMigInfosMainTitle.Location = New System.Drawing.Point(0, 0)
        Me.LblMigInfosMainTitle.Name = "LblMigInfosMainTitle"
        Me.LblMigInfosMainTitle.Size = New System.Drawing.Size(683, 29)
        Me.LblMigInfosMainTitle.TabIndex = 2
        Me.LblMigInfosMainTitle.Text = "Informations générales"
        Me.LblMigInfosMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblMigInfosMainTitle.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblMigInfosMainTitle.UseCustomBackColor = True
        Me.LblMigInfosMainTitle.UseCustomForeColor = True
        '
        'PnlMigInfosTree
        '
        Me.PnlMigInfosTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlMigInfosTree.Controls.Add(Me.PgbViewProgress)
        Me.PnlMigInfosTree.Controls.Add(Me.TvView)
        Me.PnlMigInfosTree.Controls.Add(Me.LblMigInfosTreeTitle)
        Me.PnlMigInfosTree.HorizontalScrollbarBarColor = True
        Me.PnlMigInfosTree.HorizontalScrollbarHighlightOnWheel = False
        Me.PnlMigInfosTree.HorizontalScrollbarSize = 10
        Me.PnlMigInfosTree.Location = New System.Drawing.Point(23, 188)
        Me.PnlMigInfosTree.Name = "PnlMigInfosTree"
        Me.PnlMigInfosTree.Size = New System.Drawing.Size(685, 518)
        Me.PnlMigInfosTree.TabIndex = 5
        Me.PnlMigInfosTree.VerticalScrollbarBarColor = True
        Me.PnlMigInfosTree.VerticalScrollbarHighlightOnWheel = False
        Me.PnlMigInfosTree.VerticalScrollbarSize = 10
        '
        'PgbViewProgress
        '
        Me.PgbViewProgress.HideProgressText = False
        Me.PgbViewProgress.Location = New System.Drawing.Point(-1, 498)
        Me.PgbViewProgress.Name = "PgbViewProgress"
        Me.PgbViewProgress.Size = New System.Drawing.Size(685, 19)
        Me.PgbViewProgress.Style = MetroFramework.MetroColorStyle.IndianRed
        Me.PgbViewProgress.TabIndex = 6
        Me.PgbViewProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TvView
        '
        Me.TvView.ImageIndex = 0
        Me.TvView.ImageList = Me.ImlMigview
        Me.TvView.Location = New System.Drawing.Point(-1, 27)
        Me.TvView.Name = "TvView"
        Me.TvView.SelectedImageIndex = 0
        Me.TvView.ShowLines = False
        Me.TvView.Size = New System.Drawing.Size(685, 471)
        Me.TvView.StateImageList = Me.ImlMigview
        Me.TvView.TabIndex = 0
        '
        'ImlMigview
        '
        Me.ImlMigview.ImageStream = CType(resources.GetObject("ImlMigview.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImlMigview.TransparentColor = System.Drawing.Color.Transparent
        Me.ImlMigview.Images.SetKeyName(0, "FolderEmpty.png")
        Me.ImlMigview.Images.SetKeyName(1, "FolderClose.png")
        Me.ImlMigview.Images.SetKeyName(2, "FolderOpen.png")
        Me.ImlMigview.Images.SetKeyName(3, "Drive.png")
        Me.ImlMigview.Images.SetKeyName(4, "EmptyFile.png")
        '
        'LblMigInfosTreeTitle
        '
        Me.LblMigInfosTreeTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblMigInfosTreeTitle.BackColor = System.Drawing.Color.IndianRed
        Me.LblMigInfosTreeTitle.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.LblMigInfosTreeTitle.ForeColor = System.Drawing.Color.White
        Me.LblMigInfosTreeTitle.Location = New System.Drawing.Point(0, 0)
        Me.LblMigInfosTreeTitle.Name = "LblMigInfosTreeTitle"
        Me.LblMigInfosTreeTitle.Size = New System.Drawing.Size(683, 29)
        Me.LblMigInfosTreeTitle.TabIndex = 2
        Me.LblMigInfosTreeTitle.Text = "Arborescence"
        Me.LblMigInfosTreeTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LblMigInfosTreeTitle.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LblMigInfosTreeTitle.UseCustomBackColor = True
        Me.LblMigInfosTreeTitle.UseCustomForeColor = True
        '
        'BgwFill
        '
        Me.BgwFill.WorkerReportsProgress = True
        Me.BgwFill.WorkerSupportsCancellation = True
        '
        'FrmMigView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(731, 729)
        Me.Controls.Add(Me.PnlMigInfosTree)
        Me.Controls.Add(Me.PnlMigInfosMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMigView"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.IndianRed
        Me.Text = "Informations du fichier .MIG"
        Me.PnlMigInfosMain.ResumeLayout(False)
        Me.PnlMigInfosTree.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TvView As TreeViewEx
    Friend WithEvents PnlMigInfosMain As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblMigInfosDate As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosMainTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosCountFiles As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosSize As MetroFramework.Controls.MetroLabel
    Friend WithEvents PnlMigInfosTree As MetroFramework.Controls.MetroPanel
    Friend WithEvents LblMigInfosTreeTitle As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosOS As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosProtected As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosScanDuration As MetroFramework.Controls.MetroLabel
    Friend WithEvents LblMigInfosMachineName As MetroFramework.Controls.MetroLabel
    Friend WithEvents ImlMigview As ImageList
    Friend WithEvents LblMigInfosAccounts As MetroFramework.Controls.MetroLabel
    Friend WithEvents BgwFill As System.ComponentModel.BackgroundWorker
    Friend WithEvents PgbViewProgress As MetroFramework.Controls.MetroProgressBar
End Class
