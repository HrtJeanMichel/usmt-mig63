﻿Imports System.Globalization
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports USMT_Mig63.Engine

Namespace Helper
    Public Class Mig

        Public Shared Function StoreExists(dPath As String) As Boolean
            Dim UsmtPath = Path.Combine(dPath, "USMT")
            Dim usmtDirExists = Directory.Exists(UsmtPath)
            If usmtDirExists Then
                Dim dFiles = Directory.GetFiles(UsmtPath, "*.mig", SearchOption.TopDirectoryOnly)
                If dFiles.Count > 0 Then
                    Return True
                End If
            End If
            Return False
        End Function

        Public Shared Function StoreIsAuthentic(MigfilePath As String) As Tuple(Of Boolean, String, String, MigContent)
            Dim fi As New FileInfo(MigfilePath)
            If fi.Length <= 0 Then
                Return New Tuple(Of Boolean, String, String, MigContent)(False, "Fichier invalide", "Le fichier .MIG sélectionné est vide !", Nothing)
            Else
                If fi.Directory.Name.ToUpper <> "USMT" Then
                    Return New Tuple(Of Boolean, String, String, MigContent)(False, "Répertoire invalide", "Par convention Microsoft," & vbNewLine & "le nom du répertoire parent du fichier .MIG doit être ""USMT"" !" & vbNewLine & "Le répertoire se nomme actuellement " & Chr(34) & fi.Directory.Name & Chr(34), Nothing)
                Else
                    Dim MigFileWithoutExt = Path.GetFileNameWithoutExtension(MigfilePath)
                    'On test si le fichier .MIG est conforme au nommage requis par USMT-MIG63
                    If StoreNameIsCorrect(MigfilePath) Then
                        Dim migDirpath As String = Path.GetDirectoryName(MigfilePath)
                        Dim XmlFilePath = Path.Combine(migDirpath, MigFileWithoutExt & ".xml")
                        'On vérifie si le fichier .XML existe au même emplacement que le fichier .MIG avec le même nom
                        If File.Exists(XmlFilePath) Then
                            If StoreNameIsCorrect(XmlFilePath) Then
                                'On vérifie que le fichier .XML fait référence a des fichiers de configuration Mig.xml existants au même emplacement que le fichier .MIG
                                Dim RestoreXmlInfos = MigContent.LoadFile(XmlFilePath)
                                Dim message1 As String = String.Empty
                                Dim iMissing As Integer = 0
                                For i As Integer = 0 To RestoreXmlInfos.XmlFiles.Count - 1
                                    If Not RestoreXmlInfos.XmlFiles(i) = String.Empty Then
                                        If Not File.Exists(Path.Combine(migDirpath, RestoreXmlInfos.XmlFiles(i))) Then
                                            message1 &= "- " & RestoreXmlInfos.XmlFiles(i) & vbNewLine
                                            iMissing += 1
                                        End If
                                    End If
                                Next

                                If iMissing > 0 Then
                                    Dim message0 As String = If(iMissing >= 2, "Les fichiers .xml associés au fichier .MIG sont manquants :", "Le fichier .xml associé au fichier .MIG est manquant :") & vbNewLine
                                    Return New Tuple(Of Boolean, String, String, MigContent)(False, If(iMissing >= 2, "Fichiers manquants", "Fichier manquant"), message0 & message1, Nothing)
                                Else
                                    Return New Tuple(Of Boolean, String, String, MigContent)(True, "", "", RestoreXmlInfos)
                                End If
                            Else
                                Return New Tuple(Of Boolean, String, String, MigContent)(False, "Mauvais nommage", "Le nommage du fichier " & Chr(34) & MigFileWithoutExt & ".XML"" associé au fichier .MIG n'est pas conforme !", Nothing)
                            End If
                        Else
                            Return New Tuple(Of Boolean, String, String, MigContent)(False, "Fichier manquant", "Le fichier " & Chr(34) & MigFileWithoutExt & ".XML"" associé au fichier .MIG est manquant !", Nothing)
                        End If
                    Else
                        Return New Tuple(Of Boolean, String, String, MigContent)(False, "Mauvais nommage", "Le nommage du fichier .MIG sélectionné n'est pas conforme !", Nothing)
                    End If
                End If
            End If
        End Function

        Public Shared Function StoreNameIsCorrect(filePath As String) As Boolean
            Dim FileWithoutExt = Path.GetFileNameWithoutExtension(filePath)
            If FileWithoutExt.Count = 24 Then
                If FileWithoutExt.Contains("_") Then
                    Dim splitted() = FileWithoutExt.Split("_")
                    If splitted(0) = "USMTMIG63" Then
                        If Date.TryParseExact(splitted(1), "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, Nothing) Then
                            Return True
                        End If
                    End If
                End If
            End If
            Return False
        End Function

        Public Shared Function GetPasswordMd5Hash(input As String) As String
            Using md5Hash As MD5 = MD5.Create()
                Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))
                Dim sBuilder As StringBuilder = New StringBuilder()
                For i As Integer = 0 To data.Length - 1
                    sBuilder.Append(data(i).ToString("x2"))
                Next
                Return sBuilder.ToString()
            End Using
        End Function

        Public Shared Function VerifyPasswordMd5Hash(input As String, hash As String) As Boolean
            Dim hashOfInput As String = GetPasswordMd5Hash(input)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase
            Return 0 = comparer.Compare(hashOfInput, hash)
        End Function
    End Class
End Namespace
