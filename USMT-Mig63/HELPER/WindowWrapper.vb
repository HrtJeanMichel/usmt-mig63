﻿Namespace Helper
    Public Class WindowWrapper
        Implements IWin32Window

#Region " Fields "
        Private Handle As IntPtr
#End Region

#Region " Properties "
        Private ReadOnly Property IWin32Window_Handle() As IntPtr Implements IWin32Window.Handle
            Get
                Return handle
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New(window As IWin32Window)
            Me.Handle = window.Handle
        End Sub
#End Region

    End Class
End Namespace
