﻿Imports System.ComponentModel
Imports System.DirectoryServices
Imports System.DirectoryServices.ActiveDirectory
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Net.NetworkInformation
Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports System.Text
Imports System.Xml
Imports USMT_Mig63.Core
Imports USMT_Mig63.Win32

Namespace Helper
    Public Class Utils

#Region " Fields "
        Private Shared m_WinVersions As String() = New String() {"Windows Vista", "Windows 7", "Windows 8", "Windows 8.1", "Windows 10"}
        Private Shared m_ProgramFiles32 As String = "C:\Program Files (x86)"
        Public Shared LoggedInSid As String
#End Region

#Region " Methods "
        ''' <summary>
        ''' Détermine si l'utilisateur exécutant la session appartient au groupe administrateurs local ou domaine.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function CurrentIsAdministrator() As Boolean
            Dim DomName = getDomainName()
            If DomName = String.Empty Then
                Return IsLocalAdmin()
            Else
                If Environment.UserDomainName.ToLower <> GetNetbiosNameForDomain(DomName).ToLower Then
                    Return IsLocalAdmin()
                Else
                    Return IsDomainAdmin()
                End If
            End If
        End Function

        Private Shared Function GetNetbiosNameForDomain(dns As String) As String
            Dim pDomainInfo As IntPtr
            Dim result As Integer = NativeMethods.DsGetDcName(Nothing, dns, IntPtr.Zero, Nothing, NativeEnum.DSGETDCNAME_FLAGS.DS_IS_DNS_NAME Or NativeEnum.DSGETDCNAME_FLAGS.DS_RETURN_FLAT_NAME, pDomainInfo)
            Try
                If result <> NativeConsts.ERROR_SUCCESS Then Throw New Win32Exception(result)
                Dim dcinfo = New DomainControllerInfo()
                Marshal.PtrToStructure(pDomainInfo, dcinfo)
                Return dcinfo.DomainName
            Finally
                If pDomainInfo <> IntPtr.Zero Then NativeMethods.NetApiBufferFree(pDomainInfo)
            End Try
        End Function

        Private Shared Function IsLocalAdmin() As Boolean
            Dim id As WindowsIdentity = WindowsIdentity.GetCurrent()
            Dim wp As WindowsPrincipal = New WindowsPrincipal(id)
            Dim account As NTAccount = New NTAccount(id.Name)
            Dim sid = account.Translate(GetType(SecurityIdentifier))
            LoggedInSid = sid.ToString
            Return wp.IsInRole(WindowsBuiltInRole.Administrator)
        End Function

        Private Shared Function IsDomainAdmin() As Boolean
            Dim d As Domain = Domain.GetDomain(New DirectoryContext(DirectoryContextType.Domain, getDomainName()))
            Using de As DirectoryEntry = d.GetDirectoryEntry()
                Dim domainSIdArray As Byte() = CType(de.Properties("objectSid").Value, Byte())
                Dim domainSId As SecurityIdentifier = New SecurityIdentifier(domainSIdArray, 0)
                Dim domainAdminsSId As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.AccountDomainAdminsSid, domainSId)
                Dim wp As WindowsPrincipal = New WindowsPrincipal(WindowsIdentity.GetCurrent())
                LoggedInSid = domainAdminsSId.ToString
                Return wp.IsInRole(domainAdminsSId)
            End Using
        End Function

        Private Shared Function GetDomainName() As String
            Return IPGlobalProperties.GetIPGlobalProperties().DomainName
        End Function

        ''' <summary>
        ''' Vérifie si le système d'exploitation est conforme aux exigences.
        ''' </summary>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function IsCorrectOS() As Boolean
            Return m_WinVersions.Contains(GetOsVersion)
        End Function

        ''' <summary>
        ''' Retourne la valeur chaine du système d'exploitation et sa version.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetOsProductName() As String
            Dim ProdName = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", Nothing).ToString()
            Return ProdName.Trim
        End Function

        ''' <summary>
        ''' Retourne le numéro de Build du système d'exploitation.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetReleaseId() As String
            Dim Realeaseid = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ReleaseId", "").ToString()
            Return Realeaseid.Trim
        End Function

        ''' <summary>
        ''' Retourne la valeur chaine des systèmes d'exploitation compatible pour cette application.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetOsInline() As String
            Return String.Join(vbNewLine & "- ", m_WinVersions)
        End Function

        ''' <summary>
        ''' Renvoie la version du système d'exploitation (Windows Vista, Windows 7, Windows 8, Windows 8.1, Windows 10, ...
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetOsVersion() As String
            Dim oSVersion As OperatingSystem = Environment.OSVersion
            Select Case oSVersion.Platform.ToString
                Case "Win32Windows"
                    Select Case oSVersion.Version.Minor
                        Case 0
                            Return "Windows 95"
                        Case 10
                            If (oSVersion.Version.Revision.ToString = "2222A") Then
                                Return "Windows 98 Second Edition"
                            End If
                            Return "Windows 98"
                        Case 90
                            Return "Windows Me"
                    End Select
                    Exit Select
                Case "Win32NT"
                    Select Case oSVersion.Version.Major
                        Case 3
                            Return "Windows NT 3.51"
                        Case 4
                            Return "Windows NT 4.0"
                        Case 5
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    Return "Windows 2000"
                                Case 1
                                    Return "Windows XP"
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2003 R2"
                                    Else
                                        Return "Windows Server 2003"
                                    End If
                            End Select
                            Exit Select
                        Case 6
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2008"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Vista") Then
                                        Return "Windows Vista"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 1
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2008 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("7") Then
                                        Return "Windows 7"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8") Then
                                        Return "Windows 8"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 3
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8.1") Then
                                        Return "Windows 8.1"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 4
                                    If My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                            End Select
                        Case 10
                            If My.Computer.Info.OSFullName.Contains("10") Then
                                Return "Windows 10"
                            ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                Return "Windows Embedded"
                            End If
                            Exit Select
                    End Select
                    Exit Select
            End Select
            Return "Système d'exploitation inconnu !"
        End Function

        ''' <summary>
        ''' Ferme le processus selon son nom passé en paramètre.
        ''' </summary>
        ''' <param name="ProcName">Nom du processus sans extension</param>
        Public Shared Sub KillProcess(ProcName As String)
            For Each ObjPro As Process In Process.GetProcessesByName(ProcName)
                ObjPro.Kill()
            Next
        End Sub

        '''' <summary>
        '''' Détermine l'architecture du système d'exploitation en se basant sur l'existence du chemin "C:\Program Files (x86)".
        '''' </summary>
        Public Shared Function Is64Bits() As Boolean
            Return Directory.Exists(m_ProgramFiles32)
        End Function

        '''' <summary>
        '''' Annule l'opération d'extinction ou de redémarrage de l'odinateur.
        '''' </summary>
        Public Shared Sub ShutdownCancel()
            NativeMethods.CancelShutdown()
        End Sub

        ''' <summary>
        ''' Retourne le nom de l'utilisateur exécutant la session en cours.
        ''' </summary>
        ''' <param name="CheckDomain">permet de préfixer le nom d'utilisateur par le nom de domaine ou le nom de l'ordinateur</param>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetUsernameBySessionId(Optional ByVal checkDomain As Boolean = False) As String
            Dim buff As IntPtr
            Dim strLength As Integer
            Dim userName As String = ""
            Dim sessionId = NativeMethods.WTSGetActiveConsoleSessionId()
            If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSUserName, buff, strLength) AndAlso strLength > 1 Then
                userName = Marshal.PtrToStringAnsi(buff)
                NativeMethods.WTSFreeMemory(buff)
                If checkDomain Then
                    If NativeMethods.WTSQuerySessionInformation(IntPtr.Zero, sessionId, NativeEnum.WTS_INFO_CLASS.WTSDomainName, buff, strLength) AndAlso strLength > 1 Then
                        userName = Marshal.PtrToStringAnsi(buff) & "\" & userName
                        NativeMethods.WTSFreeMemory(buff)
                    End If
                End If
            End If
            Return userName
        End Function

        ''' <summary>
        ''' Renvoie le nom de l'utilisateur qui a exécuté cette application.
        ''' Le nom renvoyé n'est pas celui qui procède à une exécution de cette application depuis une élévation de privilèges.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function CurrentLoggedinUserNameElevated() As String
            For Each theprocess As Process In Process.GetProcesses()
                If theprocess.ProcessName.ToUpper = "USMT-MIG63" Then
                    Dim samAccountname As String = String.Empty
                    Dim SID As String = String.Empty
                    Dim process As String = ExGetProcessInfoByPID(theprocess.Id, SID)
                    If SID <> "" Then
                        samAccountname = New SecurityIdentifier(SID).Translate(GetType(NTAccount)).ToString
                        If samAccountname.Contains("\") Then
                            Dim vals = samAccountname.Split("\")
                            If vals.Count = 2 Then
                                samAccountname = vals(1)
                                Return samAccountname
                            End If
                        End If
                    End If
                End If
            Next
            Return ""
        End Function

        Private Shared Function GrabUserInfo(pToken As IntPtr, <Out> ByRef SID As IntPtr) As Boolean
            Dim procToken As IntPtr = IntPtr.Zero
            Dim ret As Boolean = False
            SID = IntPtr.Zero
            Try
                If NativeMethods.OpenProcessToken(pToken, NativeConsts.TOKEN_QUERY, procToken) Then
                    ret = ProcessTokenToSid(procToken, SID)
                    NativeMethods.CloseHandle(procToken)
                End If
                Return ret
            Catch
                Return False
            End Try
        End Function

        Private Shared Function ProcessTokenToSid(token As IntPtr, <Out> ByRef SID As IntPtr) As Boolean
            Dim tokUser As NativeStruct.TOKEN_USER
            Const bufLength As Integer = 256
            Dim tokInfo As IntPtr = Marshal.AllocHGlobal(bufLength)
            Dim ret As Boolean
            SID = IntPtr.Zero

            Try
                Dim cb As Integer = bufLength
                ret = NativeMethods.GetTokenInformation(token, NativeEnum.TOKEN_INFORMATION_CLASS.TokenUser, tokInfo, cb, cb)

                If ret Then
                    tokUser = CType(Marshal.PtrToStructure(tokInfo, GetType(NativeStruct.TOKEN_USER)), NativeStruct.TOKEN_USER)
                    SID = tokUser.User.Sid
                End If

                Return ret
            Catch
                Return False
            Finally
                Marshal.FreeHGlobal(tokInfo)
            End Try
        End Function

        Private Shared Function ExGetProcessInfoByPID(PID As Integer, <Out> ByRef SID As String) As String
            Dim _SID As IntPtr = IntPtr.Zero
            SID = String.Empty

            Try
                Dim process As Process = Process.GetProcessById(PID)

                If GrabUserInfo(process.Handle, _SID) Then
                    NativeMethods.ConvertSidToStringSid(_SID, SID)
                End If

                Return process.ProcessName
            Catch
                Return "Unknown"
            End Try
        End Function

        ''' <summary>
        ''' Détermine si 2 liste génériques sont identiques de part leur contenu.
        ''' </summary>
        ''' <param name="aListA">Liste générique A</param>
        ''' <param name="aListB">Liste générique B</param>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function CompareLists(Of T)(aListA As List(Of T), aListB As List(Of T)) As Boolean
            If aListA Is Nothing OrElse aListB Is Nothing OrElse aListA.Count <> aListB.Count Then Return False
            If aListA.Count = 0 Then Return True
            Dim lookUp As Dictionary(Of T, Integer) = New Dictionary(Of T, Integer)()

            For i As Integer = 0 To aListA.Count - 1
                Dim count As Integer = 0

                If Not lookUp.TryGetValue(aListA(i), count) Then
                    lookUp.Add(aListA(i), 1)
                    Continue For
                End If

                lookUp(aListA(i)) = count + 1
            Next

            For i As Integer = 0 To aListB.Count - 1
                Dim count As Integer = 0

                If Not lookUp.TryGetValue(aListB(i), count) Then
                    Return False
                End If

                count -= 1

                If count <= 0 Then
                    lookUp.Remove(aListB(i))
                Else
                    lookUp(aListB(i)) = count
                End If
            Next

            Return lookUp.Count = 0
        End Function

        ''' <summary>
        ''' Permet de savoir si les outils de migration USMT (ScanState, LoadState) sont présents ainsi que 4 fichiers de migration .xml et retourne les messages d'erreur en conséquence.
        ''' </summary>
        ''' <returns>Valeur de type Tuple(Of Boolean, String, String)</returns>
        Public Shared Function Usmt5PathInfos() As Tuple(Of Boolean, String, String)
            Dim arch = If(is64Bits(), "x64", "x86")

            Dim LoadStateExists As Boolean
            Dim LoadStateExistsStr As String = ""
            Dim LoadStateArch As String = ""
            Dim ScanStateExists As Boolean
            Dim ScanStateExistsStr As String = ""
            Dim ScanStateArch As String = ""

            Dim MigUserXmlExists As Boolean
            Dim MigUserXmlPath As String = ""
            Dim MigAppXmlExists As Boolean
            Dim MigAppXmlPath As String = ""

            Dim ExcludeDrivesExists As Boolean
            Dim ExcludeDrivesPath As String = ""
            Dim ExcludeSystemExists As Boolean
            Dim ExcludeSystemPath As String = ""

            Dim Umst5Path = Path.Combine(My.Application.Info.DirectoryPath, "USMT5")

            Dim di As New DirectoryInfo(Umst5Path)
            If di.Exists Then
                Dim usmt As New Usmt
                If Directory.Exists(usmt.RootPath) Then
                    Dim Message As String = "Les problèmes suivants sont apparu :" & vbNewLine
                    If File.Exists(Path.Combine(usmt.RootPath, "loadstate.exe")) Then
                        If GetExeArch(Path.Combine(usmt.RootPath, "loadstate.exe")) = arch Then
                            LoadStateExists = True
                        Else
                            LoadStateArch = "- Mauvaise architecture cible pour loadstate.exe" & vbNewLine
                        End If
                    Else
                        LoadStateExistsStr = "- le fichier loadstate.exe est manquant" & vbNewLine
                    End If

                    If File.Exists(Path.Combine(usmt.RootPath, "scanstate.exe")) Then
                        If GetExeArch(Path.Combine(usmt.RootPath, "scanstate.exe")) = arch Then
                            ScanStateExists = True
                        Else
                            ScanStateArch = "- Mauvaise architecture cible pour scanstate.exe" & vbNewLine
                        End If
                    Else
                        ScanStateExistsStr = "- le fichier scanstate.exe est manquant" & vbNewLine
                    End If

                    If File.Exists(usmt.MigUserPath) Then
                        MigUserXmlExists = True
                    Else
                        MigUserXmlPath = "- le fichier miguser.xml est manquant" & vbNewLine
                    End If

                    If File.Exists(usmt.MigAppPath) Then
                        MigAppXmlExists = True
                    Else
                        MigAppXmlPath = "- le fichier migapp.xml est manquant" & vbNewLine
                    End If

                    If File.Exists(usmt.ExcludeDrivesPath) Then
                        ExcludeDrivesExists = True
                    Else
                        ExcludeDrivesPath = "- le fichier ExcludeDrives.xml est manquant" & vbNewLine
                    End If

                    If File.Exists(usmt.ExcludeSystemPath) Then
                        ExcludeSystemExists = True
                    Else
                        ExcludeSystemPath = "- le fichier ExcludeSystem.xml est manquant" & vbNewLine
                    End If

                    If LoadStateExists = False OrElse ScanStateExists = False OrElse MigUserXmlExists = False OrElse MigAppXmlExists = False OrElse ExcludeDrivesExists = False OrElse ExcludeSystemExists = False Then
                        Message &= LoadStateArch & ScanStateExistsStr & LoadStateArch & LoadStateExistsStr & MigUserXmlPath & MigAppXmlPath & ExcludeDrivesPath & ExcludeSystemPath
                        Return New Tuple(Of Boolean, String, String)(False, "Mauvais environnement", Message)
                    End If
                    Return New Tuple(Of Boolean, String, String)(True, "", "")
                Else
                    Return New Tuple(Of Boolean, String, String)(False, "Mauvais environnement", "Le répertoire contenant les outils de migration " & If(is64Bits(), "x64", "x32") & "Bits " & "est manquant : " & vbNewLine & usmt.RootPath)
                End If
            Else
                Return New Tuple(Of Boolean, String, String)(False, "Mauvais environnement", "Le répertoire contenant les outils de migration est manquant : " & vbNewLine & Umst5Path)
            End If
        End Function

        ''' <summary>
        ''' Retourne une valeur chaine de la version des outils de migration USMT.
        ''' </summary>
        ''' <param name="UsmtFilePath">Chemin complet du fichier ScanState ou LoadState</param>
        ''' <param name="ShortName">Passé à True ce paramètre permet d'obtenir l'information courte pour l'onglet information de l'application</param>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function AdkVersionInfoFromFile(UsmtFilePath As String, Optional ByVal ShortName As Boolean = False) As String
            Dim usmtFrom As String = "USMT provient de l'ADK pour Windows "
            If ShortName Then
                usmtFrom = "USMT pour Windows "
            End If
            Select Case FileVersionInfo.GetVersionInfo(UsmtFilePath).ProductVersion.ToString().Trim
                Case "10.1.19041.1", "10.0.19041.1"
                    Return usmtFrom & "10 version 2004"
                Case "10.1.18362.1", "10.0.18362.1"
                    Return usmtFrom & "10 version 1903"
                Case "10.1.17763.1", "10.0.17763.1"
                    Return usmtFrom & "10 version 1809"
                Case "10.1.17134", "10.1.17", "10.0.17134", "10.0.17"
                    Return usmtFrom & "10 version 1803"
                Case "10.1.16299", "10.1.16299.15", "10.0.16299", "10.0.16299.15"
                    Return usmtFrom & "10 version 1709"
                Case "10.1.15063", "10.0.15063"
                    Return usmtFrom & "10 version 1703"
                Case "10.1.14393.0", "10.0.14393.0"
                    Return usmtFrom & "10 version 1607"
                Case "10.1.10586.0", "10.0.10586.0"
                    Return usmtFrom & "10 version 1511"
                Case "10.1.26624.0", "10.0.26624.0"
                    Return usmtFrom & "10 version RTM"
                Case "10.1.10240.0", "10.0.10240.0"
                    Return usmtFrom & "10"
                Case "8.100.26866"
                    Return usmtFrom & "8.1"
                Case "8.100.26629", "8.100.26020"
                    Return usmtFrom & "8.1 (Mise à jour)"
                Case "8.100.25984"
                    Return usmtFrom & "8.1 RTM"
                Case "8.59.25584"
                    Return usmtFrom & "8"
                Case Else
                    Return "??????????"
            End Select
        End Function

        ''' <summary>
        ''' Détermine si le fichier .xml possède un bon formalisme.
        ''' </summary>
        ''' <param name="XmlFilePath">Chemin complet d'un fichier de configuration .xml</param>
        ''' <returns>Valeur de type Boolean</returns>
        Public Shared Function XmlFileValidation(XmlFilePath As String) As Boolean
            Dim xml As XmlDocument = New XmlDocument()
            Try
                xml.Load(XmlFilePath)
            Catch ex As Exception
                Return False
            End Try
            Return True
        End Function

        Private Shared Function GetExeMachineType(exePath As String) As NativeEnum.MachineType
            Using fs As FileStream = New FileStream(exePath, FileMode.Open, FileAccess.Read)
                Using br As BinaryReader = New BinaryReader(fs)
                    fs.Seek(&H3C, SeekOrigin.Begin)
                    Dim peOffset As Integer = br.ReadInt32()
                    fs.Seek(peOffset, SeekOrigin.Begin)
                    Dim peHead As UInteger = br.ReadUInt32()
                    If peHead <> &H4550 Then Throw New Exception("Entête PE introuvable !")
                    Dim machineType As NativeEnum.MachineType = br.ReadUInt16()
                    Return machineType
                End Using
            End Using
        End Function

        Private Shared Function GetExeArch(exePath As String) As String
            Dim arch As String = String.Empty
            Try
                Select Case GetExeMachineType(exePath)
                    Case NativeEnum.MachineType.IMAGE_FILE_MACHINE_AMD64
                        arch = "x64"
                    Case NativeEnum.MachineType.IMAGE_FILE_MACHINE_I386
                        arch = "x86"
                End Select
            Catch ex As Exception
            End Try
            Return arch
        End Function

        Public Shared Function IsDirectoryPath(dir As String) As Boolean
            If dir.EndsWith("\\") Then
                Return False
            Else
                Return Not dir.EndsWith(":\")
            End If
        End Function

        Public Shared Function HasWritePermission(dir As String) As Boolean
            If dir.StartsWith("\\") Then
                Return True
            Else
                Dim attr = File.GetAttributes(dir)
                If (attr And FileAttributes.Hidden) OrElse (attr And FileAttributes.ReparsePoint) OrElse (attr And FileAttributes.System) Then
                    Return False
                End If
                Return True
            End If
        End Function

        Public Shared Function StrFormatByteSize(filesize As Long) As String
            Dim sb As StringBuilder = New StringBuilder(20)
            NativeMethods.StrFormatByteSize(filesize, sb, sb.Capacity)
            Return sb.ToString()
        End Function

        Public Shared Sub GrayedImage(Btn As Button, Enabled As Boolean, image As Image)
            Btn.BackgroundImage = Nothing
            Btn.BackgroundImage = image
            Btn.Enabled = Enabled
            If image IsNot Nothing AndAlso Enabled = False Then
                Dim size As Size = image.Size
                Dim newMatrix As Single()() = New Single(4)() {}
                newMatrix(0) = New Single() {0.2125F, 0.2125F, 0.2125F, 0.0F, 0.0F}
                newMatrix(1) = New Single() {0.2577F, 0.2577F, 0.2577F, 0.0F, 0.0F}
                newMatrix(2) = New Single() {0.0361F, 0.0361F, 0.0361F, 0.0F, 0.0F}
                Dim arr As Single() = New Single(4) {}
                arr(3) = 1.0F
                newMatrix(3) = arr
                newMatrix(4) = New Single() {0.38F, 0.38F, 0.38F, 0.0F, 1.0F}
                Dim matrix As New ColorMatrix(newMatrix)
                Dim disabledAttr As New ImageAttributes()
                disabledAttr.ClearColorKey()
                disabledAttr.SetColorMatrix(matrix)
                Btn.BackgroundImage = New Bitmap(image.Width, image.Height)
                Using gr As Graphics = Graphics.FromImage(Btn.BackgroundImage)
                    gr.DrawImage(image, New Rectangle(0, 0, size.Width, size.Height), 0, 0, size.Width, size.Height, GraphicsUnit.Pixel, disabledAttr)
                End Using
            End If
        End Sub

        Public Shared Function IconFromExtensionShell(extension As String, size As NativeEnum.IconSize) As Icon
            Dim fileInfo As NativeStruct.SHFILEINFO = New NativeStruct.SHFILEINFO()
            NativeMethods.SHGetFileInfo(extension, 0, fileInfo, Marshal.SizeOf(fileInfo), NativeEnum.SHGFI.SHGFI_ICON Or NativeEnum.SHGFI.SHGFI_USEFILEATTRIBUTES Or CType(size, NativeEnum.SHGFI))
            Return Icon.FromHandle(fileInfo.hIcon)
        End Function
#End Region

    End Class
End Namespace
