﻿Imports System.IO
Imports System.Threading

Namespace Helper
    Public Class Watcher
        Inherits FileSystemWatcher

        Public Delegate Sub OutputFsLineEventHandler(sender As Object, e As OutputFsLineEventArgs)
        Public Event OutputFsLine As OutputFsLineEventHandler

        Private m_fLogName As String
        Private m_Restore As Boolean
        Private m_TaskName As String

        Private m_reloadingFile As Boolean
        Private m_lastFileSize As Long

        Public Class OutputFsLineEventArgs
            Inherits EventArgs

            Public ReadOnly Text As String
            Public ReadOnly Clear As Boolean

            Public Sub New(txt As String, clr As Boolean)
                Text = txt
                Clear = clr
            End Sub
        End Class

        Public Sub New(UsmtRootPath As String, fLogName As String, Optional ByVal restore As Boolean = False)
            MyBase.New(UsmtRootPath, "*.log")
            m_Restore = restore
            m_TaskName = If(restore, "LoadState", "ScanState")
            NotifyFilter = NotifyFilters.Size Or NotifyFilters.FileName
            m_fLogName = fLogName
        End Sub

        Public Sub StartWatch()
            AddHandler Changed, AddressOf FsWatcher_OnChanged
            EnableRaisingEvents = True
        End Sub

        Private Sub FsWatcher_OnChanged(source As Object, e As FileSystemEventArgs)
            If File.Exists(e.FullPath) Then
                If IO.Path.GetFileName(e.FullPath) = m_TaskName & "Progress.log" Then
                    ProcessLogFile(e.FullPath)
                End If
            End If
        End Sub

        Private Sub ProcessLogFile(fName As String)
            If m_reloadingFile Then Return
            m_reloadingFile = True
            Try
                Dim newFileLines As String = ""
                Dim newLength As Long = 0
                Dim fileExists As Boolean = File.Exists(fName)
                Dim needToClear As Boolean = Not fileExists

                If fileExists Then
                    Dim count As Integer = 0
                    Dim success As Boolean = False

                    While count < 5 AndAlso Not success
                        Try
                            Using stream As FileStream = File.Open(fName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                                newLength = stream.Length

                                If newLength >= m_lastFileSize Then
                                    stream.Position = m_lastFileSize
                                Else
                                    needToClear = True
                                End If

                                Using reader As StreamReader = New StreamReader(stream)
                                    newFileLines = reader.ReadToEnd()
                                End Using
                            End Using

                            success = True
                        Catch ex As IOException
                            Thread.Sleep(50)
                        End Try

                        count += 1
                    End While
                End If

                m_lastFileSize = newLength

                If newFileLines.Length <> 0 Then
                    Dim lastCr As Integer = newFileLines.LastIndexOf(vbLf)
                    If lastCr <> -1 AndAlso lastCr > 0 Then
                        If newFileLines(lastCr - 1) <> vbCr Then
                            newFileLines = newFileLines.Replace(vbLf, vbCrLf)
                        End If
                    End If
                End If

                If needToClear Then
                    RaiseOutputLineEvent("", True)
                End If

                If newFileLines.Length <> 0 Then
                    RaiseOutputLineEvent(newFileLines, False)
                End If
            Catch ex As Exception
                'MsgBox("ProcessLogFile Exception : " & ex.ToString)
            Finally
                m_reloadingFile = False
            End Try
        End Sub

        Protected Sub RaiseOutputLineEvent(text As String, Clr As Boolean)
            RaiseEvent OutputFsLine(Me, New OutputFsLineEventArgs(text, Clr))
        End Sub

        Public Sub StopWatch()
            RemoveHandler Changed, AddressOf FsWatcher_OnChanged
            Dispose()
        End Sub

    End Class
End Namespace
