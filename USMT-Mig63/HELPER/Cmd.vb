﻿Imports System.Threading

Namespace Helper
    Public Class Cmd

#Region "Event Handler Delegates and Event Argument Classes"

        Public Delegate Sub StartedEventHandler(sender As Object, e As StartedEventArgs)
        Public Delegate Sub ExitedEventHandler(sender As Object, e As ExitedEventArgs)
        Public Delegate Sub AbortedEventHandler(sender As Object, e As AbortedEventArgs)
        Public Delegate Sub ClosedEventHandler(sender As Object, e As ClosedEventArgs)
        Public Delegate Sub OutputLineEventHandler(sender As Object, e As OutputLineEventArgs)

        Public Class OutputLineEventArgs
            Inherits EventArgs

            Public ReadOnly OutputType As OutputType
            Public ReadOnly Text As String

            Public Sub New(type As OutputType, text__1 As String)
                OutputType = type
                Text = text__1
            End Sub

            Public Sub New(type As OutputType)
                OutputType = type
                Text = Nothing
            End Sub
        End Class

        Public Class StartedEventArgs
            Inherits EventArgs

            Public ReadOnly StartTime As Date

            Public Sub New()
                StartTime = UndefinedTimeValue
            End Sub

            Public Sub New(time As Date)
                StartTime = time
            End Sub
        End Class

        Public Class AbortedEventArgs
            Inherits EventArgs

            Public ReadOnly AbortTime As Date

            Public Sub New()
                AbortTime = UndefinedTimeValue
            End Sub

            Public Sub New(time As Date)
                AbortTime = time
            End Sub
        End Class

        Public Class ClosedEventArgs
            Inherits EventArgs

            Public ReadOnly CloseTime As Date

            Public Sub New()
                CloseTime = UndefinedTimeValue
            End Sub

            Public Sub New(time As Date)
                CloseTime = time
            End Sub
        End Class

        Public Class ExitedEventArgs
            Inherits EventArgs

            Public ReadOnly ExitCode As Integer
            Public ReadOnly ExitTime As Date

            Public Sub New()
                ExitCode = UndefinedCodeValue
                ExitTime = UndefinedTimeValue
            End Sub

            Public Sub New(code As Integer, time As Date)
                ExitCode = code
                ExitTime = time
            End Sub

            Public Sub New(code As Integer)
                ExitCode = code
                ExitTime = UndefinedTimeValue
            End Sub

            Public Sub New(time As Date)
                ExitCode = UndefinedCodeValue
                ExitTime = time
            End Sub
        End Class


#End Region

#Region "Construction/Destruction"

        Public Sub New(command As String, arguments As String)
            m_strCommand = command
            m_strArguments = arguments

            m_proc.StartInfo.FileName = command
            m_proc.StartInfo.Arguments = arguments
            m_proc.StartInfo.CreateNoWindow = True
            m_proc.StartInfo.RedirectStandardError = True
            m_proc.StartInfo.RedirectStandardInput = True
            m_proc.StartInfo.RedirectStandardOutput = True
            m_proc.StartInfo.UseShellExecute = False
            m_proc.StartInfo.ErrorDialog = False

            m_proc.EnableRaisingEvents = True

            AddHandler m_proc.Exited, New EventHandler(AddressOf OnProcessExited)
        End Sub

#End Region

#Region "Fields"

        Protected m_strCommand As String = Nothing
        Protected m_strArguments As String = Nothing
        Protected m_proc As New Process()
        Protected m_threadRec As Thread = Nothing

#End Region

#Region "Constants and Enumerations"

        Public Shared ReadOnly UndefinedCodeValue As Integer = Integer.MinValue
        Public Shared ReadOnly UndefinedTimeValue As Date = Date.MinValue
        Public Shared ReadOnly UndefinedHandleValue As IntPtr = IntPtr.Zero

        Public Enum OutputType
            None = 0
            StdOut = 1
            StdErr = 2
        End Enum

#End Region

#Region "Properties"

        Public Property Arguments() As String
            Get
                Return m_strArguments
            End Get

            Set
                If IsRunning Then
                    Return
                End If
                If Command Is Nothing Then
                    Return
                End If
                If Value = m_strArguments Then
                    Return
                End If

                If (m_strArguments Is Nothing) AndAlso (Value IsNot Nothing) Then
                    If Value.Trim() = "" Then
                        Return
                    End If
                End If

                If (Value IsNot Nothing) AndAlso (m_strArguments IsNot Nothing) Then
                    If Value.Trim().ToUpper() = m_strArguments.Trim().ToUpper() Then
                        Return
                    End If
                End If

                If Value IsNot Nothing Then
                    If Value.Trim().Length > 0 Then
                        m_strArguments = Value.Trim()
                    Else
                        m_strArguments = Nothing
                    End If
                Else
                    m_strArguments = Nothing
                End If
            End Set
        End Property

        Public Property Tag As Object

        Public Property Command() As String
            Get
                Return m_strCommand
            End Get

            Set
                If IsRunning Then
                    Return
                End If
                If Value = m_strCommand Then
                    Return
                End If
                If (m_strCommand Is Nothing) AndAlso (Value IsNot Nothing) Then
                    If Value.Trim() = "" Then
                        Return
                    End If
                End If
                If (Value IsNot Nothing) AndAlso (m_strCommand IsNot Nothing) Then
                    If Value.Trim().ToUpper() = m_strCommand.Trim().ToUpper() Then
                        Return
                    End If
                End If

                If Value IsNot Nothing Then
                    If Value.Trim().Length > 0 Then
                        m_strCommand = Value.Trim()
                    Else
                        m_strCommand = Nothing
                    End If
                Else
                    m_strCommand = Nothing
                End If

                m_strArguments = Nothing
            End Set
        End Property

        Public ReadOnly Property ExitCode() As Integer
            Get
                Try
                    Return m_proc.ExitCode
                Catch
                    Return UndefinedCodeValue
                End Try
            End Get
        End Property

        Public ReadOnly Property ExitTime() As Date
            Get
                Try
                    Return m_proc.ExitTime
                Catch
                    Return UndefinedTimeValue
                End Try
            End Get
        End Property

        Public ReadOnly Property Handle() As IntPtr
            Get
                Try
                    Return m_proc.Handle
                Catch
                    Return UndefinedHandleValue
                End Try
            End Get
        End Property

        Public ReadOnly Property HasExited() As Boolean
            Get
                Try
                    Return m_proc.HasExited
                Catch
                    Return False
                End Try
            End Get
        End Property

        Public ReadOnly Property Id() As Integer
            Get
                Try
                    Return m_proc.Id
                Catch
                    Return 0
                End Try
            End Get
        End Property

        Public ReadOnly Property IsRunning() As Boolean
            Get
                Return (Not HasExited AndAlso (Id <> 0))
            End Get
        End Property

        Public ReadOnly Property ProcessName() As String
            Get
                Try
                    Return m_proc.ProcessName
                Catch
                    Return Nothing
                End Try
            End Get
        End Property

        Public ReadOnly Property StartTime() As Date
            Get
                Try
                    Return m_proc.StartTime
                Catch
                    Return UndefinedTimeValue
                End Try
            End Get
        End Property

#End Region

#Region "Events"

        Public Event Started As StartedEventHandler
        Public Event Exited As ExitedEventHandler
        Public Event Aborted As AbortedEventHandler
        Public Event Closed As ClosedEventHandler
        Public Event OutputLine As OutputLineEventHandler

#End Region

#Region "Operations (public methods)"

        Public Function Start() As Boolean
            If IsRunning Then
                Return False
            End If
            If (Command Is Nothing) OrElse (Command.Trim() = "") Then
                Me.Command = Nothing
                Return False
            End If

            m_strCommand = Command
            m_strArguments = Arguments

            Return DoStart()
        End Function

        Public Function Start(command As String, arguments As String) As Boolean
            If IsRunning Then
                Return False
            End If
            If (command Is Nothing) OrElse (command.Trim() = "") Then
                Me.Command = Nothing
                Return False
            End If

            m_strCommand = command
            m_strArguments = arguments

            Return DoStart()
        End Function

        Public Function Abort() As Boolean
            Try
                m_proc.Kill()
            Catch
                Return False
            End Try
            RaiseAbortedEvent(Date.Now)
            Return True
        End Function

        Public Function Close() As Boolean
            If IsRunning Then
                Return False
            End If
            If Handle = UndefinedHandleValue Then
                Return False
            End If
            Try
                m_proc.Close()
            Catch
                Return False
            End Try

            RaiseClosedEvent(Date.Now)
            Return True
        End Function

#End Region

#Region "Implementation (private, protected, and internal methods)"

        Protected Overridable Function DoStart() As Boolean
            Try
                m_proc.StartInfo.FileName = m_strCommand
                m_proc.StartInfo.Arguments = m_strArguments

                If Not m_proc.Start() Then
                    Return False
                End If

                RaiseStartedEvent(StartTime)
                StartOutputRecording()

            Catch ex As Exception
                Return False
            End Try
            Return True
        End Function

        Protected Overridable Function StartOutputRecording() As Boolean
            Try
                m_threadRec = New Thread(New ThreadStart(AddressOf ReadProcessOutput))
                m_threadRec.Name = "CommandLineProcessListener"
                m_threadRec.Start()
                Return True
            Catch
                m_threadRec = Nothing
                Return False
            End Try
        End Function

        Protected Overridable Sub StopOutputRecording()
            If m_threadRec Is Nothing Then
                Return
            End If
            Try
                m_threadRec.Abort()
            Catch
            Finally
                m_threadRec = Nothing
            End Try
        End Sub

        Protected Overridable Sub ReadProcessOutput()
            If m_threadRec Is Nothing Then
                Return
            End If

            If OutputLineEvent Is Nothing Then
                m_proc.StandardOutput.DiscardBufferedData()
                m_proc.StandardError.DiscardBufferedData()
                Return
            End If

            Try
                Dim ch As Integer
                While -1 <> (InlineAssignHelper(ch, m_proc.StandardOutput.Read))
                    RaiseOutputLineEvent(OutputType.StdOut, ChrW(ch))
                End While
            Catch ex As Exception
            End Try

            Try
                Dim ch As Integer
                While -1 <> (InlineAssignHelper(ch, m_proc.StandardError.Read))
                    RaiseOutputLineEvent(OutputType.StdErr, ChrW(ch))
                End While
            Catch ex As Exception
            End Try
        End Sub

        Protected Sub RaiseAbortedEvent()
            RaiseEvent Aborted(Me, New AbortedEventArgs())
        End Sub

        Protected Sub RaiseAbortedEvent(time As Date)
            RaiseEvent Aborted(Me, New AbortedEventArgs(time))
        End Sub

        Protected Sub RaiseOutputLineEvent(type As OutputType)
            RaiseEvent OutputLine(Me, New OutputLineEventArgs(type))
        End Sub

        Protected Sub RaiseOutputLineEvent(type As OutputType, text As String)
            RaiseEvent OutputLine(Me, New OutputLineEventArgs(type, text))
        End Sub

        Protected Sub RaiseClosedEvent()
            RaiseEvent Closed(Me, New ClosedEventArgs())
        End Sub

        Protected Sub RaiseClosedEvent(time As Date)
            RaiseEvent Closed(Me, New ClosedEventArgs(time))
        End Sub

        Protected Sub RaiseStartedEvent()
            RaiseEvent Started(Me, New StartedEventArgs())
        End Sub

        Protected Sub RaiseStartedEvent(time As Date)
            RaiseEvent Started(Me, New StartedEventArgs(time))
        End Sub

        Protected Sub RaiseExitedEvent()
            RaiseEvent Exited(Me, New ExitedEventArgs())
        End Sub

        Protected Sub RaiseExitedEvent(code As Integer, time As Date)
            RaiseEvent Exited(Me, New ExitedEventArgs(code, time))
        End Sub

        Protected Sub OnProcessExited(sender As Object, e As EventArgs)
            Dim nCode As Integer = UndefinedCodeValue
            Dim dtExit As Date = UndefinedTimeValue

            Dim bEx As Boolean = True

            Try
                dtExit = m_proc.ExitTime
                nCode = m_proc.ExitCode
                bEx = False
            Catch
                bEx = True
            Finally
                ReadProcessOutput()
                StopOutputRecording()
                If bEx Then
                    RaiseExitedEvent()
                Else
                    RaiseExitedEvent(nCode, dtExit)
                End If
            End Try
        End Sub

        Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function

#End Region

    End Class
End Namespace
