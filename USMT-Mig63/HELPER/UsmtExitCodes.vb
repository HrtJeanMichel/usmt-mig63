﻿Namespace Helper
    Public Enum UsmtExitCodes
        USMT_SUCCESS = 0
        USMT_DISPLAY_HELP = 1
        USMT_STATUS_CANCELED = 2
        USMT_WOULD_HAVE_FAILED = 3 'NON FATAL CODE with /c Option
        USMT_INVALID_PARAMETERS = 11
        USMT_ERROR_OPTION_PARAM_TOO_LARGE = 12
        USMT_INIT_LOGFILE_FAILED = 13
        USMT_ERROR_USE_LAC = 14
        USMT_INIT_ERROR = 26
        USMT_INVALID_STORE_LOCATION = 27
        USMT_UNABLE_GET_SCRIPTFILES = 28
        USMT_FAILED_MIGSTARTUP = 29
        USMT_UNABLE_FINDMIGUNITS = 31
        USMT_FAILED_SETMIGRATIONTYPE = 32
        USMT_UNABLE_READKEY = 33
        USMT_ERROR_INSUFFICIENT_RIGHTS = 34
        USMT_UNABLE_DELETE_STORE = 35
        USMT_ERROR_UNSUPPORTED_PLATFORM = 36
        USMT_ERROR_NO_INVALID_KEY = 37
        USMT_ERROR_CORRUPTED_NOTENCRYPTED_STORE = 38
        USMT_UNABLE_TO_READ_CONFIG_FILE = 39
        USMT_ERROR_UNABLE_CREATE_PROGRESS_LOG = 40
        USMT_PREFLIGHT_FILE_CREATION_FAILED = 41
        USMT_ERROR_CORRUPTED_STORE = 42
        USMT_MIGRATION_STOPPED_NONFATAL = 61 'NON FATAL CODE with /c Option
        USMT_INIT_OPERATING_ENVIRONMENT_FAILED = 71
        USMT_UNABLE_DOMIGRATION = 72
    End Enum
End Namespace
