﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports USMT_Mig63.Win32.NativeEnum

Namespace Win32
    Public NotInheritable Class NativeMethods

        <DllImport("advapi32")>
        Public Shared Function OpenProcessToken(ProcessHandle As IntPtr, DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32")>
        Public Shared Function GetCurrentProcess() As IntPtr
        End Function

        <DllImport("advapi32", CharSet:=CharSet.Auto)>
        Public Shared Function GetTokenInformation(hToken As IntPtr, tokenInfoClass As TOKEN_INFORMATION_CLASS, TokenInformation As IntPtr, tokeInfoLength As Integer, ByRef reqLength As Integer) As Boolean
        End Function

        <DllImport("kernel32")>
        Public Shared Function CloseHandle(handle As IntPtr) As Boolean
        End Function

        <DllImport("advapi32", CharSet:=CharSet.Auto)>
        Public Shared Function LookupAccountSid(<[In], MarshalAs(UnmanagedType.LPTStr)> lpSystemName As String, pSid As IntPtr, Account As StringBuilder, ByRef cbName As Integer, DomainName As StringBuilder, ByRef cbDomainName As Integer, ByRef peUse As Integer) As Boolean
        End Function

        <DllImport("advapi32", CharSet:=CharSet.Auto)>
        Public Shared Function ConvertSidToStringSid(pSID As IntPtr, <[In], Out, MarshalAs(UnmanagedType.LPTStr)> ByRef pStringSid As String) As Boolean
        End Function

        <DllImport("advapi32", CharSet:=CharSet.Auto)>
        Public Shared Function ConvertStringSidToSid(<[In], MarshalAs(UnmanagedType.LPTStr)> pStringSid As String, ByRef pSID As IntPtr) As Boolean
        End Function

#Region " HELPER.UTILS "
        <DllImport("user32.dll", SetLastError:=True)>
        Public Shared Function CancelShutdown() As Integer
        End Function

        <DllImport("Shlwapi.dll", CharSet:=CharSet.Auto)>
        Public Shared Function StrFormatByteSize(fileSize As Long, <MarshalAs(UnmanagedType.LPTStr)> buffer As StringBuilder, bufferSize As Integer) As Long
        End Function

        <DllImport("shell32.dll", CharSet:=CharSet.Auto)>
        Public Shared Function SHGetFileInfo(pszPath As String, dwFileAttributes As UInteger, ByRef psfi As NativeStruct.SHFILEINFO, cbFileInfo As UInteger, uFlags As UInteger) As IntPtr
        End Function
#End Region

#Region " EXTENSIONS.TREEVIEWEX "
        <DllImport("user32.dll", CharSet:=CharSet.Auto)>
        Public Shared Function SendMessage(hWnd As IntPtr, Msg As UInteger, wParam As IntPtr, lParam As IntPtr) As IntPtr
        End Function

        <DllImport("uxtheme.dll", ExactSpelling:=True, CharSet:=CharSet.Unicode)>
        Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
        End Function
#End Region

        <DllImport("wtsapi32.dll", SetLastError:=True)>
        Public Shared Function WTSLogoffSession(hServer As IntPtr, SessionId As Integer, bWait As Boolean) As Boolean
        End Function

        <DllImport("kernel32.dll")>
        Public Shared Function WTSGetActiveConsoleSessionId() As UInteger
        End Function

        <DllImport("Wtsapi32.dll")>
        Public Shared Function WTSQuerySessionInformation(hServer As IntPtr, sessionId As UInteger, wtsInfoClass As NativeEnum.WTS_INFO_CLASS, ByRef ppBuffer As IntPtr, ByRef pBytesReturned As UInteger) As Boolean
        End Function

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
        Public Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
        End Function

        <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
        Public Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
        End Sub

        <DllImport("wtsapi32.dll", BestFitMapping:=True, CallingConvention:=CallingConvention.StdCall, CharSet:=CharSet.Auto, EntryPoint:="WTSEnumerateSessions", SetLastError:=True, ThrowOnUnmappableChar:=True)>
        Public Shared Function WTSEnumerateSessions(ByVal hServer As IntPtr, <MarshalAs(UnmanagedType.U4)> ByVal Reserved As Integer, <MarshalAs(UnmanagedType.U4)> ByVal Version As Integer, ByRef ppSessionInfo As IntPtr, <MarshalAs(UnmanagedType.U4)> ByRef pCount As Integer) As Integer
        End Function

        <DllImport("wtsapi32.dll")>
        Public Shared Sub WTSFreeMemory(pMemory As IntPtr)
        End Sub



        <DllImport("Netapi32.dll", CallingConvention:=CallingConvention.StdCall, EntryPoint:="DsGetDcNameW", CharSet:=CharSet.Unicode)>
        Public Shared Function DsGetDcName(<[In]> ByVal computerName As String, <[In]> ByVal domainName As String, <[In]> ByVal domainGuid As IntPtr, <[In]> ByVal siteName As String, <[In]> ByVal flags As DSGETDCNAME_FLAGS, <Out> ByRef domainControllerInfo As IntPtr) As Integer
        End Function

        <DllImport("Netapi32.dll")>
        Public Shared Function NetApiBufferFree(<[In]> ByVal buffer As IntPtr) As Integer
        End Function
    End Class
End Namespace
