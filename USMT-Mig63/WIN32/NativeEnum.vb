﻿Namespace Win32
    Public NotInheritable Class NativeEnum

#Region " HELPER.UTILS "

        Public Enum WTS_INFO_CLASS
            WTSInitialProgram
            WTSApplicationName
            WTSWorkingDirectory
            WTSOEMId
            WTSSessionId
            WTSUserName '5
            WTSWinStationName
            WTSDomainName '7
            WTSConnectState
            WTSClientBuildNumber
            WTSClientName
            WTSClientDirectory
            WTSClientProductId
            WTSClientHardwareId
            WTSClientAddress
            WTSClientDisplay
            WTSClientProtocolType
            WTSIdleTime
            WTSLogonTime
            WTSIncomingBytes
            WTSOutgoingBytes
            WTSIncomingFrames
            WTSOutgoingFrames
            WTSClientInfo
            WTSSessionInfo
        End Enum

        Public Enum TOKEN_INFORMATION_CLASS
            TokenUser = 1
            TokenGroups
            TokenPrivileges
            TokenOwner
            TokenPrimaryGroup
            TokenDefaultDacl
            TokenSource
            TokenType
            TokenImpersonationLevel
            TokenStatistics
            TokenRestrictedSids
            TokenSessionId
        End Enum

        Public Enum MachineType As UShort
            IMAGE_FILE_MACHINE_UNKNOWN = &H0
            IMAGE_FILE_MACHINE_AM33 = &H1D3
            IMAGE_FILE_MACHINE_AMD64 = &H8664
            IMAGE_FILE_MACHINE_ARM = &H1C0
            IMAGE_FILE_MACHINE_EBC = &HEBC
            IMAGE_FILE_MACHINE_I386 = &H14C
            IMAGE_FILE_MACHINE_IA64 = &H200
            IMAGE_FILE_MACHINE_M32R = &H9041
            IMAGE_FILE_MACHINE_MIPS16 = &H266
            IMAGE_FILE_MACHINE_MIPSFPU = &H366
            IMAGE_FILE_MACHINE_MIPSFPU16 = &H466
            IMAGE_FILE_MACHINE_POWERPC = &H1F0
            IMAGE_FILE_MACHINE_POWERPCFP = &H1F1
            IMAGE_FILE_MACHINE_R4000 = &H166
            IMAGE_FILE_MACHINE_SH3 = &H1A2
            IMAGE_FILE_MACHINE_SH3DSP = &H1A3
            IMAGE_FILE_MACHINE_SH4 = &H1A6
            IMAGE_FILE_MACHINE_SH5 = &H1A8
            IMAGE_FILE_MACHINE_THUMB = &H1C2
            IMAGE_FILE_MACHINE_WCEMIPSV2 = &H169
        End Enum

        <Flags> Public Enum SHGFI
            SHGFI_ICON = &H100
            SHGFI_DISPLAYNAME = &H200
            SHGFI_TYPENAME = &H400
            SHGFI_ATTRIBUTES = &H800
            SHGFI_ICONLOCATION = &H1000
            SHGFI_EXETYPE = &H2000
            SHGFI_SYSICONINDEX = &H4000
            SHGFI_LINKOVERLAY = &H8000
            SHGFI_SELECTED = &H10000
            SHGFI_ATTR_SPECIFIED = &H20000
            SHGFI_LARGEICON = &H0
            SHGFI_SMALLICON = &H1
            SHGFI_OPENICON = &H2
            SHGFI_SHELLICONSIZE = &H4
            SHGFI_PIDL = &H8
            SHGFI_USEFILEATTRIBUTES = &H10
            SHGFI_ADDOVERLAYS = &H20
            SHGFI_OVERLAYINDEX = &H40
            FILE_ATTRIBUTE_NORMAL = &H80
        End Enum

        Public Enum IconSize
            LargeIcon = SHGFI.SHGFI_ICON Or SHGFI.SHGFI_LARGEICON
            SmallIcon = SHGFI.SHGFI_ICON Or SHGFI.SHGFI_SMALLICON
        End Enum
#End Region

        <Flags>
        Public Enum DSGETDCNAME_FLAGS
            DS_FORCE_REDISCOVERY = &H1
            DS_DIRECTORY_SERVICE_REQUIRED = &H10
            DS_DIRECTORY_SERVICE_PREFERRED = &H20
            DS_GC_SERVER_REQUIRED = &H40
            DS_PDC_REQUIRED = &H80
            DS_BACKGROUND_ONLY = &H100
            DS_IP_REQUIRED = &H200
            DS_KDC_REQUIRED = &H400
            DS_TIMESERV_REQUIRED = &H800
            DS_WRITABLE_REQUIRED = &H1000
            DS_GOOD_TIMESERV_PREFERRED = &H2000
            DS_AVOID_SELF = &H4000
            DS_ONLY_LDAP_NEEDED = &H8000
            DS_IS_FLAT_NAME = &H10000
            DS_IS_DNS_NAME = &H20000
            DS_RETURN_DNS_NAME = &H40000000
            DS_RETURN_FLAT_NAME = &H80000000
        End Enum

    End Class
End Namespace
