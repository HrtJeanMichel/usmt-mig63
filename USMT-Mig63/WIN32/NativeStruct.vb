﻿Imports System.Runtime.InteropServices

Namespace Win32
    Public NotInheritable Class NativeStruct

#Region " HELPER.UTILS "
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
        Public Structure SHFILEINFO
            Public hIcon As IntPtr
            Public iIcon As Integer
            Public dwAttributes As UInteger
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=260)>
            Public szDisplayName As String
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)>
            Public szTypeName As String
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure TOKEN_USER
            Public User As _SID_AND_ATTRIBUTES
        End Structure

        <StructLayout(LayoutKind.Sequential)>
        Public Structure _SID_AND_ATTRIBUTES
            Public Sid As IntPtr
            Public Attributes As Integer
        End Structure

#End Region

    End Class
End Namespace
