﻿Imports USMT_Mig63.Core

Namespace Engine
    Public Class Task
        Public Property OperatingSys As OperatingSys
        Public Property Usmt As Usmt
        Public Property Store As Store
        Public Property Encryption As Encryption
        Public Property Users As List(Of String)

        Public Sub New()
            OperatingSys = New OperatingSys
            Usmt = New Usmt
            Store = New Store
            Encryption = New Encryption
            Users = New List(Of String)
        End Sub
    End Class
End Namespace
