﻿Imports System.Globalization
Imports System.IO
Imports MetroFramework.Controls
Imports USMT_Mig63.Core
Imports USMT_Mig63.Helper

Namespace Engine
    Public Class Manager

#Region " Fields "
        Private m_FrmMain As FrmMain
        Private m_Capture As Task
        Private m_Restore As Task
        Private m_RestoreXmlInfos As MigContent
#End Region

#Region " Constructor "
        Public Sub New(m_FrmM As FrmMain)
            m_FrmMain = m_FrmM
            InitGrayed()
            m_Capture = New Task
            m_Restore = New Task
            InitOS()
            InitUSMT()
        End Sub
#End Region

#Region " Main "
        Private Sub InitGrayed()
            Utils.GrayedImage(m_FrmMain.BtnCaptureEncrypt, False, My.Resources.Padunlock)
            Utils.GrayedImage(m_FrmMain.BtnCaptureUsers, False, My.Resources.Users)
            Utils.GrayedImage(m_FrmMain.BtnRestoreDecrypt, False, My.Resources.Padunlock)
            Utils.GrayedImage(m_FrmMain.BtnRestoreUsers, False, My.Resources.Users)
        End Sub

        Private Sub InitOS()
            m_FrmMain.LblCaptureOsName.Text = m_Capture.OperatingSys.FullName
            m_FrmMain.PcbCaptureOsLogo.Image = m_Capture.OperatingSys.Logo
            m_FrmMain.LblRestoreOsName.Text = m_Restore.OperatingSys.FullName
            m_FrmMain.PcbRestoreOsLogo.Image = m_Restore.OperatingSys.Logo
        End Sub

        Private Sub InitUSMT()
            m_FrmMain.LblCaptureUsmtPath.Text = m_Capture.Usmt.RootPath
            m_FrmMain.LblRestoreUsmtPath.Text = m_Restore.Usmt.RootPath
        End Sub
#End Region

#Region " Capture "
        Public Sub SettingUSMT(Optional ByVal isRestore As Boolean = False)
            Dim UmstSetting As Usmt = If(isRestore, m_Restore.Usmt, m_Capture.Usmt)
            Using Settings As New FrmSettings(UmstSetting, isRestore)
                m_FrmMain.Hide()
                If Settings.ShowDialog = DialogResult.OK Then
                    UmstSetting.MigUserPath = Settings.TxbSettingsUsmtMigFilesUserPath.Text
                    UmstSetting.MigAppPath = Settings.TxbSettingsUsmtMigFilesAppPath.Text
                    UmstSetting.MigDocsPath = Settings.TxbSettingsUsmtMigFilesOtherDocsPath.Text
                    UmstSetting.ConfigPath = Settings.TxbSettingsUsmtMigFilesCustomConfigPath.Text
                    UmstSetting.ExcludeDrivesPath = Settings.TxbSettingsUsmtMigFilesExcludeDrives.Text
                    UmstSetting.ExcludeSystemPath = Settings.TxbSettingsUsmtMigFilesExcludeSystem.Text
                    UmstSetting.Config1Path = Settings.TxbSettingsUsmtMigFilesCustomXmlPath1.Text
                    UmstSetting.Config2Path = Settings.TxbSettingsUsmtMigFilesCustomXmlPath2.Text
                End If
            End Using
            m_FrmMain.Show()
        End Sub

        Public Sub ShowLabelInfos(Lbl As MetroLabel)
            If Lbl.Text <> String.Empty Then
                m_FrmMain.TtInfos.Show(Lbl.Text, Lbl)
            End If
        End Sub

        Public Sub ShowUSMTInfos(Btn As Button, Optional ByVal isRestore As Boolean = False)
            Dim UmstSetting As Usmt = If(isRestore, m_Restore.Usmt, m_Capture.Usmt)
            Dim MigFiles = New String() {UmstSetting.MigUserPath, UmstSetting.MigAppPath,
                                            UmstSetting.MigDocsPath, UmstSetting.ConfigPath,
                                            UmstSetting.ExcludeDrivesPath, UmstSetting.ExcludeSystemPath,
                                            UmstSetting.Config1Path, UmstSetting.Config2Path}

            Dim message As String = "Sélectionnez les fichiers de configuration qui seront utilisés" & vbNewLine & "pour définir les éléments à " & If(isRestore, "restaurer.", "capturer.")
            m_FrmMain.TtInfos.Show("INFO : " & vbNewLine & message, Btn)
        End Sub

        Public Sub ShowStoreInfos(btn As Button, Optional ByVal isRestore As Boolean = False)
            Dim message As String = "Sélectionnez le répertoire dans lequel ScanState.exe créera" & vbNewLine &
                                    "un dossier nommé USMT afin de stocker le fichier de migration."
            If isRestore Then
                message = "Sélectionnez le fichier de migration .MIG qui sera utilisé" & vbNewLine &
                          "afin de restaurer les fichiers et paramètres utilisateurs."
            End If
            m_FrmMain.TtInfos.Show("INFO : " & vbNewLine & message, btn)
        End Sub

        Public Sub ShowCryptInfos(btn As Button, Optional ByVal isRestore As Boolean = False)
            Dim message As String = "Saisissez un mot de passe ou non pour protéger le fichier de migration." & vbNewLine &
                                    "Ce mot de passe vous sera demandé pour restaurer le fichier de migration."
            If isRestore Then
                message = "Si requis, saisissez le mot de passe qui protège le fichier de migration." & vbNewLine &
                          "Sans ce mot de passe il est impossible de restaurer le fichier de migration."
            End If
            m_FrmMain.TtInfos.Show("INFO : " & vbNewLine & message, btn)
        End Sub

        Public Sub ShowUsersInfos(btn As Button, Optional ByVal isRestore As Boolean = False)
            Dim message As String = "Sélectionnez le ou les comptes utilisateurs à migrer depuis le PC source." & vbNewLine &
                                    "Un compte peut être du type domaine ou local."
            If isRestore Then
                message = "Sélectionnez le ou les comptes utilisateurs qui seront restaurés sur le PC cible." & vbNewLine &
                          "Les comptes non existants sur le PC cible seront créés ainsi que leur profil associé." & vbNewLine &
                          "Les comptes existants sur le PC cible verront leurs données fusionnées avec celle contenu dans le fichier de migration."
            End If
            m_FrmMain.TtInfos.Show("INFO : " & vbNewLine & message, btn)
        End Sub

        Public Sub ShowMigInfos(btn As Button)
            Dim message As String = "Cliquez sur ce bouton pour visualisez le contenu détaillé du fichier .MIG :" & vbNewLine &
                                    "- arborescence des répertoires et fichiers," & vbNewLine &
                                    "- nombre de fichiers dans le .MIG," & vbNewLine &
                                    "- poids du fichier .MIG," & vbNewLine &
                                    "- date et heure de création du fichier .MIG," & vbNewLine &
                                    "- durée de la capture," & vbNewLine &
                                    "- nombre de comptes migrés," & vbNewLine &
                                    "- nom du système d'exploitation source," & vbNewLine &
                                    "- nom de l'ordinateur source."
            m_FrmMain.TtInfos.Show("INFO : " & vbNewLine & message, btn)
        End Sub

        Public Sub CaptureSaveBrowse(OldSelectedPath As String, SelectedPath As String)
            If Utils.IsDirectoryPath(SelectedPath) = False Then
                m_FrmMain.LblCaptureSavePath.Text = "Pas de répertoire sélectionné !"
                Using Info As New FrmInfo("Sélection invalide", "La sélection est un lecteur et non un répertoire !")
                    Info.ShowDialog()
                End Using
                BrowseCaptureUpdateControls(True)
                Exit Sub
            Else
                If Utils.HasWritePermission(SelectedPath) Then
                    If Mig.StoreExists(SelectedPath) Then
                        Using Info As New FrmInfo("Attention", "Ce répertoire contient déjà un fichier de migration !" & vbNewLine & "Voulez-vous quand même écraser son contenu ?", True)
                            If Info.ShowDialog() = DialogResult.OK Then
                                m_FrmMain.LblCaptureSavePath.Text = SelectedPath
                                If OldSelectedPath <> SelectedPath Then
                                    BrowseCaptureUpdateControls(True)
                                    Exit Sub
                                End If
                            Else
                                m_FrmMain.LblCaptureSavePath.Text = "Pas de répertoire sélectionné !"
                                BrowseCaptureUpdateControls(True)
                                Exit Sub
                            End If
                        End Using
                    Else
                        m_FrmMain.LblCaptureSavePath.Text = SelectedPath
                        If OldSelectedPath <> SelectedPath Then
                            BrowseCaptureUpdateControls(True)
                            Exit Sub
                        End If
                    End If
                Else
                    m_FrmMain.LblCaptureSavePath.Text = "Pas de répertoire sélectionné !"
                    Using Info As New FrmInfo("Sélection invalide", "Ce répertoire est inaccessible en écriture !")
                        Info.ShowDialog()
                    End Using
                    BrowseCaptureUpdateControls(True)
                    Exit Sub
                End If
            End If
            BrowseCaptureUpdateControls(False)
        End Sub

        Public Sub BrowseCaptureUpdateControls(notSamePath As Boolean)
            If notSamePath Then
                Dim Valid As Boolean = Directory.Exists(m_FrmMain.LblCaptureSavePath.Text)
                m_FrmMain.LblCaptureUser.Text = "Pas d'utilisateur sélectioné !"
                m_FrmMain.LblCaptureEncrypt.Text = "Pas de mot de passe !"
                m_FrmMain.BtnCaptureEncrypt.Image = My.Resources.Padunlock
                Utils.GrayedImage(m_FrmMain.BtnCaptureEncrypt, Valid, My.Resources.Padunlock)
                Utils.GrayedImage(m_FrmMain.BtnCaptureUsers, Valid, My.Resources.User)
                m_FrmMain.BtnCapture.Enabled = (Not m_FrmMain.LblCaptureUser.Text.EndsWith("!"))
                If Valid Then m_Capture.Store.Path = m_FrmMain.LblCaptureSavePath.Text
                m_Capture.Users.Clear()
                m_Capture.Encryption.Password = ""
            Else
                Dim Valid As Boolean = Directory.Exists(m_FrmMain.LblCaptureSavePath.Text)
                Utils.GrayedImage(m_FrmMain.BtnCaptureEncrypt, Valid, If(m_FrmMain.LblCaptureEncrypt.Text = "Pas de mot de passe !", My.Resources.Padunlock, My.Resources.Padlock))
                Utils.GrayedImage(m_FrmMain.BtnCaptureUsers, Valid, If(m_FrmMain.LblCaptureUser.Text = "Pas d'utilisateur sélectionné !" OrElse m_FrmMain.LblCaptureUser.Text.EndsWith("utilisateurs sélectionnés."), My.Resources.Users, My.Resources.User))
                m_FrmMain.BtnCapture.Enabled = (Not m_FrmMain.LblCaptureUser.Text.EndsWith("!"))
                If Valid Then m_Capture.Store.Path = m_FrmMain.LblCaptureSavePath.Text
            End If
        End Sub

        Public Sub SettingCaptureEncrypt()
            Using frmEncrypt As New FrmCrypt(m_Capture.Encryption.Password)
                m_FrmMain.Hide()
                If frmEncrypt.ShowDialog = DialogResult.OK Then
                    m_Capture.Encryption.Password = frmEncrypt.TxbCryptPassword.Text
                    m_FrmMain.LblCaptureEncrypt.Text = If(m_Capture.Encryption.Password = String.Empty, "Pas de mot de passe !", "Mot de passe OK.")
                End If
                m_FrmMain.BtnCaptureEncrypt.Image = If(m_FrmMain.LblCaptureEncrypt.Text = "Mot de passe OK.", My.Resources.Padlock, My.Resources.Padunlock)
                m_FrmMain.Show()
            End Using
        End Sub

        Public Sub SettingCaptureUsers()
            Using frmUsers As New FrmCaptureUsers(m_Capture.Users)
                m_FrmMain.Hide()
                If frmUsers.ShowDialog = DialogResult.OK Then
                    If m_Capture.Users.Count = 0 Then
                        m_FrmMain.LblCaptureUser.Text = "Pas d'utilisateur sélectionné !"
                        m_FrmMain.BtnCaptureUsers.BackgroundImage = My.Resources.Users
                        m_FrmMain.BtnCapture.Enabled = False
                    ElseIf m_Capture.Users.Count = 1 Then
                        m_FrmMain.LblCaptureUser.Text = "1 utilisateur sélectionné."
                        m_FrmMain.BtnCaptureUsers.BackgroundImage = My.Resources.User
                        m_FrmMain.BtnCapture.Enabled = (Not m_FrmMain.LblCaptureSavePath.Text.EndsWith("!"))
                    ElseIf m_Capture.Users.Count > 1 Then
                        m_FrmMain.BtnCaptureUsers.BackgroundImage = My.Resources.Users
                        m_FrmMain.LblCaptureUser.Text = m_Capture.Users.Count.ToString & " utilisateurs sélectionnés."
                        m_FrmMain.BtnCapture.Enabled = (Not m_FrmMain.LblCaptureSavePath.Text.EndsWith("!"))
                    End If
                End If
                m_FrmMain.Show()
            End Using
        End Sub

        Public Sub ApplyCapture()
            Dim tups = Utils.Usmt5PathInfos
            If tups.Item1 = True Then
                If Directory.Exists(m_Capture.Store.Path) Then
                    Dim m_XmlFiles = New String() {m_Capture.Usmt.MigUserPath, m_Capture.Usmt.MigAppPath, m_Capture.Usmt.MigDocsPath, m_Capture.Usmt.ConfigPath, m_Capture.Usmt.ExcludeDrivesPath, m_Capture.Usmt.ExcludeSystemPath, m_Capture.Usmt.Config1Path, m_Capture.Usmt.Config2Path}
                    Using CaptureFrm As New FrmTask(m_Capture.Usmt.RootPath, m_Capture.Store.Path, m_Capture.Encryption.Password, m_Capture.Users, m_XmlFiles)
                        m_FrmMain.Hide()
                        If CaptureFrm.ShowDialog() = DialogResult.OK Then
                            m_FrmMain.Show()
                        End If
                    End Using
                Else
                    Using infos As New FrmInfo("Mauvais environnement", "Le répertoire de sauvegarde n'existe pas :" & vbNewLine & m_Capture.Store.Path)
                        infos.ShowDialog()
                    End Using
                End If
            Else
                Using infos As New FrmInfo(tups.Item2, tups.Item3)
                    infos.ShowDialog()
                End Using
            End If
            'If File.Exists(m_Capture.Usmt.ConfigPath) = False Then
            '    Using GenConfig As New FrmInfo("Génération d'un fichier de configuration", "Voulez-vous générer le fichier de configuration Config.xml de manière à pouvoir sélectionner les paramètres à migrer ?")
            '        If GenConfig.ShowDialog() = DialogResult.OK Then

            '        Else
            '            Dim tups = Utils.Usmt5PathInfos
            '            If tups.Item1 = True Then
            '                If Directory.Exists(m_Capture.Store.Path) Then
            '                    Dim m_XmlFiles = New String() {m_Capture.Usmt.MigUserPath, m_Capture.Usmt.MigAppPath, m_Capture.Usmt.MigDocsPath, m_Capture.Usmt.ConfigPath, m_Capture.Usmt.ExcludeDrivesPath, m_Capture.Usmt.ExcludeSystemPath, m_Capture.Usmt.Config1Path, m_Capture.Usmt.Config2Path}
            '                    Using CaptureFrm As New FrmTask(m_Capture.Usmt.RootPath, m_Capture.Store.Path, m_Capture.Encryption.Password, m_Capture.Users, m_XmlFiles)
            '                        m_FrmMain.Hide()
            '                        If CaptureFrm.ShowDialog() = DialogResult.OK Then
            '                            m_FrmMain.Show()
            '                        End If
            '                    End Using
            '                Else
            '                    Using infos As New FrmInfo("Mauvais environnement", "Le répertoire de sauvegarde n'existe pas :" & vbNewLine & m_Capture.Store.Path)
            '                        infos.ShowDialog()
            '                    End Using
            '                End If
            '            Else
            '                Using infos As New FrmInfo(tups.Item2, tups.Item3)
            '                    infos.ShowDialog()
            '                End Using
            '            End If
            '        End If
            '    End Using
            'End If
        End Sub
#End Region

#Region " Restore "
        Public Sub LoadMigFileInfos(Selectedpath As String)

            If m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe OK." AndAlso Selectedpath = m_FrmMain.LblRestoreFilePath.Text Then
            ElseIf m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe non requis." AndAlso Selectedpath = m_FrmMain.LblRestoreFilePath.Text Then
            Else
                Dim infos = Mig.StoreIsAuthentic(Selectedpath)
                If infos.Item1 = True Then
                    Dim migPath As String = Path.GetDirectoryName(Selectedpath)
                    m_RestoreXmlInfos = infos.Item4
                    m_Restore.Encryption.Password = ""
                    m_Restore.Store.FilePath = Selectedpath
                    m_FrmMain.LblRestoreFilePath.Text = Selectedpath
                    m_FrmMain.LblRestoreUser.Text = "Pas d'utilisateur sélectionné !"
                    m_FrmMain.LblRestoreDecrypt.Text = If(m_RestoreXmlInfos.PasswordMd5 <> "", "Mot de passe requis !", "Mot de passe non requis.")
                    m_Restore.Users.Clear()

                    SettingRestoreDecrypt(Selectedpath, migPath)
                Else
                    m_RestoreXmlInfos = Nothing
                    Using Info As New FrmInfo(infos.Item2, infos.Item3)
                        Info.ShowDialog()
                    End Using
                End If
            End If
        End Sub

        Public Sub SettingRestoreDecrypt(Selectedpath As String, migPath As String)
            Dim Valid As Boolean = File.Exists(m_FrmMain.LblRestoreFilePath.Text)

            If m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe requis !" Then
                Utils.GrayedImage(m_FrmMain.BtnRestoreDecrypt, Valid, My.Resources.Padlock)
                m_FrmMain.Hide()

                Using frmEncrypt As New FrmCrypt(m_Restore.Encryption.Password, True, m_RestoreXmlInfos.PasswordMd5)
                    If frmEncrypt.ShowDialog = DialogResult.OK Then
                        SettingRestoreDb(Selectedpath, migPath)

                        m_Restore.Encryption.Password = frmEncrypt.TxbCryptPassword.Text
                        Utils.GrayedImage(m_FrmMain.BtnRestoreDecrypt, True, My.Resources.Padunlock)
                        m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe OK."
                        Utils.GrayedImage(m_FrmMain.BtnRestoreUsers, True, If(m_FrmMain.LblRestoreUser.Text.EndsWith("utilisateurs sélectionnés."), My.Resources.Users, My.Resources.User))
                        m_FrmMain.BtnRestore.Enabled = (Not m_FrmMain.LblRestoreUser.Text.EndsWith("!"))
                    Else
                        If m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe OK." AndAlso Selectedpath = m_FrmMain.LblRestoreFilePath.Text Then
                            m_FrmMain.BtnRestoreFileInfos.Visible = True
                        ElseIf m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe requis !" Then
                            Utils.GrayedImage(m_FrmMain.BtnRestoreUsers, False, If(m_FrmMain.LblRestoreUser.Text.EndsWith("utilisateurs sélectionnés."), My.Resources.Users, My.Resources.User))
                            m_FrmMain.BtnRestoreFileInfos.Visible = False
                        ElseIf m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe non requis." Then
                            Utils.GrayedImage(m_FrmMain.BtnRestoreUsers, Valid, If(m_FrmMain.LblRestoreUser.Text.EndsWith("utilisateurs sélectionnés."), My.Resources.Users, My.Resources.User))
                            m_FrmMain.BtnRestoreFileInfos.Visible = True
                        End If
                    End If
                    m_FrmMain.Show()
                End Using
            ElseIf m_FrmMain.LblRestoreDecrypt.Text = "Mot de passe non requis." Then
                SettingRestoreDb(Selectedpath, migPath)
                Utils.GrayedImage(m_FrmMain.BtnRestoreDecrypt, Valid, My.Resources.Padunlock)
                Utils.GrayedImage(m_FrmMain.BtnRestoreUsers, True, If(m_FrmMain.LblRestoreUser.Text.EndsWith("utilisateurs sélectionnés."), My.Resources.Users, My.Resources.User))
                m_FrmMain.BtnRestore.Enabled = (Not m_FrmMain.LblRestoreUser.Text.EndsWith("!"))
            End If
        End Sub

        Private Sub SettingRestoreDb(Selectedpath As String, migpath As String)
            For Each acc In m_RestoreXmlInfos.Accounts
                m_Restore.Users.Add(acc.Name & "|" & acc.Type & "|" & acc.Include.ToString)
            Next
            m_FrmMain.LblRestoreUser.Text = If(m_Restore.Users.Count > 1, m_Restore.Users.Count.ToString & " utilisateurs sélectionnés.", "1 utilisateur sélectionné.")
            m_FrmMain.LblRestoreFilePath.Text = Selectedpath

            m_Restore.Usmt.MigUserPath = If(m_RestoreXmlInfos.XmlFiles(0) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(0)), "")
            m_Restore.Usmt.MigAppPath = If(m_RestoreXmlInfos.XmlFiles(1) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(1)), "")
            m_Restore.Usmt.MigDocsPath = If(m_RestoreXmlInfos.XmlFiles(2) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(2)), "")
            m_Restore.Usmt.ConfigPath = If(m_RestoreXmlInfos.XmlFiles(3) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(3)), "")
            m_Restore.Usmt.ExcludeDrivesPath = If(m_RestoreXmlInfos.XmlFiles(4) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(4)), "")
            m_Restore.Usmt.ExcludeSystemPath = If(m_RestoreXmlInfos.XmlFiles(5) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(5)), "")
            m_Restore.Usmt.Config1Path = If(m_RestoreXmlInfos.XmlFiles(6) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(6)), "")
            m_Restore.Usmt.Config2Path = If(m_RestoreXmlInfos.XmlFiles(7) <> String.Empty, Path.Combine(migpath, m_RestoreXmlInfos.XmlFiles(7)), "")

            m_FrmMain.BtnRestoreFileInfos.Visible = True
        End Sub

        Public Sub SettingRestoreUsers()
            Using frmUsers As New FrmRestoreUsers(m_Restore.Users)
                m_FrmMain.Hide()
                If frmUsers.ShowDialog = DialogResult.OK Then
                    Dim UsersTicked = m_Restore.Users.Where(Function(f) f.Split("|")(2) = "True")
                    If UsersTicked.Count = 1 Then
                        m_FrmMain.LblRestoreUser.Text = "1 utilisateur sélectionné."
                        m_FrmMain.BtnRestoreUsers.BackgroundImage = My.Resources.User
                        m_FrmMain.BtnRestore.Enabled = (Not m_FrmMain.LblRestoreFilePath.Text.EndsWith("!"))
                    ElseIf UsersTicked.Count > 1 Then
                        m_FrmMain.BtnRestoreUsers.BackgroundImage = My.Resources.Users
                        m_FrmMain.LblRestoreUser.Text = UsersTicked.Count.ToString & " utilisateurs sélectionnés."
                        m_FrmMain.BtnRestore.Enabled = (Not m_FrmMain.LblRestoreFilePath.Text.EndsWith("!"))
                    End If
                End If
                m_FrmMain.Show()
            End Using
        End Sub

        Public Sub ApplyRestore()
            Dim tups = Utils.Usmt5PathInfos
            If tups.Item1 = True Then
                If File.Exists(m_Restore.Store.FilePath) Then
                    Dim m_XmlFiles = New String() {m_Restore.Usmt.MigUserPath, m_Restore.Usmt.MigAppPath, m_Restore.Usmt.MigDocsPath, m_Restore.Usmt.ConfigPath, m_Restore.Usmt.ExcludeDrivesPath, m_Restore.Usmt.ExcludeSystemPath, m_Restore.Usmt.Config1Path, m_Restore.Usmt.Config2Path, True}
                    Using RestoreFrm As New FrmTask(m_Restore.Usmt.RootPath, m_Restore.Store.FilePath, m_Restore.Encryption.Password, m_Restore.Users, m_XmlFiles, True)
                        m_FrmMain.Hide()
                        If RestoreFrm.ShowDialog() = DialogResult.OK Then
                            m_FrmMain.Show()
                        End If
                    End Using
                Else
                    ShowBadEnvironmemntDialog(m_Restore.Store.FilePath)
                End If
            Else
                Using infos As New FrmInfo(tups.Item2, tups.Item3)
                    infos.ShowDialog()
                End Using
            End If
        End Sub

        Public Sub MigFileInfosViewer()
            If File.Exists(m_FrmMain.LblRestoreFilePath.Text) Then
                Dim FileWithoutExt = Path.GetFileNameWithoutExtension(m_FrmMain.LblRestoreFilePath.Text)
                If FileWithoutExt.Count = 24 Then
                    If FileWithoutExt.Contains("_") Then
                        Dim splitted() = FileWithoutExt.Split("_")
                        If splitted(0) = "USMTMIG63" Then
                            Dim reationDate As Date
                            If Date.TryParseExact(splitted(1), "yyyyMMddHHmmss", CultureInfo.InvariantCulture,
                                                                      DateTimeStyles.None, reationDate) Then
                                Using migInfo As New FrmMigView(m_RestoreXmlInfos, reationDate, New FileInfo(m_FrmMain.LblRestoreFilePath.Text).Length)
                                    m_FrmMain.Hide()
                                    If migInfo.ShowDialog() = DialogResult.OK Then m_FrmMain.Show()
                                End Using
                            Else
                                ShowBadNameDialog()
                            End If
                        Else
                            ShowBadNameDialog()
                        End If
                    Else
                        ShowBadNameDialog()
                    End If
                Else
                    ShowBadNameDialog()
                End If
            Else
                ShowBadEnvironmemntDialog(m_FrmMain.LblRestoreFilePath.Text)
            End If
        End Sub

        Private Sub ShowBadNameDialog()
            Using infos As New FrmInfo("Mauvais nommage", "Le nom du fichier .MIG n'est pas correct !")
                infos.ShowDialog()
            End Using
        End Sub

        Private Sub ShowBadEnvironmemntDialog(fPath As String)
            Using infos As New FrmInfo("Mauvais environnement", "Le fichier .MIG n'existe pas :" & vbNewLine & fPath)
                infos.ShowDialog()
            End Using
        End Sub
#End Region

    End Class
End Namespace
