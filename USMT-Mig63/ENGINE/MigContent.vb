﻿Imports System.IO
Imports System.Xml.Serialization
Imports USMT_Mig63.Core

Namespace Engine
    <Serializable>
    Public Class MigContent

        <XmlIgnore()>
        Private m_Accounts As List(Of AccountInfos)
        <XmlIgnore()>
        Private m_Paths As List(Of PathInfos)
        Public Property ScanDuration As String
        Public Property SizeMBToTransfert As String
        Public Property PasswordMd5 As String
        Public Property FromOperatingSystem As String
        Public Property FromComputerName As String
        Public Property FilesCount As Integer
        Public Property XmlFiles As String()

        Public Property Accounts As List(Of AccountInfos)
            Get
                Return m_Accounts
            End Get
            Set(value As List(Of AccountInfos))
                m_Accounts = value
            End Set
        End Property

        Public Property Paths As List(Of PathInfos)
            Get
                Return m_Paths
            End Get
            Set(value As List(Of PathInfos))
                m_Paths = value
            End Set
        End Property

        Public Sub New()
        End Sub

        ''' <summary>
        ''' Enregistre l'état courant de la classe dans un fichier au format XML.
        ''' </summary>
        Public Sub SaveFile(fPath As String)
            Dim serializer As New XmlSerializer(GetType(MigContent))
            Using Save As New StreamWriter(fPath)
                serializer.Serialize(Save, Me)
            End Using
        End Sub

        ''' <summary>
        ''' Charge l'état courant du fichier XML.
        ''' </summary>
        ''' <returns>Valeur de type MigContent</returns>
        Public Shared Function LoadFile(fPath As String) As MigContent
            Dim deserializer As New XmlSerializer(GetType(MigContent))
            Using read As New StreamReader(fPath)
                Return DirectCast(deserializer.Deserialize(read), MigContent)
            End Using
        End Function
    End Class
End Namespace
