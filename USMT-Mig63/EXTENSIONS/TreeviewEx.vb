﻿Imports USMT_Mig63.Win32

Namespace Extensions
    <ToolboxBitmap(GetType(TreeView))>
    Public Class TreeViewEx
        Inherits TreeView

#Region " Property "
        Protected Overrides ReadOnly Property CreateParams() As CreateParams
            Get
                Dim cp As CreateParams = MyBase.CreateParams
                If Environment.OSVersion.Version.Major > 5 Then
                    cp.Style = cp.Style Or &H8000
                End If
                Return cp
            End Get
        End Property
#End Region

#Region " Constructor "
        Public Sub New()
            SetStyle(ControlStyles.AllPaintingInWmPaint Or ControlStyles.OptimizedDoubleBuffer Or ControlStyles.UserPaint, True)
            ShowLines = False
            HideSelection = True
        End Sub
#End Region

#Region " Methods "
        Protected Overrides Sub OnPaint(e As PaintEventArgs)
            If GetStyle(ControlStyles.UserPaint) Then
                Dim m As New Message()
                m.HWnd = Me.Handle
                m.Msg = &H318
                m.WParam = e.Graphics.GetHdc()
                m.LParam = CType(4, IntPtr)
                DefWndProc(m)
                e.Graphics.ReleaseHdc(m.WParam)
            End If
            MyBase.OnPaint(e)
        End Sub

        Protected Overrides Sub OnHandleCreated(e As EventArgs)
            MyBase.OnHandleCreated(e)
            NativeMethods.SetWindowTheme(Handle, "explorer", Nothing)
            If Environment.OSVersion.Version.Major > 5 Then
                Dim lParam As IntPtr = CType(NativeMethods.SendMessage(Handle, &H112D, IntPtr.Zero, IntPtr.Zero).ToInt32() Or &H60, IntPtr)
                NativeMethods.SendMessage(Handle, &H112C, IntPtr.Zero, lParam)
            End If
        End Sub
#End Region

    End Class
End Namespace
