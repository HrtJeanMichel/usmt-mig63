﻿Imports USMT_Mig63.Helper

Namespace Core
    Public Class OperatingSys

        Public Property VersionName As String
        Public Property ProductName As String
        Public Property ReleaseId As String
        Public Property Arch As String
        Public ReadOnly Property Logo() As Bitmap

        Private m_FullName As String
        Public Property FullName() As String
            Get
                Return ProductName & " " & ReleaseId & " " & "(" & Arch & ")"
            End Get
            Set(ByVal value As String)
                m_FullName = value
            End Set
        End Property

        Public Sub New()
            _VersionName = Utils.GetOsVersion
            _ProductName = Utils.GetOsProductName
            _ReleaseId = Utils.GetReleaseId
            _Arch = If(Utils.is64Bits, "x64", "x32") & " bits"
            Select Case _VersionName
                Case "Windows Vista"
                    Logo = My.Resources.WinVista
                Case "Windows 7"
                    Logo = My.Resources.Win7
                Case "Windows 8", "Windows 8.1"
                    Logo = My.Resources.Win8
                Case "Windows 10"
                    Logo = My.Resources.win10
            End Select
        End Sub
    End Class
End Namespace