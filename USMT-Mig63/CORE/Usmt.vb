﻿Imports System.IO
Imports USMT_Mig63.Helper

Namespace Core
    Public Class Usmt

        Private ReadOnly m_AdkInfoShort As String
        Public Property MigUserPath As String
        Public Property MigAppPath As String
        Public Property MigDocsPath As String
        Public Property ConfigPath As String
        Public Property ExcludeDrivesPath As String
        Public Property ExcludeSystemPath As String
        Public Property Config1Path As String
        Public Property Config2Path As String
        Public ReadOnly Property RootPath() As String
        Public ReadOnly Property AdkInfo() As String

        Public Sub New(Optional ByVal ShortAdkString As Boolean = False)
            RootPath = Path.Combine(My.Application.Info.DirectoryPath, "USMT5\" & If(Utils.is64Bits, "amd64", "x86"))
            If Directory.Exists(RootPath) Then
                If File.Exists(Path.Combine(RootPath, "MigUser.xml")) Then
                    MigUserPath = Path.Combine(RootPath, "MigUser.xml")
                End If
                If File.Exists(Path.Combine(RootPath, "MigApp.xml")) Then
                    MigAppPath = Path.Combine(RootPath, "MigApp.xml")
                End If
                If File.Exists(Path.Combine(RootPath, "ExcludeDrives.xml")) Then
                    ExcludeDrivesPath = Path.Combine(RootPath, "ExcludeDrives.xml")
                End If
                If File.Exists(Path.Combine(RootPath, "ExcludeSystem.xml")) Then
                    ExcludeSystemPath = Path.Combine(RootPath, "ExcludeSystem.xml")
                End If
                AdkInfo = Utils.AdkVersionInfoFromFile(Path.Combine(RootPath, "scanstate.exe"), ShortAdkString)
            End If
        End Sub
    End Class
End Namespace