﻿Imports System.IO

Namespace Core
    Public Class Store
        Public Property Path As String

        Private m_FilePath As String

        Public Property FilePath() As String
            Get
                Return m_FilePath
            End Get
            Set(ByVal value As String)
                m_FilePath = value
                If File.Exists(value) Then
                    Dim fi As New FileInfo(m_FilePath)
                    'Dim di As New DirectoryInfo(fi.Directory.FullName)
                    _Path = fi.Directory.Parent.FullName
                End If
            End Set
        End Property
    End Class
End Namespace
