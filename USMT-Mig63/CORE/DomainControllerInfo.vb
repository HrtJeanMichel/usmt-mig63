﻿Imports System.Runtime.InteropServices

Namespace Core
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)>
    Public Class DomainControllerInfo
        Public DomainControllerName As String
        Public DomainControllerAddress As String
        Public DomainControllerAddressType As Integer
        Public DomainGuid As Guid
        Public DomainName As String
        Public DnsForestName As String
        Public Flags As Integer
        Public DcSiteName As String
        Public ClientSiteName As String
    End Class

End Namespace
